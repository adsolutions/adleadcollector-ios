//
//  BTPlatformStatus.h
//  Bitrace
//
//  Created by Daniele Angeli on 13/07/14.
//  Copyright (c) 2014 ADSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * @brief Instance of a Service
 */
@interface BTService : NSObject

@property (nonatomic, readonly) BOOL serviceStatus;
@property (nonatomic, readonly) NSString *serviceLocalizedName;
@property (nonatomic, readonly) NSString *serviceName;
@property (nonatomic, readonly) NSUInteger serviceWeight;

@end

/**
 * @brief Platform status updates directly taken from http://status.bitrace.co
 */
@interface BTPlatformStatus : NSObject

/**
 * @brief Returns the current platform status, an internet connection
 * is required
 */
+(void)currentStatusWithCompletionHandler:(void (^)(BTPlatformStatus *status))block;

/**
 * @brief An array of BTServices
 */
@property (nonatomic, readonly) NSArray *services;

@property (nonatomic, readonly) BOOL needClientUpdate;
@property (nonatomic, readonly) NSUInteger currentServerVersion;
@property (nonatomic, readonly) NSURL *bitraceAppDownloadURL;
@property (nonatomic, readonly, getter = isMaintenanceModeActive) BOOL maintenanceModeActive;

@end