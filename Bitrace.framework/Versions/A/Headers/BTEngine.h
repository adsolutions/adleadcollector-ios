//
//  ADBitraceEngine.h
//  Bitrace®
//
//  Created by Daniele Angeli on 10/05/13.
//
//  Copyright (c) 2013 AD Solutions®. All rights reserved.
//
//  This program and the accompanying materials
//  are made available under the terms of the Bitrace® License
//  which accompanies this distribution, and is available at
//  http://bitrace.aditsolutions.it/license/
//
/// @author Daniele Angeli

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#import "BTRemoteKeychainServer.h"

/// @brief Error Domain to identify Bitrace Errors
FOUNDATION_EXPORT NSString *const BTErrorDomain;

/**
 * @brief Use this completion Block to receive call the Bitrace RestFull WS
 * @return error Indicates if the callback was handled successfully, you have to
 * dinstinguish between BTErrorDomain and/or NSURLErrorDomain to identify network
 * errors or login errors
 * @return response the json WS response
 */
typedef void (^BTRestCallbackCompletionHandler)(NSError *error, NSDictionary *response);

/** 
 * @brief BTEngine is the Object reponsible of making the Bitrace Service to work as you expect
 */
@interface BTEngine : NSObject

/**
 * @brief Singleton instance of the Bitrace Engine
 */
+(instancetype)sharedEngine;

/**
 * @brief Invoke loadEngine to initialize the Bitrace Engine instance for the first time
 */
+(void)loadEngineWithToken:(NSString *)token;

/**
 * @brief Verify if the passed URL is a valid login verification callBack
 */
+(void)handleURL:(NSURL *)url withCompletionHandler:(BTVerifyCredentialCompletionHandler)completionHandler;

/**
 * @brief When set to true it will send user GPS coordinates to the Bitrace backend, the default value is false.
 * You can keep it false and use your own CLLocationManager instead the built in one
 */
@property (nonatomic, readwrite, getter=isLocationMonitoringAvaible) BOOL locationMonitoringAvaible;

/**
 * @brief If it's true it'll always monitor the GPS user position even when the app is in a background or inactive state
 */
@property (nonatomic, readwrite) BOOL alwaysMonitorLocation __unavailable;

/*
 * @brief If you set a CLLocationCoordinate2D here, this method will takes care about sending this data for you, 
 * accordingly with your locationMonitoringFrequency
 */
@property (nonatomic) CLLocationCoordinate2D coordinates;

/**
 * @brief Sets the NSTimeInterval frequency value of the GPS data will be sent to the server
 */
@property (nonatomic, readwrite) NSTimeInterval locationMonitoringFrequency;

/**
 * @brief Use this @property to retrieve the Unique Identifier shared accross your Bitrace's enabled apps, similar to
 * the Apple identifierForVendor but still the same also after an app deletion
 * @return UniqueIdentifier an NSString representing your app's unique identifier for vendor
 */
@property (nonatomic, readonly) NSString *bitraceUniqueIdentifier;

/**
 * @brief Tests automated Bitrace Login verification
 * @param controller your controller where to bind the callBack
 * @return True if can be linked, otherwise you don't need to login
 */
-(BOOL)canLinkFromController:(id)controller;

/**
 * @brief Invoke the automated Bitrace Login verification
 * @param controller your controller where to bind the callBack
 * @return True if can be linked, otherwise you don't need to login
 */
-(BOOL)linkFromController:(id)controller;

/**
 * @brief Unique & easy method to interope with the Bitrace RestFULL WebServices via POST
 * @param method the API method to call
 * @param parameters NSDictionary keys & object to send to the Bitrace RestFULL WebServices
 * @param completionHandler BTRestCallbackCompletionHandler block
 */
-(void)postRestCallbackOnMethod:(NSString *)method withParameters:(NSDictionary *)parameters andCompletionHandler:(BTRestCallbackCompletionHandler)completionHandler;

/**
 * @brief Unique & easy method to interope with the Bitrace RestFULL WebServices via GET
 * @param method the API method to call
 * @param parameters NSDictionary keys & object to send to the Bitrace RestFULL WebServices
 * @param completionHandler BTRestCallbackCompletionHandler block
 */
-(void)getRestCallbackOnMethod:(NSString *)method withParameters:(NSDictionary *)parameters andCompletionHandler:(BTRestCallbackCompletionHandler)completionHandler;

/**
 * @brief Handle and send to Bitrace your app's push notification token
 * @param pushToken your app's push notification token
 */
-(void)handlePushNotificationToken:(NSData *)pushToken;

@end