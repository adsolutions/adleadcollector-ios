//
//  BTRemoteKeychainServer.h
//  Bitrace®
//
//  Created by Daniele Angeli on 15/09/13.
//  Copyright (c) 2013 AD Solutions®. All rights reserved.
//
//  This program and the accompanying materials
//  are made available under the terms of the Bitrace® License
//  which accompanies this distribution, and is available at
//  http://bitrace.aditsolutions.it/license/
//
/// @author Daniele Angeli

#import <Foundation/Foundation.h>

@class BTEngine;

/**
 * @brief The Wallet is responsible to keep the user sensible information that you have retrieved from the Bitrace Remote Keychain, 
 * those informations are AES256 encrypted, it's not safe to store this object for later use
 */
@interface BTWallet : NSObject

/// @brief Retrieve your previous valid Wallet instance if any, it can be nil
+(BTWallet *)myWallet;

/**
 * @brief Initialize a new instance of BTWallet
 * @param userName a username or password for a Service
 * @param password a password for a Service
 * @param token if nil it'ill be used your current applicationToken
 */
-(id)initWithEmailAddress:(NSString *)emailAddress password:(NSString *)password;

/**
 * @brief This method act as a logout, it'll wipe an existing and verified Wallet
 * @param wallet BTWallet instance that store the user login informations
 * @return boolean value to determinate if the wipe was successful
 */
-(BOOL)destroyWallet;

/// @brief emailAddress associated to the selected service
@property (nonatomic, copy, readonly) NSString *emailAddress;

/// @brief password associated to the selected service
@property (nonatomic, copy, readonly) NSString *password;

/// @brief encrypted password associated to the selected service
@property (nonatomic, copy, readonly) NSString *encryptedPassword;

/// @brief companiesArray the companies in which you are subscribed
@property (nonatomic, copy, readonly) NSArray *companiesArray;

/// @brief username associated to the selected service
@property (nonatomic, copy, readonly) NSString *userName;

/// @brief userSurname associated to the selected service
@property (nonatomic, copy, readonly) NSString *userSurname;

/// @brief session associated to the current user domain
@property (nonatomic, copy, readonly) NSString *session;

/// @brief the entire response associated with your user session
@property (nonatomic, copy, readonly) NSDictionary *data;

@end

/**
 * @brief Use this completion Block to receive the login callback
 * @return error Indicates if the callback was handled successfully, you have to
 * dinstinguish between BTErrorDomain and/or NSURLErrorDomain to identify network
 * errors or login errors
 * @return verified BOOL value indicating the success or a failure in the user login callback
 * @return updatedWallet Your fresh Wallet, updated with a private session ID that will let you use the Bitrace RestFULL WebServices
 * and the companies in which you are subscribed
 */
typedef void (^BTVerifyCredentialCompletionHandler)(NSError *error, BOOL verified, BTWallet *updatedWallet);

/** 
 * @brief Use this completion Block to receive the licence Verification callback
 * @return error Indicates if the callback was handled successfully, you have to
 * dinstinguish between BTErrorDomain and/or NSURLErrorDomain to identify network 
 * errors or license errors
 * @return licenseExpirationDate license expiration date, nil if not verified
 * @return currentServerDate Server reference date, nil if not verified
 */
typedef void (^BTLicenseVerificationCompletionHandler)(NSError *error, NSDate *licenseExpirationDate, NSDate *currentServerDate);

/**
 * @brief Use this completion Block to receive your Personal Keychain infos callback
 * @return error Indicates if the callback was handled successfully, you have to
 * dinstinguish between BTErrorDomain and/or NSURLErrorDomain to identify network
 * errors or keychain errors
 * @return userWallet BTWallet instance that stores the user credentials, nil if not success
 */
typedef void (^BTPersonalKeychainCompletionHandler)(NSError *error, BTWallet *userWallet);

FOUNDATION_EXPORT NSString *const BTLoginServiceKey;

/// @brief Brings all of the Enterprise class services from Bitrace Servers right on your hands
@interface BTRemoteKeychainServer : NSObject

/**
 * @brief Use this method to verify the user Login informations
 * @param wallet BTWallet instance that store the user login informations
 * @param completionHandler verifyCredentialCompletionHandler block
 */
-(void)verifyUserWallet:(BTWallet *)wallet withCompletionHandler:(BTVerifyCredentialCompletionHandler)completionHandler;

/**
 * @brief This method is used for verifying if this Device has a valid license to run your App.
 * It's the most usefull & easy to use Enterprise License Server you can adopt, just flag the devices on Bitrace admin panel and that's it!
 * @param completionHandler licenseVerificationCompletionHandler block
 */
-(void)verifyLicenseWithCompletionHandler:(BTLicenseVerificationCompletionHandler)completionHandler __unavailable;

/**
 * @brief Use this method to retrieve your user Username & Password from the online wallet that resides on the Bitrace Remote Keychain Server.
 * As you know, on Bitrace, you and your users are able to store personal Wallet informations associated to a service, so Bitrace can identify your user identity
 * mixing the App token and the device ID associated to the current user and give to you all the informations you want
 * @param serviceName Pass the name of the service from witch you want to retrieve the keychain infos
 * @param completionHandler personalKeychainCompletionHandler block
 */
-(void)retrievePersonalKeychainInformationsForService:(NSString *)serviceName withCompletionHandler:(BTPersonalKeychainCompletionHandler)completionHandler __unavailable;

@end