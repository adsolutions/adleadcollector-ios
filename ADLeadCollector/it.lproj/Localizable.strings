/* 
  Localizable.strings
  LeadsCollector

  Created by Daniele Angeli on 22/04/15.
  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
*/

"kReplicationInProgress" = "Replica in corso...";
"kSettings" = "Impostazioni";
"kLastUpdate%@" = "Ultimo aggiornamento: %@";
"kError" = "Errore";
"kFolderViewControllerTitle" = "Collezioni";
"kCollections" = "Collezioni";
"kLeads" = "Leads";
"kNewLead" = "Nuovo lead";
"kNoLeadsAvailable" = "Nessun lead disponibile\nPremi \"+\" per crearne uno";
"kNoCollectionsAvailable" = "Nessuna collezione disponibile\nPremi \"+\" per crearne una";
"kLoginButton" = "Account Login";
"kBitraceLoginButton" = "Login con Bitrace";
"kDontHaveAnAccount" = "Non hai ancora un account?";
"kSignupHere" = "Crea account";
"kSignup" = "Iscriviti";
"kName" = "Nome";
"kSurname" = "Cognome";
"kEmail" = "Email";
"kPassword" = "Password";
"kCompanyName" = "Azienda";
"kCompanyMessage" = "LASCIARE VUOTO SE NON PRESENTE";
"kMinPasswordChars" = "MINIMO 6 CARATTERI";
"kConnectedAs%@" = "Loggato come %@ - Logout";
"kConnectedAs\n%@" = "Loggato come %@ -\nLogout";
"kAccountDetails" = "Dettagli account";
"kSubmit" = "Invia";
"kCompanyDetails" = "Azienda - LASCIARE VUOTO SE NON PRESENTE";
"kGet-Premium" = "Diventa Premium";
"kRequestPremium" = "Ottieni LC Premium";
"kLCPremiumText" = "Leads Collector Premium ti permette di:\n- Gestire Leads senza nessun limite\n- Lavorare con il tuo Team e condividere i Leads\n- Esportare i Leads verso MailChimp e altri sistemi di DEM\n\nCosa aspetti?";
"kLCFormSendError" = "C'è stato un errore durante l'invio della tua richiesta. Riprovare più tardi";
"kThanksMessage" = "Grazie per l'interesse verso Leads Collector Premium, sarai ricontattato il prima possibile";
"kThanks" = "Grazie!";
"kMessage" = "Messaggio";
"kRequestDetails" = "Dettagli richiesta";
"kDefaultMessage" = "Sono interessato a LC Premium";
"kCompileForm" = "Completa il form";
"kLeadDeletion" = "Eliminazione Lead";
"kLeadDeletionMessage" = "Sei sicuro di voler eliminare questo Lead? Perderai anche eventuali foto e note vocali associate";
"kGet-PremiumInfo" = "Hai raggiunto il limite massimo di Leads collezionabili (10). Per poter aggiungere altri Leads devi richiedere la versione Premium";
"kGet-PremiumExplanation" = "Le feature dell'Unlimited più un set di Tools a cui non puoi rinunciare!";
"kLeadSavedSuccessfully" = "Lead salvato con successo";
"kNoLeads" = "Nessun Lead";
"kNoLeads%@" = "%@ - Nessun Lead";
"kSearchLeads" = "Cerca Leads";
"kAbout" = "About";
"kUpgrade" = "Upgrade!";
"kUpgradeYourLC" = "Potenzia LeadsCollector";
"kGetUnlimited%@" = "Passa a Unlimited per %@";
"kHaveATour" = "Guarda il tour";
"kPurchased" = "Comprato";
"kGotIt" = "Lo hai preso!";
"kThankYou" = "Grazie";
"kEnjoyYourUnlimitedPlan" = "Ora hai Leads senza limiti!";
"kAboutLC" = "Realizzato con amore in Italia e orgogliosamente offerto da AD Solutions, la software house dietro a MobileBridge® e Bitrace®, servizi per l'enterprise.";
"kAboutMB" = "LeadsCollector® poggia sull'infrastruttura Cloud di MobileBridge® per offrire la migliore esperienza utente possibile in un contesto sicuro, cross-platform e affidabile.";
"kGoToWebsite" = "Visita il nostro Sito";
"kInitializing..." = "Inizializzazione...";
"kUnassigned" = "Non assegnati";
"kPickAColor" = "Scegli un colore";
"kYou" = "Tu";

/* LC Premium */
"kUnlimitedLeads" = "Leads illimitati, nel Cloud";
"kUnlimitedLeadsText" = "Mai più limiti sui Leads collezionabili.\nTutti i tuoi Leads verranno salvati in totale sicurezza all'interno del nostro Cloud.";
"kCustomizableForms" = "Form tagliati su misura";
"kCustomizableFormsText" = "Immagina un form. Avremo il piacere di realizzarlo.";
"kShareIntro" = "Lavora in team";
"kShareIntroText" = "Unisciti al tuo team di lavoro e condividete i vostri Leads.";
"kTouchIDIntro" = "Integrato con TouchID";
"kTouchIDIntroText" = "Blocca e mantieni al sicuro\ntutti i preziosi dati dei tuoi Leads.";

/* AlertView */
"kAlertViewAddFolderTitle" = "Aggiungi Collezione";
"kAlertViewAddFolderMessage" = "Inserisci il nome della nuova Collezione";
"kCancel" = "Annulla";
"kAlertViewAddFolderOKButton" = "OK";
"kAlertViewAddFolderSaveButton" = "Salva";
"kTotalLeads%ui" = "Hai %i Leads";
"kTotalLead%ui" = "Hai %i Lead";
"kTotalLeadsNoLeads" = "Nessun Lead";
"kLoadingLeads" = "Carico i Leads...";
"kLeadAddedSuccessfully" = "Lead aggiunto con successo";
"kLeadAdd" = "Inserimento Lead";
"kRenameFolder" = "Rinomina";
"kRenameFolder%@" = "Rinomina %@ con...";
"kMore" = "Altro...";
"kAreYouSureToDelete%@?" = "Sei sicuro di voler eliminare la collezione %@?";
"kDelete" = "Elimina";
"kLogoutMessage" = "Sei sicuro di voler effettuare il logout da Leads Collector?";
"kLogout" = "Logout";
"kFormFillNameSurnameError" = "Il campo nome / cognome è obbligatorio";
"kPolicyError" = "Per procedere al salvataggio devi acconsentire alle norme sulla privacy";
"kReplicationError%@" = "Errore durante la replica dei dati - %@";

/* ActionSheet */
"kDeleteFolder" = "Elimina collezione e contenuto";
"kShare" = "Condividi";
"kChooseAnOption" = "Scegli una opzione";

/* 1.1 */
"kAnalytics" = "Insights";
"kYourTeamPerformance" = "Ecco le performance del tuo Team";
"kInviteTeamMate" = "Invita un nuovo compagno al tuo Team";
"kLoginError" = "Errore di Login";
"kTeammates" = "Compagni di squadra";
"kTotalLeads" = "Leads totali: ";
"kNoTeamPerformanceAvailable" = "Non ci sono dati disponibili per il tuo Team";

/* 1.1.1 */
"kSupport" = "Supporto";
"kCannotBuyProduct" = "C'è stato un errore con la tua transazione, riprova più tardi";
"kErrorRetrievingProduct" = "Impossibile ottenere l'elemento";
"kRestore" = "o Ripristina";

/* 1.2 */
"kTeammatesActivity" = "Attività del Team";
"kEver" = "Tutti";
"k7Days" = "7 Giorni";
"k30Days" = "30 Giorni";
"kQuarter" = "4 Mesi";
"kWrongNumberError" = "Numero di telefono errato";
"kWelcome" = "Benvenuto";
"kDeleteRecording" = "Elimina nota vocale";
"kVoiceNote" = "Nota vocale";
"kConnectionError" = "La connessione a Internet sembra essere offline";
"kAttention" = "Attenzione";
"kUnsaved" = "Hai modifiche non salvate. Sei sicuro di voler annullare?";
"kRequired%@" = "Il campo %@ è obbligatorio";
"kYes" = "Si";
"kNo" = "No";
"kSignupError" = "Errore di registrazione";
"kUploading" = "Upload in corso...";
"kUploadFailed" = "Upload fallito, riprova più tardi";
"kRetry" = "Riprova";
"kPlayAnyway" = "Riproduci lo stesso";
"kPreviousUploadFailedFound" = "E' stato trovato un upload non completato o in errore, cosa vuoi fare?";
"kDeletePicture" = "Elimina foto";
"kNetworkErrorWhileDownloading" = "E' stato riscontrato un errore di rete che impedisce il download della risorsa richiesta. Riprova più tardi";
"kResourceCurrentlyUnavailable" = "La risorsa desiderata non è al momento disponibile all'interno dei nostri sistemi. Riprova più tardi";
"kShowAnyway" = "Visualizza lo stesso";
"LCAudioRecorderViewController-Title" = "Registra Audio";
"LCAudioRecorderViewController-Subtitle" = "Permetti l'accesso al tuo microfono per iniziare a registrare con questa App";
"LCAudioRecorderViewController-Step1" = "1. Apri le impostazioni del tuo device";
"LCAudioRecorderViewController-Step2" = "2. Vai su Privacy";
"LCAudioRecorderViewController-Step3" = "3. Vai su Microfono";
"LCAudioRecorderViewController-Step4" = "4. Attiva Leads Collector";
"kDownloadFailed" = "Download fallito, riprova più tardi";
"kFileNotFound" = "La risorsa desiderata non esiste più all'interno dei nostri sistemi";
"kSearchIn%@" = "Cerca in %@";
"kOverwriteAndProceed" = "Sovrascrivi e procedi lo stesso";