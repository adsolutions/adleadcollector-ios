//
//  LCLockScreenViewController.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 21/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCLockScreenViewController.h"
#import "WBTouchID.h"

#import <Crashlytics/Crashlytics.h>

@interface LCLockScreenViewController ()

@end

@implementation LCLockScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    [WBTouchID validateTouchId:^(BOOL success,NSError *err){
        if (success) {
            CLS_LOG(@"Autheticated successfully");
        } else {
            CLS_LOG(@"Failed to authenticate: %@",err.localizedDescription);
        }
    }];
}

@end