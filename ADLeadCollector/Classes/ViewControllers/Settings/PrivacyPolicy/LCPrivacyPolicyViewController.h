//
//  LCPrivacyPolicyViewController.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 14/05/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCPrivacyPolicyViewController : UIViewController

@property (nonatomic) BOOL standardPolicy;

@end