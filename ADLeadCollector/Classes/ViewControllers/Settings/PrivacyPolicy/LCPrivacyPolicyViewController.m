//
//  LCPrivacyPolicyViewController.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 14/05/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCPrivacyPolicyViewController.h"

@interface LCPrivacyPolicyViewController ()

@property (nonatomic, weak) IBOutlet UITextView *textView;

@end

@implementation LCPrivacyPolicyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    if (_standardPolicy) {
        [_textView setText:[LCUtils privacyPolicyForCompany:@"AD Solutions di Angeli Daniele"]];
    }
    else
        [_textView setText:[LCUtils privacyPolicyForCompany:[BTWallet myWallet].companiesArray.firstObject[@"company"]]];
}

@end