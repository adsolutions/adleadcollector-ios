//
//  LCAboutViewController.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 25/05/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCViewController.h"

@interface LCAboutViewController : LCViewController

@end