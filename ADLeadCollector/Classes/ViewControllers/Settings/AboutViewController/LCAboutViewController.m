//
//  LCAboutViewController.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 25/05/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCAboutViewController.h"
#import "LCFolder.h"

@interface LCAboutViewController ()

@property (nonatomic, strong) UILabel *aboutLCLabel;
@property (nonatomic, strong) UILabel *aboutMBLabel;
@property (nonatomic, strong) UIButton *goToWebsiteButton;
@property (nonatomic, strong) UIImageView *lcImageView;
@property (nonatomic, strong) UIImageView *mbImageView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *contentView;

@end

@implementation LCAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"kAbout", @"");
    
    self.view.backgroundColor = FlatBlack;
    
    _scrollView = [[UIScrollView alloc] initForAutoLayout];
    [_scrollView setScrollEnabled:YES];
    [_scrollView setUserInteractionEnabled:YES];
    [_scrollView setExclusiveTouch:NO];
    [self.view addSubview:_scrollView];
    [_scrollView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
    
    [_scrollView setContentInset:UIEdgeInsetsMake(self.navigationController.navigationBar.frame.size.height, 0, 0, 0)];
    
    _contentView = [[UIView alloc] initForAutoLayout];
    [_scrollView addSubview:_contentView];
    
    [_contentView setUserInteractionEnabled:NO];
    [_contentView setExclusiveTouch:NO];
    
    [_contentView autoSetDimension:ALDimensionWidth toSize:self.view.frame.size.width];
    
    NSString *buildLocalNumber = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *appLocalVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    NSMutableAttributedString *aboutString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"kAboutLC", @"") attributes:@{NSFontAttributeName : [LCUtils fontWithSize:18], NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [aboutString insertAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n\nVersion %@ (%@)", appLocalVersion, buildLocalNumber] attributes:@{NSFontAttributeName : [LCUtils fontWithSize:12], NSForegroundColorAttributeName : [UIColor whiteColor]}] atIndex:aboutString.length];
    
    _aboutLCLabel = [[UILabel alloc] initForAutoLayout];
    [_aboutLCLabel setNumberOfLines:0];
    [_aboutLCLabel setAttributedText:aboutString];
    [_aboutLCLabel setTextAlignment:NSTextAlignmentCenter];
    [_contentView addSubview:_aboutLCLabel];
    
    _aboutMBLabel = [[UILabel alloc] initForAutoLayout];
    [_aboutMBLabel setText:NSLocalizedString(@"kAboutMB", @"")];
    [_aboutMBLabel setNumberOfLines:0];
    [_aboutMBLabel setFont:[LCUtils fontWithSize:18]];
    [_aboutMBLabel setTextColor:[UIColor whiteColor]];
    [_aboutMBLabel setTextAlignment:NSTextAlignmentCenter];
    [_contentView addSubview:_aboutMBLabel];
    
    _lcImageView = [[UIImageView alloc] initForAutoLayout];
    [_lcImageView setImage:[UIImage imageNamed:@"LeadsCollectorGreen"]];
    [_lcImageView setContentMode:UIViewContentModeCenter];
    [_contentView addSubview:_lcImageView];
    [_lcImageView autoSetDimensionsToSize:CGSizeMake(self.contentView.frame.size.width, 150)];
    [_lcImageView autoAlignAxis:ALAxisVertical toSameAxisOfView:_contentView];
    [_lcImageView autoPinEdgeToSuperviewMargin:ALEdgeTop];
    
    [_aboutLCLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_lcImageView withOffset:0];
    [_aboutLCLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:_contentView withOffset:10];
    [_aboutLCLabel autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:_contentView withOffset:-10];
    
    _mbImageView = [[UIImageView alloc] initForAutoLayout];
    [_mbImageView setImage:[UIImage imageNamed:@"mobileBridge"]];
    [_mbImageView setContentMode:UIViewContentModeCenter];
    [_contentView addSubview:_mbImageView];
    [_mbImageView autoSetDimensionsToSize:CGSizeMake(self.contentView.frame.size.width, 150)];
    [_mbImageView autoAlignAxis:ALAxisVertical toSameAxisOfView:_contentView];
    [_mbImageView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_aboutLCLabel withOffset:30];
    
    [_aboutMBLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_mbImageView];
    [_aboutMBLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:_contentView withOffset:10];
    [_aboutMBLabel autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:_contentView withOffset:-10];
    
//    _goToWebsiteButton = [[UIButton alloc] initForAutoLayout];
//    [_goToWebsiteButton setTitle:NSLocalizedString(@"kGoToWebsite", @"") forState:UIControlStateNormal];
//    [_contentView addSubview:_goToWebsiteButton];
//    [_goToWebsiteButton setBackgroundColor:companyColor()];
//    [_goToWebsiteButton addTarget:self action:@selector(goToWebSiteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
//    [_goToWebsiteButton.titleLabel setFont:[LCUtils fontWithSize:22]];
//    [_goToWebsiteButton.layer setCornerRadius:5];
//    [_goToWebsiteButton setShowsTouchWhenHighlighted:YES];
//    [_goToWebsiteButton setExclusiveTouch:YES];
//    [_goToWebsiteButton setUserInteractionEnabled:YES];
//    [_goToWebsiteButton autoSetDimension:ALDimensionHeight toSize:40];
//    [_goToWebsiteButton autoSetDimension:ALDimensionWidth toSize:225];
//    [_goToWebsiteButton autoAlignAxis:ALAxisVertical toSameAxisOfView:_contentView];
//    [_goToWebsiteButton autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_aboutMBLabel withOffset:30];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [_contentView autoSetDimension:ALDimensionWidth toSize:self.view.frame.size.width];
    [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, 600)];
}

#pragma mark - Actions

-(void)goToWebSiteButtonPressed:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.leadscollector.com"]];
}

@end