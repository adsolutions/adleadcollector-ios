//
//  LCUpgradeViewController.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 24/05/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCUpgradeViewController.h"

#import <MAFormViewController/MAFormField.h>
#import <EAIntroView/EAIntroView.h>
#import <AFNetworking/AFNetworking.h>
#import <CRToast/CRToast.h>

#import "MKStoreManager.h"
#import "LCSettings.h"
#import "LCAlwaysTemplateImage.h"
#import "LCFormViewController.h"

@interface LCUpgradeViewController () <EAIntroDelegate>

@property (nonatomic, strong) UIImageView *cloudLeadsImageView;
@property (nonatomic, strong) UIImageView *premiumImageView;
@property (nonatomic, strong) UIButton *unlimitedButton;
@property (nonatomic, strong) UIButton *haveATourButton;
@property (nonatomic, strong) UIButton *restorePurchase;
@property (nonatomic, strong) UILabel *unlimitedLabel;
@property (nonatomic, strong) UILabel *haveATourLabel;

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@end

@implementation LCUpgradeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productFetchedNotification:) name:kProductFetchedNotification object:nil];
    
    UIImage *image = [LCAlwaysTemplateImage alwaysTemplateImageNamed:@"cloudLeads"];
    _cloudLeadsImageView = [[UIImageView alloc] initForAutoLayout];
    [self.view addSubview:_cloudLeadsImageView];
    [_cloudLeadsImageView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(50, 0, 0, 0) excludingEdge:ALEdgeBottom];
    [_cloudLeadsImageView setImage:image];
    [_cloudLeadsImageView setContentMode:UIViewContentModeCenter];
    [_cloudLeadsImageView setTintColor:companyColor()];
    
    image = [LCAlwaysTemplateImage alwaysTemplateImageNamed:@"get-premium"];
    [_premiumImageView setImage:image];
    [_premiumImageView setTintColor:companyColor()];
    
    [_haveATourButton setTitle:NSLocalizedString(@"kHaveATour", @"") forState:UIControlStateNormal];
    
    _unlimitedLabel = [[UILabel alloc] initForAutoLayout];
    [_unlimitedLabel setText:[[NSLocalizedString(@"kUnlimitedLeads", @"") stringByAppendingString:@"\n"] stringByAppendingString:NSLocalizedString(@"kUnlimitedLeadsText", @"")]];
    [self.view addSubview:_unlimitedLabel];
    [_unlimitedLabel setNumberOfLines:0];
    [_unlimitedLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_cloudLeadsImageView];
    [_unlimitedLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view];
    [_unlimitedLabel autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.view];
    [_unlimitedLabel setFont:[LCUtils fontWithSize:16]];
    [_unlimitedLabel setTextAlignment:NSTextAlignmentCenter];
    [_unlimitedLabel setTextColor:[UIColor whiteColor]];
    
    [_haveATourLabel setText:[[NSLocalizedString(@"kGet-Premium", @"") stringByAppendingString:@"\n"] stringByAppendingString:NSLocalizedString(@"kGet-PremiumExplanation", @"")]];
    
    self.title = NSLocalizedString(@"kUpgradeYourLC", @"");
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed:)]];
    
    self.view.backgroundColor = FlatBlack;
    
    _unlimitedButton = [[UIButton alloc] initForAutoLayout];
    [self.view addSubview:_unlimitedButton];
    [_unlimitedButton autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_unlimitedLabel withOffset:10];
    [_unlimitedButton autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [_unlimitedButton autoSetDimensionsToSize:CGSizeMake(200, 40)];
    [_unlimitedButton setBackgroundColor:FlatGreen];
    [_unlimitedButton.layer setCornerRadius:8];
    [_unlimitedButton addTarget:self action:@selector(getUnlimitedButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    _restorePurchase = [[UIButton alloc] initForAutoLayout];
    [self.view addSubview:_restorePurchase];
    [_restorePurchase autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_unlimitedButton];
    [_restorePurchase autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view];
    [_restorePurchase autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.view];
    [_restorePurchase setTitleColor:FlatGreen forState:UIControlStateNormal];
    [_restorePurchase setTitle:NSLocalizedString(@"kRestore", @"") forState:UIControlStateNormal];
    [_restorePurchase addTarget:self action:@selector(restoreButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.unlimitedButton addSubview:_indicatorView];
    [_indicatorView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_indicatorView autoPinEdgesToSuperviewMargins];
    [_indicatorView autoSetDimensionsToSize:CGSizeMake(40, 40)];
    [_indicatorView setColor:[UIColor whiteColor]];
    [_indicatorView startAnimating];
}

-(void)viewWillAppear:(BOOL)animated
{
    [MKStoreManager sharedManager];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self checkPurchased];
}

-(void)checkPurchased
{
    if ([MKStoreManager isFeaturePurchased:kLCUnlimitedPurchaseID]) {
        [_unlimitedButton setEnabled:NO];
        [_unlimitedButton setBackgroundColor:FlatGray];
        
        UILabel *purchasedLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 50)];
        [purchasedLabel setText:NSLocalizedString(@"kPurchased", @"")];
        [purchasedLabel setBackgroundColor:ClearColor];
        [purchasedLabel setTextAlignment:NSTextAlignmentCenter];
        [purchasedLabel setMinimumScaleFactor:0.10f];
        [purchasedLabel setFont:[LCUtils boldFontWithSize:26]];
        [purchasedLabel setTextColor:FlatRed];
        [purchasedLabel.layer setBorderWidth:4];
        [purchasedLabel.layer setBorderColor:FlatRed.CGColor];
        [purchasedLabel.layer setOpacity:1];
        [purchasedLabel.layer setCornerRadius:10];
        [purchasedLabel setCenter:CGPointMake(_cloudLeadsImageView.center.x, _cloudLeadsImageView.center.y+30)];
        [self.view addSubview:purchasedLabel];
        
        purchasedLabel.transform = CGAffineTransformMakeScale(4, 4);
        
        [UIView animateWithDuration:0.25f delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            purchasedLabel.transform = CGAffineTransformIdentity;
            purchasedLabel.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-30));
        } completion:^(BOOL finished) {
            [_indicatorView stopAnimating];
            [_indicatorView removeFromSuperview];
            [_unlimitedButton setHidden:NO];
            
            [_unlimitedButton setTitle:NSLocalizedString(@"kGotIt", @"") forState:UIControlStateDisabled];
            
            [_restorePurchase setHidden:YES];
            
            LCSettings *setting = [LCSettings findFirst];
            setting.LCPremiumToken = @"kPurchasedViaiCloud";
            [setting saveDocument];
        }];
    }
    else
    {
        [self productFetchedNotification:nil];
    }
}

#pragma mark - EAIntroDelegate

-(void)introDidFinish:(EAIntroView *)introView
{
    MAFormField *nameField = [MAFormField fieldWithKey:@"name" type:MATextFieldTypeName initialValue:@""placeholder:NSLocalizedString(@"kName", @"") required:YES];
    MAFormField *surnameField = [MAFormField fieldWithKey:@"surname" type:MATextFieldTypeName initialValue:@""placeholder:NSLocalizedString(@"kSurname", @"") required:YES];
    MAFormField *emailField = [MAFormField fieldWithKey:@"email" type:MATextFieldTypeEmail initialValue:@"" placeholder:NSLocalizedString(@"kEmail", @"") required:YES];
    MAFormField *messageField = [MAFormField fieldWithKey:@"message" type:MATextFieldTypeDefault initialValue:NSLocalizedString(@"kDefaultMessage", @"") placeholder:NSLocalizedString(@"kMessage", @"") required:NO];
    MAFormField *companyNameField = [MAFormField fieldWithKey:@"company" type:MATextFieldTypeDefault initialValue:@"" placeholder:NSLocalizedString(@"kCompanyName", @"") required:NO];
    
    // separate the cells into sections
    NSArray *firstSection = @[nameField, surnameField, emailField, companyNameField, messageField];
    NSArray *cellConfig = @[firstSection];
    NSArray *sectionsHeader = @[NSLocalizedString(@"kRequestDetails", @"")];
    
    __block UINavigationController *navigationController = nil;
    
    // create the form and present it modally with its own navigation controller
    __block LCFormViewController *formVC = [[LCFormViewController alloc] initWithCellConfigurations:cellConfig actionText:NSLocalizedString(@"kSubmit", @"") animatePlaceholders:YES handler:^(NSDictionary *resultDictionary) {
        
        // if we don't have a result dictionary, the user cancelled, rather than submitted the form
        if (!resultDictionary) {
            [self dismissViewControllerAnimated:YES completion:nil];
            return;
        }
        
        AFHTTPRequestOperationManager *requestManager = [AFHTTPRequestOperationManager manager];
        requestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        [requestManager POST:@"http://leadscollector.aditsolutions.it/wp-admin/admin-ajax.php" parameters:@{@"action" : @"lc_premium", @"lc_contact_action" : @"send", @"email" : resultDictionary[@"email"], @"name" : resultDictionary[@"name"], @"surname" : resultDictionary[@"surname"], @"company" : resultDictionary[@"company"], @"message" : resultDictionary[@"message"]} success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
            if ([responseObject[@"success"] boolValue]) {
                [navigationController dismissViewControllerAnimated:YES completion:^{
                    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"kThanks", @"") message:NSLocalizedString(@"kThanksMessage", @"") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                }];
            }
            else
            {
                [formVC reset];
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"kError", @"") message:NSLocalizedString(@"kLCFormSendError", @"") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [formVC reset];
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"kError", @"") message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }];
    }];
    
    [formVC setSectionsHeader:sectionsHeader];
    [formVC.navigationItem setTitle:NSLocalizedString(@"kCompileForm", @"")];
    
    navigationController = [[UINavigationController alloc] initWithRootViewController:formVC];
    [self presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark - Actions

-(void)doneButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)haveATourButtonPressed:(id)sender
{
    EAIntroPage *page1 = [EAIntroPage page];
    UIImage *image = [LCAlwaysTemplateImage alwaysTemplateImageNamed:@"cloudLeads"];
    page1.titleIconView = [[UIImageView alloc] initWithImage:image];
    [page1.titleIconView setTintColor:companyColor()];
    page1.titleIconPositionY = self.view.bounds.size.height/4;
    page1.titlePositionY = self.view.bounds.size.height/3 -30;
    page1.descPositionY = self.view.bounds.size.height/4;
    page1.title = NSLocalizedString(@"kUnlimitedLeads", @"");
    page1.desc = NSLocalizedString(@"kUnlimitedLeadsText", @"");
    
    EAIntroPage *page2 = [EAIntroPage page];
    image = [LCAlwaysTemplateImage alwaysTemplateImageNamed:@"customizable"];
    page2.titleIconView = [[UIImageView alloc] initWithImage:image];
    [page2.titleIconView setTintColor:companyColor()];
    page2.titleIconPositionY = self.view.bounds.size.height/4;
    page2.titlePositionY = self.view.bounds.size.height/3 -30;
    page2.descPositionY = self.view.bounds.size.height/4;
    page2.title = NSLocalizedString(@"kCustomizableForms", @"");
    page2.desc = NSLocalizedString(@"kCustomizableFormsText", @"");
    
    EAIntroPage *page3 = [EAIntroPage page];
    image = [LCAlwaysTemplateImage alwaysTemplateImageNamed:@"share"];
    page3.titleIconView = [[UIImageView alloc] initWithImage:image];
    [page3.titleIconView setTintColor:companyColor()];
    page3.titleIconPositionY = self.view.bounds.size.height/4;
    page3.titlePositionY = self.view.bounds.size.height/3 -30;
    page3.descPositionY = self.view.bounds.size.height/4;
    page3.title = NSLocalizedString(@"kShareIntro", @"");
    page3.desc = NSLocalizedString(@"kShareIntroText", @"");
    
    EAIntroPage *page4 = [EAIntroPage page];
    image = [LCAlwaysTemplateImage alwaysTemplateImageNamed:@"touchid"];
    page4.titleIconView = [[UIImageView alloc] initWithImage:image];
    [page4.titleIconView setTintColor:companyColor()];
    page4.titleIconPositionY = self.view.bounds.size.height/4;
    page4.titlePositionY = self.view.bounds.size.height/3 -30;
    page4.descPositionY = self.view.bounds.size.height/4;
    page4.title = NSLocalizedString(@"kTouchIDIntro", @"");
    page4.desc = NSLocalizedString(@"kTouchIDIntroText", @"");
    
    EAIntroView *intro = [[EAIntroView alloc] initWithFrame:self.view.frame andPages:@[page1, page2, page3, page4]];
    [intro setDelegate:self];
    intro.showSkipButtonOnlyOnLastPage = true;
    [intro.skipButton setTitle:NSLocalizedString(@"kGet-Premium", @"") forState:UIControlStateNormal];
    intro.backgroundColor = ContrastColor(companyColor(), YES);
    
    [intro showInView:self.view animateDuration:0.3];
}

-(void)getUnlimitedButtonPressed:(id)sender
{
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [_indicatorView setFrame:[(UIButton *)sender frame]];
    [_indicatorView setColor:companyColor()];
    [self.view addSubview:_indicatorView];
    [_indicatorView startAnimating];
    [sender setHidden:YES];
    
    [[MKStoreManager sharedManager] buyFeature:kLCUnlimitedPurchaseID onComplete:^(NSString *purchasedFeature, NSData *purchasedReceipt, NSArray *availableDownloads) {
        
        LCSettings *settings = [LCSettings findFirst];
        [settings setUnlimitedPurchased:YES];
        [settings saveDocument];
        
        [CRToastManager showNotificationWithOptions:[LCUtils successToastWithTitle:NSLocalizedString(@"kThankYou", @"") andMessage:NSLocalizedString(@"kEnjoyYourUnlimitedPlan", @"")] completionBlock:^{
            [self checkPurchased];
            
            [_indicatorView stopAnimating];
            [_indicatorView removeFromSuperview];
            [sender setHidden:NO];
            
            // Answer
            [Answers logPurchaseWithPrice:[NSDecimalNumber decimalNumberWithString:@"0.99"]
                                 currency:@"USD"
                                  success:@YES
                                 itemName:@"Unlimited"
                                 itemType:@"In-App Purchase"
                                   itemId:@"it.aditsolutions.leadsCollectorUnlimited"
                         customAttributes:@{}];
        }];
    } onCancelled:^{
        [CRToastManager showNotificationWithOptions:[LCUtils errorToastWithTitle:NSLocalizedString(@"kError", @"") andMessage:NSLocalizedString(@"kCannotBuyProduct", @"")] completionBlock:^{
            [self checkPurchased];
            
            [_indicatorView stopAnimating];
            [_indicatorView removeFromSuperview];
            [sender setHidden:NO];
            
            // Answer
            [Answers logPurchaseWithPrice:[NSDecimalNumber decimalNumberWithString:@"0.99"]
                                 currency:@"USD"
                                  success:@NO
                                 itemName:@"Unlimited"
                                 itemType:@"In-App Purchase"
                                   itemId:@"it.aditsolutions.leadsCollectorUnlimited"
                         customAttributes:@{}];
        }];
    }];
}

-(void)productFetchedNotification:(NSNotification *)notification
{
    BOOL finded = false;
    
    for (SKProduct *product in [[MKStoreManager sharedManager] purchasableObjects]) {
        if ([[product productIdentifier] isEqualToString:kLCUnlimitedPurchaseID]) {
            NSString *price = [[[MKStoreManager sharedManager] pricesDictionary] objectForKey:kLCUnlimitedPurchaseID];
            [_unlimitedButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"kGetUnlimited%@", @""), price] forState:UIControlStateNormal];
            [_unlimitedButton setTitle:NSLocalizedString(@"kGotIt", @"") forState:UIControlStateDisabled];
            
            finded = true;
        }
    }
    
    if (!finded && notification) {
        [_unlimitedButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"kGetUnlimited%@", @""), @"$0,99"] forState:UIControlStateNormal];
    }
    else
    {
        [_indicatorView stopAnimating];
        [_indicatorView removeFromSuperview];
        [_unlimitedButton setHidden:NO];
    }
}

-(void)restoreButtonPressed:(id)sender
{
    [[MKStoreManager sharedManager] restorePreviousTransactionsOnComplete:^{
        [self checkPurchased];
    } onError:^(NSError *error) {
        [self checkPurchased];
    }];
}

@end