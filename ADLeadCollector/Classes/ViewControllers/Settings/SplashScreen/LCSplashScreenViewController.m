//
//  LCSplashScreenViewController.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 28/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCSplashScreenViewController.h"
#import "LCRootViewController.h"

#import <CBZSplashView/CBZSplashView.h>

@interface LCSplashScreenViewController ()

@property (nonatomic, strong) CBZSplashView *splashView;

@end

@implementation LCSplashScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.splashView = [CBZSplashView splashViewWithBezierPath:[LCUtils lcSplashBezier] backgroundColor:FlatGreen];
    [self.splashView setAnimationDuration:1];
    [self.view addSubview:self.splashView];
    [self.view setBackgroundColor:[UIColor whiteColor]];
}

-(void)viewDidAppear:(BOOL)animated
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.splashView startAnimationWithCompletionHandler:^{
            [self.view setBackgroundColor:FlatGreen];
            LCRootViewController *rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LCRootViewController"];
            [rootViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self presentViewController:rootViewController animated:YES completion:nil];
        }];
    });
}

@end