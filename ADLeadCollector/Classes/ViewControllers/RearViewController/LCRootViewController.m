//
//  BTRootViewController.m
//  BitraceApp
//
//  Created by Daniele Angeli on 13/07/14.
//  Copyright (c) 2014 ADSolutions. All rights reserved.
//

#import "LCRootViewController.h"
#import "LCNavigationController.h"
#import "LCLeftViewController.h"
#import "LCWelcomeViewController.h"
#import "LCSettings.h"

#import <ReactiveCocoa/ReactiveCocoa.h>
#import <SWFSemanticVersion/SWFSemanticVersion.h>
#import <JDStatusBarNotification/JDStatusBarNotification.h>
#import <NSString+Validation/NSString+Validation.h>

NSString *const BTAppBuildVersion = @"kAppBuildVersion";
NSString *const BTAppVersion = @"kAppVersion";

@interface LCRootViewController () <LCLoginViewControllerDelegate, MBEngineDelegate>

@property (nonatomic, strong) NSDictionary *tempUserData;
@property (nonatomic, getter=isLoggedIn) BOOL loggedIn;

@end

@implementation LCRootViewController

-(void)awakeFromNib
{
    self.menuPreferredStatusBarStyle = UIStatusBarStyleLightContent;
    self.contentViewShadowColor = [UIColor blackColor];
    self.contentViewShadowOffset = CGSizeMake(0, 0);
    self.contentViewShadowOpacity = 0.6;
    self.contentViewShadowRadius = 12;
    self.contentViewShadowEnabled = YES;
    [self setParallaxEnabled:NO];
    
    self.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftMenuViewController"];
    
    if (![BTWallet myWallet].emailAddress.length > 0 || [self checkLocalVersion])
    {
        [self presentLoginController];
    }
    else
    {
        self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LCFoldersCollectionViewController"];
        [self.contentViewController.view setBackgroundColor:companyColor()];
        
        if ([BTWallet myWallet].emailAddress.length > 0) {
            self.loggedIn = true;
            
            [self initMobileBridge];
            
            [self startSubscribingToEvents];
            
            [LCAWSManager sharedManager];
        }
        else
        {
            [JDStatusBarNotification showWithStatus:NSLocalizedString(@"kSignupError", @"") dismissAfter:5 styleName:JDStatusBarStyleError];
            [CRToastManager dismissNotification:YES];
            [self presentLoginController];
        }
    }
    
    [[self leftMenuViewController].view setBackgroundColor:companyColor()];
    
    self.delegate = self;
    
    [super awakeFromNib];
    
    [self.view setBackgroundColor:companyColor()];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewDidAppear:(BOOL)animated
{
    if (![BTWallet myWallet].emailAddress.length > 0 || [self checkLocalVersion])
    {
        [self presentLoginController];
    }
    
    [(LCLeftViewController *)self.leftMenuViewController setWallet:[BTWallet myWallet]];
}

#pragma mark - RAC

-(void)startSubscribingToEvents
{
    [[RACObserve([MBEngine sharedEngine].pull, completedChangesCount) throttle:3 valuesPassingTest:^BOOL(id next) {
        if (next) {
            return YES;
        }
        
        return NO;
    }] subscribeNext:^(id x) {
        if (!self.isLoggedIn && (([x floatValue] - [[MBEngine sharedEngine].pull changesCount] == 0) || ([[MBEngine sharedEngine].pull changesCount] == 0))) {
            if ([BTWallet myWallet]) {
                [self firstSyncEnded];
            }
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateNotificationKey object:kUpdateNotificationKey];
    }];
}

#pragma mark - Login

-(void)presentLoginController
{
    _loggedIn = NO;
    
    LCWelcomeViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LCWelcomeViewController"];
    [viewController setDelegate:self];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [navigationController setNavigationBarHidden:YES];
    
    self.contentViewController = navigationController;
    [self.contentViewController.view setBackgroundColor:companyColor()];
}

#pragma mark - BTLoginViewControllerDelegate

-(void)loginViewController:(id)viewController didAuthenticateUserWithWallet:(BTWallet *)wallet wasSubscribing:(BOOL)subscribing andData:(NSDictionary *)userData
{
    [CRToastManager showNotificationWithOptions:[LCUtils infoToastWithMessage:NSLocalizedString(@"kInitializing...", @"") andTimeout:INT16_MAX] completionBlock:nil];
    
    [viewController dismissViewControllerAnimated:YES completion:^{
        self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LCFoldersCollectionViewController"];
        
        _tempUserData = userData;
        
        if (wallet) {
            [self initMobileBridge];
            
            [kUserDefaults setBool:YES forKey:@"kInstalled"];
            [kUserDefaults synchronize];
            
            [self startSubscribingToEvents];
            
            [LCAWSManager sharedManager];
        }
        else
        {
            [JDStatusBarNotification showWithStatus:NSLocalizedString(@"kSignupError", @"") dismissAfter:5 styleName:JDStatusBarStyleError];
            [CRToastManager dismissNotification:YES];
            [self presentLoginController];
        }
        
        NSDictionary *attributes = [LCUtils answersUserDetails];
        
        if (subscribing) {
            // Answers
            [Answers logSignUpWithMethod:@"Bitrace" success:@YES customAttributes:attributes];
        }
        else
        {
            // Answers
            [Answers logLoginWithMethod:@"Bitrace" success:@YES customAttributes:attributes];
        }
        
        [kUserDefaults setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] forKey:@"CFBundleVersion"];
        [kUserDefaults setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forKey:@"CFBundleShortVersionString"];
        [kUserDefaults synchronize];
    }];
}

-(void)firstSyncEnded
{
    if (![[NSThread currentThread] isMainThread]) {
        [self performSelectorOnMainThread:@selector(firstSyncEnded) withObject:nil waitUntilDone:YES];
        return;
    }
    
    _loggedIn = true;
    
    LCSettings *userSettings = [LCSettings findFirst];
    if (!userSettings) {
        userSettings = [LCSettings createForPersonalUse];
    }
    
    NSString *companyColor = _tempUserData[@"data"][@"LCCompanyColor"];
    if ([NSString isEmptyString:companyColor]) {
        companyColor = @"#2ecc71";
    }
    
    [userSettings setCompanyColor:companyColor];
    
    [kUserDefaults setBool:[_tempUserData[@"data"][@"LCEnterprise"] boolValue] forKey:@"LCEnterprise"];
    [kUserDefaults setObject:companyColor forKey:@"it.aditsolutions.LeadsCollector.companyColor"];
    [kUserDefaults synchronize];
    
    [userSettings setLCPremiumToken:_tempUserData[@"data"][@"LCPremiumToken"]];
    [userSettings saveDocument];
    
    [LCUtils applyStyle];
    
    [(LCLeftViewController *)self.leftMenuViewController setWallet:[BTWallet myWallet]];
    [(LCLeftViewController *)self.leftMenuViewController loginSuccessed];
    
    [CRToastManager dismissNotification:YES];
    
    // MB Continuous Sync
    [[MBEngine sharedEngine] setContinuousSync:YES];
}

#pragma mark - Versioning

-(BOOL)checkLocalVersion
{
    if (![kUserDefaults boolForKey:@"kInstalled"]) {
        [[MBEngine sharedEngine] closeAllConnections];
        [[MBEngine database] deleteDatabase:nil];
        return true;
    }
    
    NSString *localVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    localVersion = [localVersion stringByAppendingFormat:@"+build%li", (long)[[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] integerValue]];
    SWFSemanticVersion *version = [SWFSemanticVersion semanticVersionWithString:localVersion];
    
    if (version) {
        localVersion = [kUserDefaults objectForKey:@"CFBundleShortVersionString"];
        if (localVersion.length == 0) {
            localVersion = @"";
        }
        
        localVersion = [localVersion stringByAppendingFormat:@"+build%li", [[kUserDefaults objectForKey:@"CFBundleVersion"] integerValue]];
        SWFSemanticVersion *comparedVersion = [SWFSemanticVersion semanticVersionWithString:localVersion];
        if ([version compare:comparedVersion] == NSOrderedDescending || !comparedVersion) {
            [[MBEngine sharedEngine] closeAllConnections];
            [[MBEngine database] deleteDatabase:nil];
            return true;
        }
    }
    
    return false;
}

#pragma mark - MBEngineDelegate

-(void)mobileBridgeEngine:(MBEngine *)engine hasEncounteredAnError:(NSError *)error
{
    if ([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive) {
        return;
    }
    
    [CRToastManager showNotificationWithOptions:[LCUtils errorToastWithTitle:NSLocalizedString(@"kError", @"") andMessage:[NSString stringWithFormat:NSLocalizedString(@"kReplicationError%@", @""), error.description]] completionBlock:nil];
    
    CLS_LOG(@"%@", error.description);
}

-(void)mobileBridgeEngine:(MBEngine *)engine didChangeStatus:(MBEngineState)state
{
    if ([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive) {
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // switch to a background thread and perform your expensive operation
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // switch back to the main thread to update your UI
            [JDStatusBarNotification showWithStatus:NSLocalizedString(@"kReplicationInProgress", @"") dismissAfter:2];
            [JDStatusBarNotification showActivityIndicator:YES indicatorStyle:UIActivityIndicatorViewStyleWhite];
        });
    });
}

-(void)mobileBridgeEngine:(MBEngine *)engine hasGetNotifiedAboutChanges:(NSArray *)changesArray
{
    if ([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive) {
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // switch to a background thread and perform your expensive operation
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // switch back to the main thread to update your UI
            [JDStatusBarNotification showProgress:[engine.overallReplicationProgress unsignedIntegerValue]];
        });
    });
}

-(void)mobileBridgeEngine:(MBEngine *)engine didResetDatabase:(CBLDatabase *)database
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateNotificationKey object:kUpdateNotificationKey];
}

-(CBLAuthenticator *)mobileBridgeEngine:(MBEngine *)engine credentialsForReplication:(CBLReplication *)replication
{
    return [CBLAuthenticator basicAuthenticatorWithName:kSyncGatewayUsername password:kSyncGatewayPassword];
}

-(void)initMobileBridge
{
    NSString *companyName = [LCUtils defaultCompanyID];
    
    if (companyName) {
        [[MBEngine sharedEngine] setPullChannels:@[companyName, [BTWallet myWallet].emailAddress]];
    }
    else
    {
        [[MBEngine sharedEngine] setPullChannels:@[[BTWallet myWallet].emailAddress]];
    }
    
    // MB Continuous Sync
    [[MBEngine sharedEngine] setContinuousSync:_loggedIn? YES : NO];
    
    [[MBEngine sharedEngine] setDatabaseName:kDefaultDatabaseName];
    [[MBEngine sharedEngine] setRemoteServerURL:kDefaultServerDbURL];
    
    // safely remove before add
    [[MBEngine sharedEngine] removeFromDelegatesPool:self];
    
    [[MBEngine sharedEngine] addToDelegatesPool:self];
    
    [[MBEngine sharedEngine] sync];
}

@end