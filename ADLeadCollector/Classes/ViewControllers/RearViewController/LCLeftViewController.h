//
//  BTLeftViewController.h
//  BitraceApp
//
//  Created by Daniele Angeli on 13/07/14.
//  Copyright (c) 2014 ADSolutions. All rights reserved.
//

#import "LCViewController.h"
#import "RESideMenu.h"

@class BTWallet;

@interface LCLeftViewController : LCViewController <UITableViewDataSource, UITableViewDelegate, RESideMenuDelegate>

@property (nonatomic, strong) BTWallet *wallet;

-(void)showGetPremiumView;
-(void)loginSuccessed;

@end