//
//  BTLeftViewController.m
//  BitraceApp
//
//  Created by Daniele Angeli on 13/07/14.
//  Copyright (c) 2014 ADSolutions. All rights reserved.
//

#import "LCLeftViewController.h"
#import "LCRootViewController.h"
#import "LCLeadsTableViewController.h"
#import "LCSettings.h"
#import "LCFormViewController.h"
#import "LCFoldersCollectionViewController.h"
#import "LCNavigationController.h"
#import "LCAlwaysTemplateImage.h"
#import "LCFoldersCollectionViewController.h"
#import "LCUpgradeViewController.h"
#import "LCAboutViewController.h"
#import "LCGravatarBadgeImageView.h"
#import "LCInsightsViewController.h"
#import "LCLeftTableViewCell.h"
#import "LCAWSManager.h"

#import <CouchbaseLite/CouchbaseLite.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <uservoice-iphone-sdk/UserVoice.h>
#import <FCFileManager/FCFileManager.h>

@interface LCLeftViewController () <UIAlertViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIView *footerView;
@property (nonatomic, weak) IBOutlet UITextView *footerLabel;
@property (nonatomic, strong) LCGravatarBadgeImageView *gravatarImageView;

@property (nonatomic, strong) NSString *oldText;
@property (nonatomic, strong) NSArray *titles;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@end

@implementation LCLeftViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGFloat gravatarSize = self.view.frame.size.height/6;
    
    _gravatarImageView = [[LCGravatarBadgeImageView alloc] initForAutoLayout];
    [self.view addSubview:_gravatarImageView];
    [_gravatarImageView autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:self.view.frame.size.height/10];
    [_gravatarImageView autoSetDimension:ALDimensionWidth toSize:gravatarSize];
    [_gravatarImageView autoSetDimension:ALDimensionHeight toSize:gravatarSize];
    [_gravatarImageView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:self.view.frame.size.width/4];
    
    self.tableView = ({
        UITableView *tableView = [[UITableView alloc] initForAutoLayout];
        [self.view addSubview:tableView];
        [tableView autoPinEdge:ALEdgeBottom toEdge:ALEdgeTop ofView:_footerView];
        [tableView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_gravatarImageView withOffset:20];
        [tableView autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view];
        [tableView autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.view];
        [tableView registerClass:[LCLeftTableViewCell class] forCellReuseIdentifier:@"Cell"];
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.opaque = NO;
        tableView.backgroundColor = companyColor();
        tableView.backgroundView = nil;
        tableView.separatorColor = [UIColor whiteColor];
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        tableView.bounces = NO;
        [tableView setShowsVerticalScrollIndicator:NO];
        tableView.scrollsToTop = NO;
        [tableView setTintColor:[UIColor whiteColor]];
        _selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        tableView;
    });
}

- (void)viewWillLayoutSubviews {
    // Your adjustments accd to
    // viewController.bounds
    
    [super viewWillLayoutSubviews];
    [self updateViewConstraints];
}

-(void)viewWillAppear:(BOOL)animated
{
    [_footerView setBackgroundColor:companyColor()];
    [_footerLabel setTextColor:[UIColor whiteColor]];
    
    [self updateViewConstraints];
}

-(void)setWallet:(BTWallet *)wallet
{
    _wallet = wallet;
    
    if ([LCUtils isPremium]) {
        _titles = @[@"Collections", @"Leads", @"Analytics", @"About", @"Support", @""];
    }
    else
    {
        _titles = @[@"Collections", @"Leads", @"Analytics", @"Upgrade", @"About", @"Support", @""];
    }
    
    [self reloadData];
    
    [_gravatarImageView setEmail:_wallet.emailAddress];
    
    NSString *companyName = [LCUtils defaultCompanyName];
    
    if (![LCUtils isEnterprise]) {
        companyName = @"AD Solutions di Angeli Daniele";
    }
    
    [_footerLabel setText:[NSString stringWithFormat:@"Copyright (c) 2015 %@ - All rights reserved.", companyName]];
}

#pragma mark - Actions

-(void)reloadData
{
    [self.tableView reloadData];
    [self.tableView selectRowAtIndexPath:_selectedIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
}

-(void)logout
{
    [[BTWallet myWallet] destroyWallet];
    _wallet = nil;
    
    [_gravatarImageView setEmail:nil];
    
    [[MBEngine sharedEngine] closeAllConnections];
    [[MBEngine database] deleteDatabase:nil];
    
    // reset all the stored userDefaults
    NSString *domainName = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:domainName];
    
    // elimino tutti i dati cachati
    [LCAWSManager wipeCache];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    [FCFileManager emptyCachesDirectory];
    [FCFileManager emptyTemporaryDirectory];
    
    [(LCRootViewController *)self.sideMenuViewController presentLoginController];
    
    _selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    [self reloadData];
}

-(void)loginSuccessed
{
    [_gravatarImageView refresh];
}

-(void)showGetPremiumView
{
    NSUInteger index = [_titles indexOfObject:@"Upgrade"];
    if (index != NSNotFound) {
        [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return _titles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    LCLeftTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [cell.imageView setImage:[LCAlwaysTemplateImage alwaysTemplateImageNamed:[_titles[indexPath.row] lowercaseString]]];
    
    if (indexPath.row == _titles.count-1) {
        NSString *userName = _wallet.userName;
        NSString *logoutText = nil;
        if (!userName) {
            logoutText = @"Logout";
        }
        else
        {
            if (IS_IPAD) {
                logoutText = [NSString stringWithFormat:NSLocalizedString(@"kConnectedAs%@", @""), userName];
            }
            else
            {
                logoutText = [NSString stringWithFormat:NSLocalizedString(@"kConnectedAs\n%@", @""), userName];
            }
        }
        
        if (!IS_IPAD) {
            [cell.textLabel setNumberOfLines:2];
        }
        
        cell.textLabel.text = logoutText;
        [cell.imageView setImage:[LCAlwaysTemplateImage alwaysTemplateImageNamed:@"logout"]];
        [cell.imageView setTintColor:[UIColor whiteColor]];
    }
    else
    {
        cell.textLabel.text = NSLocalizedString([@"k" stringByAppendingString:_titles[indexPath.row]], @"");
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_selectedIndexPath isEqual:indexPath]) {
        [tableView deselectRowAtIndexPath:_selectedIndexPath animated:YES];
    }
    
    NSString *title = [_titles objectAtIndex:indexPath.row];
    
    if ([title isEqualToString:@"Collections"]) {
        if (![[[self.sideMenuViewController.contentViewController childViewControllers] lastObject] isKindOfClass:[LCFoldersCollectionViewController class]])
        {
            [self.sideMenuViewController setContentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"LCFoldersCollectionViewController"] animated:YES];
        }
    }
    else if ([title isEqualToString:@"Leads"])
    {
        if (![[[self.sideMenuViewController.contentViewController childViewControllers] lastObject] isKindOfClass:[LCLeadsTableViewController class]])
        {
            LCLeadsTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LCLeadsTableViewController"];
            [viewController setFolder:nil];
            LCNavigationController *navigationController = [[LCNavigationController alloc] initWithRootViewController:viewController];
            [self.sideMenuViewController setContentViewController:navigationController animated:YES];
        }
    }
    else if ([title isEqualToString:@""])
    {
        indexPath = _selectedIndexPath;
        
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"kLogout", @"") message:NSLocalizedString(@"kLogoutMessage", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"kLogout", @"") otherButtonTitles:NSLocalizedString(@"kCancel", @""), nil] show];
    }
    else if ([title isEqualToString:@"Upgrade"])
    {
        if (![[[self.sideMenuViewController.contentViewController childViewControllers] lastObject] isKindOfClass:[LCUpgradeViewController class]])
        {
            LCUpgradeViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LCUpgradeViewController"];
            LCNavigationController *navigationController = [[LCNavigationController alloc] initWithRootViewController:viewController];
            [navigationController setModalPresentationStyle:UIModalPresentationFormSheet];
            [self.sideMenuViewController presentViewController:navigationController animated:YES completion:nil];
        }
    }
    else if ([title isEqualToString:@"About"])
    {
        if (![[[self.sideMenuViewController.contentViewController childViewControllers] lastObject] isKindOfClass:[LCAboutViewController class]])
        {
            LCAboutViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LCAboutViewController"];
            LCNavigationController *navigationController = [[LCNavigationController alloc] initWithRootViewController:viewController];
            [navigationController setModalPresentationStyle:UIModalPresentationFormSheet];
            [self.sideMenuViewController setContentViewController:navigationController animated:YES];
        }
    }
    else if ([title isEqualToString:@"Settings"])
    {
        //
    }
    else if ([title isEqualToString:@"Analytics"])
    {
        if (![[[self.sideMenuViewController.contentViewController childViewControllers] lastObject] isKindOfClass:[LCInsightsViewController class]])
        {
            LCInsightsViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LCAnalyticsViewController"];
            LCNavigationController *navigationController = [[LCNavigationController alloc] initWithRootViewController:viewController];
            [self.sideMenuViewController setContentViewController:navigationController animated:YES];
        }
    }
    else if ([title isEqualToString:@"Support"])
    {
        indexPath = _selectedIndexPath;
        
        // Set this up once when your application launches
        UVConfig *config = [UVConfig configWithSite:@"adsolutions.uservoice.com"];
        config.forumId = 303819;
        
        NSString *username = [NSString stringWithFormat:@"%@ %@", [BTWallet myWallet].userName, [BTWallet myWallet].userSurname];
        
        [config identifyUserWithEmail:[BTWallet myWallet].emailAddress name:username guid:[BTWallet myWallet].session];
        [UserVoice initialize:config];
        
        // Call this wherever you want to launch UserVoice
        [[UINavigationBar appearance] setTintColor:companyColor()];
        [UserVoice presentUserVoiceInterfaceForParentViewController:self];
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    }
    
    _selectedIndexPath = indexPath;
    [tableView selectRowAtIndexPath:_selectedIndexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    
    [self.sideMenuViewController hideMenuViewController];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return ({
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1)];
        [view setBackgroundColor:ClearColor];
        view;
    });;
}

#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.cancelButtonIndex) {
        [self logout];
    }
}

-(void)mobileBridgeDidUpdateDataWithNotification:(NSNotification *)notification
{
    self.wallet = [BTWallet myWallet];
    
    [self reloadData];
}

@end