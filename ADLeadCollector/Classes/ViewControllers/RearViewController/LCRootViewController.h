//
//  BTRootViewController.h
//  BitraceApp
//
//  Created by Daniele Angeli on 13/07/14.
//  Copyright (c) 2014 ADSolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RESideMenu/RESideMenu.h>

@interface LCRootViewController : RESideMenu <RESideMenuDelegate>

-(void)presentLoginController;

@end