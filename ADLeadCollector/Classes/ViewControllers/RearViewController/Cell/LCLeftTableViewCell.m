//
//  LCLeftTableViewCell.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 25/07/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCLeftTableViewCell.h"

@implementation LCLeftTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.textLabel.textColor = [UIColor whiteColor];
        self.textLabel.highlightedTextColor = companyColor();
        [self.textLabel setFont:[LCUtils fontWithSize:18]];
        [self.imageView setTintColor:[UIColor whiteColor]];
        self.selectedBackgroundView = nil;
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    if (selected) {
        self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1];
        [self.imageView setTintColor:companyColor()];
        [self.textLabel setFont:[LCUtils boldFontWithSize:18]];
    }
    else
    {
        self.backgroundColor = ClearColor;
        [self.imageView setTintColor:[UIColor whiteColor]];
        [self.textLabel setFont:[LCUtils fontWithSize:18]];
    }
    
    [self.textLabel setHighlighted:selected];
}

@end