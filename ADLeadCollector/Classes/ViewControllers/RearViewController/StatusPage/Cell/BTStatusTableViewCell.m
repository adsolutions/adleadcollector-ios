//
//  BTStatusTableViewCell.m
//  BitraceApp
//
//  Created by Daniele Angeli on 13/07/14.
//  Copyright (c) 2014 ADSolutions. All rights reserved.
//

#import "BTStatusTableViewCell.h"

@interface BTStatusTableViewCell ()

@property (nonatomic, weak) IBOutlet UIView *statusColorView;
@property (nonatomic, weak) IBOutlet UILabel *generalStatusLabel;
@property (nonatomic, weak) IBOutlet UILabel *statusTitleLabel;

@end

@implementation BTStatusTableViewCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

//-(void)setNeedsLayout
//{
//    [self subViewColorIteratorOnArray:self.subviews atIndex:self.subviews.count-1];
//}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

#pragma mark - Setters

-(void)setStatusColor:(UIColor *)statusColor
{
    _statusColor = statusColor;
    
    [_statusColorView setBackgroundColor:statusColor];
    [_statusColorView setClipsToBounds:YES];
    [_statusColorView.layer setCornerRadius:10];
}

-(void)setStatusGeneralMessage:(NSString *)statusGeneralMessage
{
//    [_generalStatusLabel setFont:[BTAppCatalogUtils bitraceFontWithSize:IS_IPAD? 16 : 12]];
    [_generalStatusLabel setText:statusGeneralMessage];
}

-(void)setStatusTitle:(NSString *)statusTitle
{
//    [_statusTitleLabel setFont:[BTAppCatalogUtils bitraceFontWithSize:IS_IPAD? 22 : 18]];
    [_statusTitleLabel setMinimumScaleFactor:0.5];
    [_statusTitleLabel setText:statusTitle];
}

-(NSInteger)subViewColorIteratorOnArray:(NSArray *)subviewsArray atIndex:(NSUInteger)index
{
    if (index == -1) {
        return -1;
    }
    
    if ([[[subviewsArray objectAtIndex:index] subviews] count] > 0) {
        [self subViewColorIteratorOnArray:[[subviewsArray objectAtIndex:index] subviews] atIndex:[[[subviewsArray objectAtIndex:index] subviews] count] -1];
    }
    
    if ([(UIView *)[subviewsArray objectAtIndex:index] tag] != 999) {
        ((UIView *)[subviewsArray objectAtIndex:index]).backgroundColor = companyColor();
    }
    
    return [self subViewColorIteratorOnArray:subviewsArray atIndex:index-1];
}

@end