//
//  BTStatusTableViewCell.h
//  BitraceApp
//
//  Created by Daniele Angeli on 13/07/14.
//  Copyright (c) 2014 ADSolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTStatusTableViewCell : UITableViewCell

@property (nonatomic, strong) UIColor *statusColor;
@property (nonatomic, strong) NSString *statusGeneralMessage;
@property (nonatomic, strong) NSString *statusTitle;

@end