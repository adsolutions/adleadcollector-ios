//
//  BTStatusPageViewController.m
//  BitraceApp
//
//  Created by Daniele Angeli on 02/11/14.
//  Copyright (c) 2014 ADSolutions. All rights reserved.
//

#import "BTStatusPageViewController.h"
#import "LCRootViewController.h"

@interface BTStatusPageViewController () <UIWebViewDelegate>

@property (nonatomic, weak) IBOutlet UIWebView *webView;

@end

@implementation BTStatusPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_webView setDelegate:self];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://status.bitrace.co"]];
    [_webView loadRequest:request];
    
    [self.webView.scrollView setBackgroundColor:companyColor()];
    [self.webView setScalesPageToFit:YES];
}

#pragma mark - UIWebViewDelegate

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
//    [SVProgressHUD showWithStatus:NSLocalizedString(@"kLoading", @"Loading")];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
//    [SVProgressHUD dismiss];
}

@end
