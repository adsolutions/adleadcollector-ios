//
//  LCRegistrationFormViewController.h
//  ADLeadCollector
//
//  Created by Daniele Angeli on 12/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <FXForms/FXForms.h>

@class LCRegistrationFormViewController, LCLead, LCFolder;

@protocol LCRegistrationFormDelegate <NSObject>

@optional
-(void)registrationFormViewController:(LCRegistrationFormViewController *)controller didAddLead:(LCLead *)lead;

@optional
-(void)registrationFormViewController:(LCRegistrationFormViewController *)controller didUpdateLead:(LCLead *)lead;

@end

@interface LCRegistrationFormViewController : FXFormViewController

@property (nonatomic, strong) LCLead *lead;
@property (nonatomic, strong) LCFolder *folder;

@property (nonatomic, unsafe_unretained) id <LCRegistrationFormDelegate> delegate;

@end