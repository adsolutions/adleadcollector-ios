//
//  LCRegistrationFormViewController.m
//  ADLeadCollector
//
//  Created by Daniele Angeli on 12/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCRegistrationFormViewController.h"
#import "LCLead.h"
#import "LCSettings.h"
#import "LCFolder.h"
#import "LCDynamicForm.h"
#import "NSDate+Extras.h"
#import "ISO8601DateValueTransformer.h"
#import "LCAudioRecorderViewController.h"
#import "LCAWSManager.h"
#import "LCFormButtonPickerCell.h"
#import "LCNavigationController.h"
#import "LCActionMenu.h"

#import <AFNetworking/AFNetworking.h>
#import <ChimpKit/ChimpKit.h>
#import <TGCameraViewController/TGCameraViewController.h>
#import <TGCameraViewController/TGCameraColor.h>
#import <NSString+Validation/NSString+Validation.h>
#import <UIActionSheet+Blocks/UIActionSheet+Blocks.h>
#import <FSImageViewer/FSImageViewer.h>
#import <FSImageViewer/FSBasicImage.h>
#import <FSImageViewer/FSBasicImageSource.h>
#import <ALSystemUtilities/ALSystem.h>
#import <UIAlertView+Blocks/UIAlertView+Blocks.h>

@interface LCRegistrationFormViewController () <LCFXFormControllerExtensionsDelegate, LCAudioRecorderDelegate, TGCameraDelegate>

@property (nonatomic, strong) NSData *audioFileData;

@property (nonatomic, strong) NSString *audioFileURL;
@property (nonatomic, strong) NSString *audioRealPath;

@property (nonatomic, strong) NSString *pictureFileURL;
@property (nonatomic, strong) NSString *pictureRealPath;

@property (nonatomic, strong) TGCameraNavigationController *cameraNavigationController;

@end

@implementation LCRegistrationFormViewController

#pragma mark - View Lifecycle

-(void)viewWillAppear:(BOOL)animated
{
    if (!_lead && !self.formController.form) {
        self.title = NSLocalizedString(@"kNewLead", @"");
        [self.formController setForm:[[LCDynamicForm alloc] init]];
    }
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveButtonPressed:)]];
    
    [self.formController.tableView setSeparatorColor:ClearColor];
    [self.formController.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    // dismiss toolbar every time
    [self.navigationController setToolbarHidden:YES animated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    NSIndexPath *selectedRowIndexPath = [self.formController.tableView indexPathForSelectedRow];
    if (selectedRowIndexPath) {
        [self.formController.tableView deselectRowAtIndexPath:selectedRowIndexPath animated:YES];
        [self.formController.tableView reloadRowsAtIndexPaths:@[selectedRowIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    
    [self.formController.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Setters

-(void)setLead:(LCLead *)lead
{
    _lead = lead;
    LCDynamicForm *dynamicForm = [[LCDynamicForm alloc] init];
    [dynamicForm setValuesByKey:_lead.document.userProperties.mutableCopy];
    [self.formController setForm:dynamicForm];
    
    self.title = _lead.nameSurname? _lead.nameSurname : _lead.companyName;
    
    self.audioFileURL = _lead.audioFileURLs.firstObject;
    self.pictureFileURL = _lead.pictureFileURLs.firstObject;
}

#pragma mark - Getters

-(NSString *)audioFileURL
{
    return _audioFileURL;
}

#pragma mark - Actions

-(void)saveButtonPressed:(id)sender
{
    LCDynamicForm *dynamicForm = self.formController.form;
    
    if (([NSString isEmptyString:(NSString *)[dynamicForm valueForKey:@"nameSurname"]]) && ![sender isKindOfClass:[LCAudioRecorderViewController class]] && ![sender isKindOfClass:[TGCameraNavigationController class]]) {
        [CRToastManager showNotificationWithOptions:[LCUtils errorToastWithTitle:NSLocalizedString(@"kError", @"") andMessage:NSLocalizedString(@"kFormFillNameSurnameError", @"")] completionBlock:nil];
    }
    else
    {
        LCLead *currentLead = _lead? _lead : [LCLead createNew];
        __block NSMutableDictionary *properties = currentLead.document.properties.mutableCopy;
        
        if (!properties) {
            properties = @{}.mutableCopy;
        }
        
        [dynamicForm.valuesByKey.allKeys enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            id value = [[dynamicForm valuesByKey] objectForKey:obj];
            
            if ([value isKindOfClass:[NSString class]] && [NSString isEmptyString:(NSString *)value]) {
                [properties setObject:@"" forKey:obj];
            }
            else if ([value isKindOfClass:[NSURL class]] && ![LCUtils validateUrl:[(NSURL *)value relativeString]])
            {
                return;
            }
            else if ([value isKindOfClass:[NSDate class]])
            {
                [properties setObject:[NSDate stringFromDate:value withFormat:@"YYYY-MM-dd"] forKey:obj];
            }
            else
            {
                [properties setObject:value forKey:obj];
            }
        }];
        
        // author email for future analytics
        if (!_lead) {
            [properties setObject:[BTWallet myWallet].emailAddress forKey:@"author"];
            [properties setObject:NSStringFromClass([LCLead class]) forKey:@"type"];
            [properties setObject:@[[MBEngine sharedEngine].pullChannels.firstObject] forKey:@"channels"];
        }
        
        //no nameSurname > temp lead
        if ([NSString isEmptyString:(NSString *)[dynamicForm valueForKey:@"nameSurname"]]) {
            if ([sender isKindOfClass:[LCAudioRecorderViewController class]]) {
                [properties setObject:@"Voice - Lead" forKey:@"nameSurname"];
            }
            else if ([sender isKindOfClass:[TGCameraNavigationController class]])
            {
                [properties setObject:@"Picture - Lead" forKey:@"nameSurname"];
            }
        }
        
        NSString *documentID = [[[MBEngine sharedEngine] storeDocument:properties withID:_lead? _lead.document.documentID : nil] documentID];
        
        NSAssert(![NSString isEmptyString:documentID], @"Nil or invalid document ID");
        
        if (!_folder) {
            _folder = [[LCFolder findAllMatchingPredicate:[NSPredicate predicateWithFormat:@"value.name = %@", kUnassigned] andSortedBy:nil] firstObject];
            
            if (!_folder) {
                _folder = [LCFolder createNew];
                [_folder setName:kUnassigned];
            }
        }
        
        if (![_folder.relatedLeads containsObject:documentID]) {
            NSMutableArray *leadsIDs = nil;
            if (!_folder.relatedLeads) {
                leadsIDs = @[].mutableCopy;
            }
            else
            {
                leadsIDs = _folder.relatedLeads.mutableCopy;
            }
            
            [leadsIDs addObject:documentID];
            [_folder saveDocument];
        }
        
        BOOL leadIsNew = false;
        
        if (!_lead) {
            _lead = [[LCLead alloc] initFromDocumentID:documentID];
            leadIsNew = YES;
        }
        
        if ([NSString isEmptyString:[_lead timestamp]])
        {
            [_lead setTimestamp:[[NSDate date] description]];
        }
        
        // audio file saving
        if (![NSString isEmptyString:self.audioFileURL] && ![NSString isEmptyString:self.audioRealPath]) {
            NSData *fileData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:self.audioRealPath]];
            if (fileData && ![_lead.audioFileURLs.firstObject isEqualToString:self.audioFileURL]) {
                [_lead setAudioFileURLs:@[self.audioFileURL]];
                
                [self uploadFile:self.audioFileURL fromPath:self.audioRealPath andExtension:LCAWSManagerFileTypeM4A];
            }
        }
        
        // picture saving
        if (![NSString isEmptyString:self.pictureFileURL] && ![NSString isEmptyString:self.pictureRealPath]) {
            NSData *fileData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:self.pictureRealPath]];
            if (fileData && ![_lead.pictureFileURLs.firstObject isEqualToString:self.pictureFileURL]) {
                [_lead setPictureFileURLs:@[self.pictureFileURL]];
                
                [self uploadFile:self.pictureFileURL fromPath:self.pictureRealPath andExtension:LCAWSManagerFileTypeJPEG];
            }
        }
        
        [_lead setAssociatedFolder:_folder.document.documentID];
        [_lead saveDocument];
        
        if (![sender isKindOfClass:[LCAudioRecorderViewController class]] && ![sender isKindOfClass:[TGCameraNavigationController class]]) {
            [CRToastManager showNotificationWithOptions:[LCUtils successToastWithTitle:nil andMessage:NSLocalizedString(@"kLeadSavedSuccessfully", @"")] completionBlock:nil];
        }
        
        if (leadIsNew) {
            if ([self.delegate respondsToSelector:@selector(registrationFormViewController:didAddLead:)]) {
                [self setLead:_lead];
                [self.delegate registrationFormViewController:self didAddLead:_lead];
            }
        }
        else
        {
            if ([self.delegate respondsToSelector:@selector(registrationFormViewController:didUpdateLead:)]) {
                [self.delegate registrationFormViewController:self didUpdateLead:_lead];
            }
        }
    }
}

- (void)submitRegistrationForm:(UITableViewCell<FXFormFieldCell> *)cell
{
    LCDynamicForm *dynamicForm = cell.field.form;
    
    if (!_lead && ![NSString isEmptyString:[dynamicForm valueForKey:@"email"]]) {
        [self saveButtonPressed:cell];
        
        //        if ([LCUtils isPremium]) {
        //            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        //            [manager POST:@"http://leadscollector.aditsolutions.it/wp-admin/admin-ajax.php" parameters:@{@"email" : _lead.email, @"lead" : _lead.document.userProperties, @"session" : [BTWallet myWallet].session, @"username" : [BTWallet myWallet].emailAddress, @"company" : [BTWallet myWallet].companiesArray.firstObject[@"identifier"]? [BTWallet myWallet].companiesArray.firstObject[@"identifier"] : @"", @"action" : @"leadscollector_user_subscription"} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //
        //            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //                CLS_LOG(@"POST error: %@", error);
        //            }];
        //        }
    }
    else
    {
        [self saveButtonPressed:cell];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)uploadFile:(NSString *)fileName fromPath:(NSString *)filePath andExtension:(LCAWSManagerFileType)extension
{
    __block typeof(self) me = self;
    
    __block LCAWSInUpload *inUpload = [LCAWSInUpload new];
    [inUpload setFileName:fileName];
    [inUpload setType:extension];
    [inUpload store];
    
    [me.formController.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    // quickly store the file in cache
    [[LCAWSManager sharedManager] storeFileInCache:[NSData dataWithContentsOfFile:filePath] forName:fileName andType:extension];
    
    [CRToastManager showNotificationWithOptions:[LCUtils infoToastWithMessage:NSLocalizedString(@"kUploading", @"") andTimeout:2] completionBlock:nil];
    
    // put the recorded audio to the AWS S3
    [[LCAWSManager sharedManager] putFile:filePath withName:fileName forType:extension inBucketPath:extension == LCAWSManagerFileTypeM4A? AWSVoiceNotesPath : AWSPicturesPath withCompletionHandler:^(BOOL success, id object) {
        LCAWSFailedUpload *failedUpload = [LCAWSFailedUpload objectForName:fileName];
        if (!success) {
            [CRToastManager showNotificationWithOptions:[LCUtils errorToastWithTitle:NSLocalizedString(@"kError", @"") andMessage:NSLocalizedString(@"kUploadFailed", @"")] completionBlock:nil];
            
            if (!failedUpload) {
                failedUpload = [LCAWSFailedUpload new];
                [failedUpload setTries:0];
                [failedUpload setFileName:fileName];
                [failedUpload setType:extension];
            }
            
            // i should save the current upload to a cache and then retry the transfer back again
            [failedUpload store];
        }
        else
        {
            if (failedUpload) {
                // if any
                [failedUpload remove];
            }
        }
        
        // if any
        [inUpload remove];
        
        [me.formController.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

#pragma mark - LCFXFormControllerExtensionsDelegate

-(NSString *)formButtonPicker:(LCFormButtonPickerCell *)picker pathForResourceForButton:(LCFormCellPickerButton *)button atIndex:(NSUInteger)buttonIndex
{
    switch (button.type) {
        case LCFormCellPickerButtonTypeCameraCapture:
            return self.pictureFileURL;
            break;
        case LCFormCellPickerButtonTypeVoiceRecorder:
            return self.audioFileURL;
        default:
            return nil;
            break;
    }
}

-(NSUInteger)formButtonPicker:(LCFormButtonPickerCell *)picker stateForButton:(LCFormCellPickerButton *)button atIndex:(NSUInteger)buttonIndex
{
    if (button.status == LCFormCellPickerButtonStateDownloading) {
        return button.status;
    }
    
    if (button.type == LCFormCellPickerButtonTypeVoiceRecorder) {
        if ([LCAWSInUpload objectForName:self.audioFileURL]) {
            return LCFormCellPickerButtonStateUploading;
        }
        
        if ([LCAWSFailedUpload objectForName:self.audioFileURL]) {
            return LCFormCellPickerButtonStateErrorUpload;
        }
        
        return ![NSString isEmptyString:self.audioFileURL]? LCFormCellPickerButtonStatePlay : LCFormCellPickerButtonStateRecord;
    }
    else
    {
        if ([LCAWSInUpload objectForName:self.pictureFileURL]) {
            return LCFormCellPickerButtonStateUploading;
        }
        
        if ([LCAWSFailedUpload objectForName:self.pictureFileURL]) {
            return LCFormCellPickerButtonStateErrorUpload;
        }
        
        return ![NSString isEmptyString:self.pictureFileURL]? LCFormCellPickerButtonStateShowPicture : LCFormCellPickerButtonStateTakePicture;
    }
}

-(void)formButtonPicker:(LCFormButtonPickerCell *)picker didPressButton:(LCFormCellPickerButton *)button atIndex:(NSUInteger)buttonIndex
{
    __block typeof(self) me = self;
    
    switch (button.type) {
        case LCFormCellPickerButtonTypeCameraCapture:
        {
            if (button.status == LCFormCellPickerButtonStateDownloading) {
                return;
            }
            
            if (button.status == LCFormCellPickerButtonStateErrorUpload) {
                [UIActionSheet showInView:button withTitle:NSLocalizedString(@"kPreviousUploadFailedFound", @"") cancelButtonTitle:NSLocalizedString(@"kCancel", @"") destructiveButtonTitle:NSLocalizedString(@"kDelete", @"") otherButtonTitles:@[NSLocalizedString(@"kRetry", @""), NSLocalizedString(@"kShowAnyway", @"")] tapBlock:^(UIActionSheet * __nonnull actionSheet, NSInteger buttonIndex){
                    if (actionSheet.destructiveButtonIndex == buttonIndex) {
                        [[LCAWSManager sharedManager] deleteFileFromCache:me.pictureFileURL withType:LCAWSManagerFileTypeJPEG];
                        [[LCAWSFailedUpload objectForName:me.pictureFileURL] remove];
                        
                        _lead.pictureFileURLs = nil;
                        [me saveButtonPressed:nil];
                        
                        [me.formController.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
                    }
                    else if (actionSheet.cancelButtonIndex == buttonIndex)
                    {
                        //
                    }
                    else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"kShowAnyway", @"")])
                    {
                        [me showPicture:button];
                    }
                    else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"kRetry", @"")])
                    {
                        [me uploadFile:me.pictureFileURL fromPath:[LCAWSFailedUpload objectForName:me.pictureFileURL].filePath andExtension:LCAWSManagerFileTypeJPEG];
                    }
                }];
            }
            else if (button.status == LCFormCellPickerButtonStateErrorDownload)
            {
                if (button.error.code == 9) {
                    [UIActionSheet showInView:button withTitle:NSLocalizedString(@"kResourceCurrentlyUnavailable", @"") cancelButtonTitle:NSLocalizedString(@"kCancel", @"") destructiveButtonTitle:NSLocalizedString(@"kOverwriteAndProceed", @"") otherButtonTitles:nil tapBlock:^(UIActionSheet * _Nonnull actionSheet, NSInteger buttonIndex) {
                        if (buttonIndex != actionSheet.cancelButtonIndex) {
                            _lead.pictureFileURLs = nil;
                            me.pictureFileURL = nil;
                            me.pictureRealPath = nil;
                            
                            [me saveButtonPressed:nil];
                            
                            [me showPicture:button];
                        }
                    }];
                }
                else
                {
                    [UIActionSheet showInView:button withTitle:NSLocalizedString(@"kDownloadFailed", @"") cancelButtonTitle:NSLocalizedString(@"OK", @"") destructiveButtonTitle:nil otherButtonTitles:@[NSLocalizedString(@"kRetry", @"")] tapBlock:^(UIActionSheet * _Nonnull actionSheet, NSInteger buttonIndex) {
                        if (buttonIndex != actionSheet.cancelButtonIndex) {
                            button.error = nil;
                            [me.formController.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
                        }
                    }];
                }
            }
            else
            {
                [self showPicture:button];
            }
        }
            break;
        case LCFormCellPickerButtonTypeVoiceRecorder:
        {
            if (button.status == LCFormCellPickerButtonStateDownloading) {
                return;
            }
            
            if (button.status == LCFormCellPickerButtonStateErrorUpload) {
                [UIActionSheet showInView:button withTitle:NSLocalizedString(@"kPreviousUploadFailedFound", @"") cancelButtonTitle:NSLocalizedString(@"kCancel", @"") destructiveButtonTitle:NSLocalizedString(@"kDelete", @"") otherButtonTitles:@[NSLocalizedString(@"kRetry", @""), NSLocalizedString(@"kPlayAnyway", @"")] tapBlock:^(UIActionSheet * __nonnull actionSheet, NSInteger buttonIndex){
                    if (actionSheet.destructiveButtonIndex == buttonIndex) {
                        [[LCAWSManager sharedManager] deleteFileFromCache:me.audioFileURL withType:LCAWSManagerFileTypeM4A];
                        [[LCAWSFailedUpload objectForName:me.audioFileURL] remove];
                        
                        _lead.audioFileURLs = nil;
                        [me saveButtonPressed:nil];
                        
                        [me.formController.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
                    }
                    else if (actionSheet.cancelButtonIndex == buttonIndex)
                    {
                        //
                    }
                    else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"kPlayAnyway", @"")])
                    {
                        [me playVoiceNote:button];
                    }
                    else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"kRetry", @"")])
                    {
                        [me uploadFile:me.audioFileURL fromPath:[LCAWSFailedUpload objectForName:me.audioFileURL].filePath andExtension:LCAWSManagerFileTypeM4A];
                    }
                }];
            }
            else if (button.status == LCFormCellPickerButtonStateErrorDownload)
            {
                if (button.error.code == 9) {
                    [UIActionSheet showInView:button withTitle:NSLocalizedString(@"kResourceCurrentlyUnavailable", @"") cancelButtonTitle:NSLocalizedString(@"kCancel", @"") destructiveButtonTitle:NSLocalizedString(@"kOverwriteAndProceed", @"") otherButtonTitles:nil tapBlock:^(UIActionSheet * _Nonnull actionSheet, NSInteger buttonIndex) {
                        if (buttonIndex != actionSheet.cancelButtonIndex) {
                            _lead.audioFileURLs = nil;
                            me.audioFileURL = nil;
                            me.audioRealPath = nil;
                            
                            [me saveButtonPressed:nil];
                            
                            [me playVoiceNote:button];
                        }
                    }];
                }
                else
                {
                    [UIActionSheet showInView:button withTitle:NSLocalizedString(@"kDownloadFailed", @"") cancelButtonTitle:NSLocalizedString(@"OK", @"") destructiveButtonTitle:nil otherButtonTitles:@[NSLocalizedString(@"kRetry", @"")] tapBlock:^(UIActionSheet * _Nonnull actionSheet, NSInteger buttonIndex) {
                        if (buttonIndex != actionSheet.cancelButtonIndex) {
                            button.error = nil;
                            [me.formController.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
                        }
                    }];
                }
            }
            else
            {
                [self playVoiceNote:button];
            }
        }
            break;
        default:
            break;
    }
}

-(void)playVoiceNote:(id)sender
{
    __block typeof(self) me = self;
    
    [sender setStatus:LCFormCellPickerButtonStateDownloading];
    
    [[LCAWSManager sharedManager] getFilePathForFile:[self.audioFileURL stringByDeletingPathExtension] forType:LCAWSManagerFileTypeM4A inBucketPath:AWSVoiceNotesPath withCompletionHandler:^(BOOL success, id object) {
        // errore network
        if ([object isKindOfClass:[NSError class]]) {
            if (!success) {
                [UIAlertView showWithTitle:NSLocalizedString(@"kError", @"") message:NSLocalizedString(@"kNetworkErrorWhileDownloading", @"") cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:^(UIAlertView * _Nonnull alertView, NSInteger buttonIndex) {
                    // ok only
                }];
            }
            else
            {
                [UIAlertView showWithTitle:NSLocalizedString(@"kError", @"") message:NSLocalizedString(@"kResourceCurrentlyUnavailable", @"") cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:^(UIAlertView * _Nonnull alertView, NSInteger buttonIndex) {
                    // ok only
                }];
            }
            
            [sender setStatus:LCFormCellPickerButtonStateErrorDownload];
            
            return;
        }
        
        me.lead.audioFileURLs = success? @[me.audioFileURL] : nil;
        me.audioRealPath = success? object : nil;
        
        LCAudioRecorderViewController *controller = [[LCAudioRecorderViewController alloc] init];
        controller.delegate = me;
        [me.navigationController pushViewController:controller animated:YES];
        
        [sender setStatus:success? LCFormCellPickerButtonStatePlay : LCFormCellPickerButtonStateRecord];
    }];
}

-(void)showPicture:(id)sender
{
    __block typeof(self) me = self;
    
    [sender setStatus:LCFormCellPickerButtonStateDownloading];
    
    // switch back to the main thread to update your UI
    [[LCAWSManager sharedManager] getFilePathForFile:[self.pictureFileURL stringByDeletingPathExtension] forType:LCAWSManagerFileTypeJPEG inBucketPath:AWSVoiceNotesPath withCompletionHandler:^(BOOL success, id object) {
        // errore network
        if ([object isKindOfClass:[NSError class]]) {
            if (!success) {
                [UIAlertView showWithTitle:NSLocalizedString(@"kError", @"") message:NSLocalizedString(@"kNetworkErrorWhileDownloading", @"") cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:^(UIAlertView * _Nonnull alertView, NSInteger buttonIndex) {
                    // ok only
                }];
            }
            else
            {
                [UIAlertView showWithTitle:NSLocalizedString(@"kError", @"") message:NSLocalizedString(@"kResourceCurrentlyUnavailable", @"") cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:^(UIAlertView * _Nonnull alertView, NSInteger buttonIndex) {
                    // ok only
                }];
            }
            
            [sender setStatus:LCFormCellPickerButtonStateErrorDownload];
            
            return;
        }
        
        // file trovato
        if (success) {
            me.lead.pictureFileURLs = success? @[me.pictureFileURL] : nil;
            me.pictureRealPath = success? object : nil;
            
            FSBasicImage *firstPhoto = [[FSBasicImage alloc] initWithImage:[UIImage imageWithContentsOfFile:object] name:nil];
            FSBasicImageSource *photoSource = [[FSBasicImageSource alloc] initWithImages:@[firstPhoto]];
            FSImageViewerViewController *imageViewController = [[FSImageViewerViewController alloc] initWithImageSource:photoSource];
            LCNavigationController *navigationController = [[LCNavigationController alloc] initWithRootViewController:imageViewController];
            
            UIBarButtonItem *trashButton = [[UIBarButtonItem alloc] initWithImage:[LCActionMenuImage imageWithSize:CGSizeMake(30, 30) color:[UIColor whiteColor] andType:LCActionMenuButtonTypeDelete] style:UIBarButtonItemStylePlain target:me action:@selector(cameraDidDeletePicture:)];
            
            UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
            
            [imageViewController setToolbarItems:@[flexibleSpace, trashButton] animated:NO];
            [navigationController setToolbarHidden:NO animated:YES];
            [me.navigationController presentViewController:navigationController animated:YES completion:^{
                [imageViewController setTitle:me.lead.nameSurname];
            }];
        }
        else
        {
            // file non trovato
#if !TARGET_IPHONE_SIMULATOR
            [TGCameraColor setTintColor:companyColor()];
            [TGCamera setOption:kTGCameraOptionSaveImageToAlbum value:@NO];
            me.cameraNavigationController = [TGCameraNavigationController newWithCameraDelegate:me];
            [me.navigationController presentViewController:me.cameraNavigationController animated:YES completion:nil];
#endif
        }
        
        [sender setStatus:success? LCFormCellPickerButtonStateShowPicture : LCFormCellPickerButtonStateTakePicture];
    }];
}

#pragma mark - LCAudioRecorderDelegate

-(NSString *)audioRecorderControllerLoadResource:(LCAudioRecorderViewController *)controller
{
    return self.audioRealPath;
}

-(void)audioRecorderController:(LCAudioRecorderViewController *)controller didFinishWithAudioAtPath:(NSString *)audioPath
{
    _lead.audioFileURLs = nil;
    
    self.audioFileURL = [[audioPath lastPathComponent] stringByDeletingPathExtension];
    self.audioRealPath = audioPath;
    
    // forcely save the lead data
    [self performSelectorOnMainThread:@selector(saveButtonPressed:) withObject:controller waitUntilDone:YES];
    
    // refresh not needed, it will be permformed when this VC appears
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)audioRecorderControllerDidCancel:(LCAudioRecorderViewController *)controller
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)audioRecorderController:(LCAudioRecorderViewController *)controller performDeletionOfAudioAtPath:(NSString *)audioPath
{
    self.audioFileURL = nil;
    self.audioRealPath = nil;
    
    if (![NSString isEmptyString:_lead.audioFileURLs.firstObject]) {
        _lead.audioFileURLs = nil;
        
        if (_lead) {
            [self performSelectorOnMainThread:@selector(saveButtonPressed:) withObject:controller waitUntilDone:YES];
        }
    }
    
    // if any
    [[LCAWSFailedUpload objectForName:audioPath] remove];
    
    [[LCAWSManager sharedManager] deleteFile:audioPath forType:LCAWSManagerFileTypeM4A inBucketPath:AWSVoiceNotesPath withCompletionHandler:^(BOOL success, id object) {
        if (success) {
            // if any
            [[LCAWSFailedDeletion objectForName:audioPath] remove];
        }
        else
        {
            LCAWSFailedDeletion *failedDeletion = [LCAWSFailedDeletion new];
            [failedDeletion setFileName:audioPath];
            [failedDeletion setType:LCAWSManagerFileTypeM4A];
            [failedDeletion store];
        }
    }];
}

#pragma mark - TGCameraDelegate

- (void)cameraDidCancel
{
    [_cameraNavigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraDidTakePhoto:(UIImage *)image
{
    NSString *tempFilePath = [LCUtils tempFileForType:LCAWSManagerFileTypeJPEG];
    self.pictureFileURL = [[tempFilePath lastPathComponent] stringByDeletingPathExtension];
    self.pictureRealPath = tempFilePath;
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.8);
    [imageData writeToFile:tempFilePath atomically:NO];
    
    // forcely save the lead data
    [self saveButtonPressed:_cameraNavigationController];
    
    // refresh not needed, it will be permformed when this VC appears
    [_cameraNavigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraDidSelectAlbumPhoto:(UIImage *)image
{
    [self cameraDidTakePhoto:image];
}

-(void)cameraDidDeletePicture:(id)sender
{
    __block typeof(self) me = self;
    
    [UIActionSheet showFromBarButtonItem:sender animated:YES withTitle:nil cancelButtonTitle:NSLocalizedString(@"kCancel", @"") destructiveButtonTitle:NSLocalizedString(@"kDeletePicture", @"") otherButtonTitles:nil tapBlock:^(UIActionSheet * __nonnull actionSheet, NSInteger buttonIndex) {
        if (buttonIndex == actionSheet.destructiveButtonIndex) {
            me.pictureFileURL = nil;
            me.pictureRealPath = nil;
            
            if (![NSString isEmptyString:me.lead.pictureFileURLs.firstObject]) {
                me.lead.pictureFileURLs = nil;
                
                if (me.lead) {
                    [me saveButtonPressed:me.navigationController.presentedViewController];
                }
            }
            
            // refresh not needed, it will be permformed when this VC appears
            [me.navigationController dismissViewControllerAnimated:YES completion:nil];
            
            // if any
            [[LCAWSFailedUpload objectForName:me.pictureFileURL] remove];
            
            [[LCAWSManager sharedManager] deleteFile:me.pictureFileURL forType:LCAWSManagerFileTypeM4A inBucketPath:AWSVoiceNotesPath withCompletionHandler:^(BOOL success, id object) {
                if (success) {
                    // if any
                    [[LCAWSFailedDeletion objectForName:me.pictureFileURL] remove];
                }
                else
                {
                    LCAWSFailedDeletion *failedDeletion = [LCAWSFailedDeletion new];
                    [failedDeletion setFileName:me.pictureFileURL];
                    [failedDeletion setType:LCAWSManagerFileTypeJPEG];
                    [failedDeletion store];
                }
            }];
        }
    }];
}

@end