//
//  LCImageVoicePickerCell.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 19/07/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCFormButtonPickerCell.h"
#import "LCAWSManager.h"

#import <FontAwesome+iOS/UIFont+FontAwesome.h>
#import <FontAwesome+iOS/NSString+FontAwesome.h>
#import <ASImageResize/UIImage+Resize.h>
#import <SGImageCache/SGImageCache.h>
#import <ALSystemUtilities/ALHardware.h>
#import <NSString+Validation/NSString+Validation.h>

const CGFloat kSpaceBetweenButtons = 8;
const CGFloat kSpaceBetweenBorders = 8;
const CGFloat kFontSize = 32;

@interface LCFormCellPickerButton ()

@property (nonatomic, strong) LCFormButtonPickerCell *parentCell;

@end

@implementation LCFormCellPickerButton

#pragma mark - Setters

-(void)setStatus:(LCFormCellPickerButtonState)status
{
    _status = status;
    
    NSMutableAttributedString *string = nil;
    
    switch (status) {
        case LCFormCellPickerButtonStatePlay:
        {
            string = [[NSMutableAttributedString alloc] initWithString:[NSString fontAwesomeIconStringForEnum:FAIconPlay] attributes:@{NSFontAttributeName:[UIFont fontAwesomeFontOfSize:kFontSize], NSForegroundColorAttributeName:companyColor()}];
            
        }
            break;
        case LCFormCellPickerButtonStateRecord:
        {
            string = [[NSMutableAttributedString alloc] initWithString:[NSString fontAwesomeIconStringForEnum:FAIconMicrophone] attributes:@{NSFontAttributeName:[UIFont fontAwesomeFontOfSize:kFontSize], NSForegroundColorAttributeName:companyColor()}];
            [self setAttributedTitle:string forState:UIControlStateNormal];
        }
            break;
        case LCFormCellPickerButtonStateDownloading:
        {
            string = [[NSMutableAttributedString alloc] initWithString:[NSString fontAwesomeIconStringForEnum:FAIconCloudDownload] attributes:@{NSFontAttributeName:[UIFont fontAwesomeFontOfSize:kFontSize], NSForegroundColorAttributeName:FlatWhiteDark}];
            [self setAttributedTitle:string forState:UIControlStateNormal];
        }
            break;
        case LCFormCellPickerButtonStateErrorUpload:
        case LCFormCellPickerButtonStateErrorDownload:
        {
            string = [[NSMutableAttributedString alloc] initWithString:[NSString fontAwesomeIconStringForEnum:FAIconExclamationSign] attributes:@{NSFontAttributeName:[UIFont fontAwesomeFontOfSize:kFontSize], NSForegroundColorAttributeName:FlatRed}];
        }
            break;
        case LCFormCellPickerButtonStateTakePicture:
        {
            string = [[NSMutableAttributedString alloc] initWithString:[NSString fontAwesomeIconStringForEnum:FAIconCamera] attributes:@{NSFontAttributeName:[UIFont fontAwesomeFontOfSize:kFontSize], NSForegroundColorAttributeName:companyColor()}];
        }
            break;
        case LCFormCellPickerButtonStateShowPicture:
        {
            string = [[NSMutableAttributedString alloc] initWithString:[NSString fontAwesomeIconStringForEnum:FAIconPicture] attributes:@{NSFontAttributeName:[UIFont fontAwesomeFontOfSize:kFontSize], NSForegroundColorAttributeName:companyColor()}];
        }
            break;
        case LCFormCellPickerButtonStateUploading:
        {
            string = [[NSMutableAttributedString alloc] initWithString:[NSString fontAwesomeIconStringForEnum:FAIconCloudUpload] attributes:@{NSFontAttributeName:[UIFont fontAwesomeFontOfSize:kFontSize], NSForegroundColorAttributeName:companyColor()}];
        }
            break;
        default:
            break;
    }
    
    [self setAttributedTitle:string forState:UIControlStateNormal];
}

@end

@interface LCFormButtonPickerCell ()

@property (nonatomic, strong) LCFormCellPickerButton *takePicture;
@property (nonatomic, strong) LCFormCellPickerButton *recordVoiceNote;

@end

@implementation LCFormButtonPickerCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:ClearColor];
    
    NSMutableArray *tempButtons = @[].mutableCopy;
    
    _takePicture = ({
        LCFormCellPickerButton *button = [[LCFormCellPickerButton alloc] initForAutoLayout];
        [self addSubview:button];
        [button setType:LCFormCellPickerButtonTypeCameraCapture];
        [button setBackgroundColor:[UIColor whiteColor]];
        [button autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, kSpaceBetweenBorders, 0, 0) excludingEdge:ALEdgeRight];
        [button.layer setCornerRadius:6];
        [button setShowsTouchWhenHighlighted:YES];
        [button addTarget:self action:@selector(takePictureButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [button setParentCell:self];
        
        [tempButtons addObject:button];
        
        button;
    });
    
    _recordVoiceNote = ({
        LCFormCellPickerButton *button = [[LCFormCellPickerButton alloc] initForAutoLayout];
        [self addSubview:button];
        [button setType:LCFormCellPickerButtonTypeVoiceRecorder];
        [button setBackgroundColor:[UIColor whiteColor]];
        [button autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, 0, 0, kSpaceBetweenBorders) excludingEdge:ALEdgeLeft];
        [button.layer setCornerRadius:6];
        [button setShowsTouchWhenHighlighted:YES];
        [button addTarget:self action:@selector(recordVoiceNoteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [button setParentCell:self];
        
        [tempButtons addObject:button];
        
        button;
    });
    
    _buttons = tempButtons;
    
    [self.superview.layer setBorderWidth:0];
}

-(void)layoutSubviews
{
    [_takePicture autoSetDimension:ALDimensionWidth toSize:self.frame.size.width/_buttons.count-kSpaceBetweenBorders-(kSpaceBetweenButtons/_buttons.count)];
    [_recordVoiceNote autoSetDimension:ALDimensionWidth toSize:self.frame.size.width/_buttons.count-kSpaceBetweenBorders-(kSpaceBetweenButtons/_buttons.count)];
    
    [super layoutSubviews];
}

-(void)update
{
    if ([(id<LCFXFormControllerExtensionsDelegate>)[self.field.formController delegate] respondsToSelector:@selector(formButtonPicker:stateForButton:atIndex:)]) {
        for (int k = 0; k < _buttons.count; k++) {
            [self setState:[(id<LCFXFormControllerExtensionsDelegate>)[self.field.formController delegate] formButtonPicker:self stateForButton:_buttons[k] atIndex:k] forButton:_buttons[k] atIndex:k];
        }
    }
    
    [super update];
}

#pragma mark - Setters

-(void)setState:(LCFormCellPickerButtonState)state forButton:(LCFormCellPickerButton *)button atIndex:(NSUInteger)index
{
    [button setStatus:state];
    
    if (button.type == LCFormCellPickerButtonTypeCameraCapture) {
        switch (button.status) {
            case LCFormCellPickerButtonStateShowPicture:
            {
                if ([(id<LCFXFormControllerExtensionsDelegate>)[self.field.formController delegate] respondsToSelector:@selector(formButtonPicker:pathForResourceForButton:atIndex:)]) {
                    NSString *fileName = [(id<LCFXFormControllerExtensionsDelegate>)[self.field.formController delegate] formButtonPicker:self pathForResourceForButton:button atIndex:index];
                    
                    if (![NSString isEmptyString:fileName]) {
                        LCAWSInUpload *inUpload = [LCAWSInUpload objectForName:fileName];
                        if (inUpload) {
                            button.status = LCFormCellPickerButtonStateUploading;
                        }
                        else
                        {
                            button.status = LCFormCellPickerButtonStateDownloading;
                            
                            [[LCAWSManager sharedManager] getFile:fileName forType:LCAWSManagerFileTypeJPEG inBucketPath:AWSPicturesPath withCompletionHandler:^(BOOL success, id object) {
                                if (success) {
                                    UIImage *cachedCroppedImage = [SGImageCache imageForCacheKey:fileName];
                                    if (!cachedCroppedImage) {
                                        cachedCroppedImage = [[UIImage imageWithData:object] resizedImageToSize: CGSizeMake(button.frame.size.width*[UIScreen mainScreen].scale, button.frame.size.height*[UIScreen mainScreen].scale)];
                                    } 
                                    
                                    [button setBackgroundImage:cachedCroppedImage forState:UIControlStateNormal];
                                    [button setClipsToBounds:YES];
                                    
                                    button.status = LCFormCellPickerButtonStateShowPicture;
                                }
                                else
                                {
                                    button.status = LCFormCellPickerButtonStateErrorDownload;
                                    [button setError:object];
                                }
                            }];
                        }
                    }
                    else
                    {
                        button.status = LCFormCellPickerButtonStateTakePicture;
                    }
                }
                else
                {
                    button.status = LCFormCellPickerButtonStateTakePicture;
                }
            }
                break;
            default:
                break;
        }
    }
    else if (button.type == LCFormCellPickerButtonTypeVoiceRecorder) {
        switch (button.status) {
            case LCFormCellPickerButtonStatePlay:
            {
                if ([(id<LCFXFormControllerExtensionsDelegate>)[self.field.formController delegate] respondsToSelector:@selector(formButtonPicker:pathForResourceForButton:atIndex:)]) {
                    NSString *fileName = [(id<LCFXFormControllerExtensionsDelegate>)[self.field.formController delegate] formButtonPicker:self pathForResourceForButton:button atIndex:index];
                    
                    if (![NSString isEmptyString:fileName]) {
                        LCAWSInUpload *inUpload = [LCAWSInUpload objectForName:fileName];
                        if (inUpload) {
                            button.status = LCFormCellPickerButtonStateUploading;
                        }
                        else
                        {
                            button.status = LCFormCellPickerButtonStateDownloading;
                            
                            [[LCAWSManager sharedManager] getFile:fileName forType:LCAWSManagerFileTypeM4A inBucketPath:AWSVoiceNotesPath withCompletionHandler:^(BOOL success, id object) {
                                if (success) {
                                    button.status = LCFormCellPickerButtonStatePlay;
                                }
                                else
                                {
                                    button.status = LCFormCellPickerButtonStateErrorDownload;
                                    [button setError:object];
                                }
                            }];
                        }
                    }
                    else
                    {
                        button.status = LCFormCellPickerButtonStateRecord;
                    }
                }
                else
                {
                    button.status = LCFormCellPickerButtonStateRecord;
                }
            }
                break;
            default:
                break;
        }
    }
}

#pragma mark - Actions

-(void)takePictureButtonPressed:(id)sender
{
    if ([(id<LCFXFormControllerExtensionsDelegate>)[self.field.formController delegate] respondsToSelector:@selector(formButtonPicker:didPressButton:atIndex:)]) {
        [(id<LCFXFormControllerExtensionsDelegate>)[self.field.formController delegate] formButtonPicker:self didPressButton:_takePicture atIndex:[_buttons indexOfObjectIdenticalTo:_takePicture]];
    }
}

-(void)recordVoiceNoteButtonPressed:(id)sender
{
    if ([(id<LCFXFormControllerExtensionsDelegate>)[self.field.formController delegate] respondsToSelector:@selector(formButtonPicker:didPressButton:atIndex:)]) {
        [(id<LCFXFormControllerExtensionsDelegate>)[self.field.formController delegate] formButtonPicker:self didPressButton:_recordVoiceNote atIndex:[_buttons indexOfObjectIdenticalTo:_recordVoiceNote]];
    }
}

@end