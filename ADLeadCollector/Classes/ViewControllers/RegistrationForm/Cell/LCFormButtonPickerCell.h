//
//  LCImageVoicePickerCell.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 19/07/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "FXForms.h"

typedef NS_ENUM(NSUInteger, LCFormCellPickerButtonState) {
    LCFormCellPickerButtonStatePlay = 0,
    LCFormCellPickerButtonStateRecord = 1,
    LCFormCellPickerButtonStateTakePicture = 11,
    LCFormCellPickerButtonStateShowPicture = 12,
    LCFormCellPickerButtonStateLoading = 20,
    LCFormCellPickerButtonStateUploading = 21,
    LCFormCellPickerButtonStateDownloading = 22,
    LCFormCellPickerButtonStateErrorUpload = 98,
    LCFormCellPickerButtonStateErrorDownload = 99
};

typedef NS_ENUM(NSUInteger, LCFormCellPickerButtonType) {
    LCFormCellPickerButtonTypeVoiceRecorder = 0,
    LCFormCellPickerButtonTypeCameraCapture = 1
};

@interface LCFormCellPickerButton : UIButton

@property (nonatomic) LCFormCellPickerButtonType type;
@property (nonatomic) LCFormCellPickerButtonState status;
@property (nonatomic) NSError *error;

@end

@interface LCFormButtonPickerCell : FXFormBaseCell

@property (nonatomic, strong, readonly) NSArray *buttons;

@end