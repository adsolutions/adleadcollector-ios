//
//  LCLeadPreviewViewController.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 28/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCLeadPreviewViewController.h"
#import "LCLead.h"
#import "LCDynamicForm.h"
#import "FXFormController+Multicast.h"
#import "LCActionMenu.h"

#import <RFGravatarImageView/RFGravatarImageView.h>
#import <MBTwitterScroll/MBTwitterScroll.h>
#import <UIColor-HexString/UIColor+HexString.h>
#import <FontAwesome+iOS/UIFont+FontAwesome.h>
#import <FontAwesome+iOS/NSString+FontAwesome.h>
#import <libPhoneNumber-iOS/NBPhoneNumberUtil.h>

@interface LCLeadPreviewViewController () <MBTwitterScrollDelegate, LCFXFormControllerExtensionsDelegate, LCActionMenuDelegate, UITableViewDataSource>

@property (nonatomic, strong) MBTwitterScroll *twitterScroll;
@property (nonatomic, strong) RFGravatarImageView *gravatarImageView;
@property (nonatomic, strong) FXFormController *form;
@property (nonatomic, strong) UIButton *actionButton;
@property (nonatomic, strong) NSArray *availableActions;

@end

@implementation LCLeadPreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated
{
    self.twitterScroll.frame = self.view.frame;
    self.twitterScroll.tableView.frame = self.view.frame;
    [self.twitterScroll.header setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.twitterScroll.header.frame.size.height)];
    [self.twitterScroll.headerLabel setCenter:CGPointMake(self.view.center.x, self.twitterScroll.headerLabel.center.y)];
    [self.twitterScroll.tableView.tableHeaderView setBackgroundColor:ClearColor];
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"ebebec"]];
    
    NSMutableArray *availableTempActions = @[].mutableCopy;
    
    NSString *actionsString = @"";
    if (_lead.email.length > 0) {
        actionsString = [actionsString stringByAppendingFormat:@" %@ ", [NSString fontAwesomeIconStringForEnum:FAIconEnvelope]];
        [availableTempActions addObject:@(LCActionMenuButtonTypeMail)];
    }
    
    if (_lead.phoneNumber.length > 0) {
        actionsString = [actionsString stringByAppendingFormat:@" %@ ", [NSString fontAwesomeIconStringForEnum:FAIconPhone]];
        [availableTempActions addObject:@(LCActionMenuButtonTypePhoneCall)];
        actionsString = [actionsString stringByAppendingFormat:@" %@ ", [NSString fontAwesomeIconStringForEnum:FAIconComment]];
        [availableTempActions addObject:@(LCActionMenuButtonTypeTextMessage)];
    }
    
    _availableActions = [availableTempActions copy];
    availableTempActions = nil;
    
    if (actionsString.length > 0) {
        _actionButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.view.frame)-120, CGRectGetMaxY(self.twitterScroll.avatarImage.frame)-30, 110, 40)];
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:actionsString attributes:@{NSFontAttributeName:[UIFont fontAwesomeFontOfSize:24], NSForegroundColorAttributeName:companyColor()}];
        [_actionButton setAttributedTitle:string forState:UIControlStateNormal];
        [_actionButton setBackgroundColor:ClearColor];
        [_actionButton.layer setCornerRadius:10];
        [_actionButton.layer setBorderWidth:1];
        [_actionButton.layer setBorderColor:companyColor().CGColor];
        [_actionButton setShowsTouchWhenHighlighted:YES];
        [_actionButton addTarget:self action:@selector(actionButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.twitterScroll.tableView.tableHeaderView addSubview:_actionButton];
    }
    
    FRDLivelyButton *closeButton = [LCUtils livelyButtonWithFrame:CGRectMake(0, 0, 25, 25) forType:kFRDLivelyButtonStyleClose target:self andAction:@selector(closeButtonPressed:)];
    [closeButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.twitterScroll.tableView.tableHeaderView addSubview:closeButton];
    [closeButton autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.twitterScroll.header withOffset:10];
    [closeButton autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.twitterScroll.header withOffset:-5];
    [closeButton autoSetDimensionsToSize:CGSizeMake(30, 30)];
}

#pragma mark - Setters

-(void)setLead:(LCLead *)lead
{
    _lead = lead;
    
    self.twitterScroll = [[MBTwitterScroll alloc] initTableViewWithBackgound:[UIImage imageNamed:@"your image"] avatarImage:[UIImage imageNamed:@"LeadsCollectorWhite"] titleString:_lead.nameSurname subtitleString:_lead.companyName buttonTitle:nil];
    self.twitterScroll.delegate = self;
    [self.view addSubview:self.twitterScroll];
    
    [self.twitterScroll.headerImageView setBackgroundColor:FlatGreenDark];
    
    self.gravatarImageView = [[RFGravatarImageView alloc] initWithFrame:self.twitterScroll.avatarImage.frame andPlaceholder:[UIImage imageNamed:@"LeadsCollectorWhite"]];
    
    if ([lead email].length > 0) {
        [self.gravatarImageView setEmail:lead.email];
        [self.gravatarImageView load:^(NSError *error) {
            if (!error) {
                self.twitterScroll.avatarImage.image = self.gravatarImageView.image;
            }
        }];
    }
    
    [self.twitterScroll.avatarImage setBackgroundColor:companyColor()];
    [self.twitterScroll.avatarImage.layer setCornerRadius:self.twitterScroll.avatarImage.frame.size.height/2];
    [self.twitterScroll.avatarImage.layer setBorderWidth:2];
    [self.twitterScroll.avatarImage.layer setBorderColor:companyColor().CGColor];
    
    _form = [[FXFormController alloc] init];
    [_form setForm:[[LCDynamicForm alloc] init]];
    [_form setTableView:self.twitterScroll.tableView];
    [[_form multicastDelegate] addDelegate:self];
    [_form.tableView setEditing:NO];
    [(LCDynamicForm *)_form.form setValuesByKey:_lead.document.userProperties.mutableCopy];
}

#pragma mark - Actions

-(void)closeButtonPressed:(id)sender
{
    [self mz_dismissFormSheetControllerAnimated:YES completionHandler:nil];
}

#pragma mark - MBTwitterScrollDelegate

-(void)recievedMBTwitterScrollEvent
{
    //
}

#pragma mark - UIScrollViewDelegate

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setUserInteractionEnabled:NO];
    
    if ((indexPath.section == [tableView numberOfSections]-1) && (indexPath.row == [tableView numberOfRowsInSection:indexPath.section] -1)) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    else
    {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
}

#pragma mark - Actions

-(void)actionButtonPressed:(id)sender
{
    [self performSegueWithIdentifier:@"blurOptionsView" sender:sender];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    LCActionMenu *destination = segue.destinationViewController;
    destination.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [destination setButtons:_availableActions];
    [destination setDelegate:self];
}

-(void)actionMenu:(LCActionMenu *)menu didPressButtonForType:(LCActionMenuButtonType)type
{
    if (type == LCActionMenuButtonTypeMail) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[@"mailto:" stringByAppendingString:_lead.email]]];
    }
    else if (type == LCActionMenuButtonTypePhoneCall)
    {
        NBPhoneNumberUtil *phoneUtil = [NBPhoneNumberUtil new];
        NBPhoneNumber *phoneNumber = [phoneUtil parse:_lead.phoneNumber defaultRegion:[NSLocale currentLocale].description error:nil];
        NSError *error = nil;
        NSString *number = [phoneUtil format:phoneNumber numberFormat:NBEPhoneNumberFormatRFC3966 error:&error];
        if (!error && number.length > 0) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:number]];
        }
        else
        {
            [CRToastManager showNotificationWithOptions:[LCUtils errorToastWithTitle:NSLocalizedString(@"kError", @"") andMessage:NSLocalizedString(@"kWrongNumberError", @"")] completionBlock:nil];
        }
    }
    else
    {
        NBPhoneNumberUtil *phoneUtil = [NBPhoneNumberUtil new];
        NBPhoneNumber *phoneNumber = [phoneUtil parse:_lead.phoneNumber defaultRegion:[NSLocale currentLocale].description error:nil];
        NSError *error = nil;
        NSString *number = [phoneUtil format:phoneNumber numberFormat:NBEPhoneNumberFormatRFC3966 error:&error];
        if (!error && number.length > 0) {
            NSURL *url = [NSURL URLWithString:[@"sms:" stringByAppendingString:number]];
            [[UIApplication sharedApplication] openURL:url];
        }
        else
        {
            [CRToastManager showNotificationWithOptions:[LCUtils errorToastWithTitle:NSLocalizedString(@"kError", @"") andMessage:NSLocalizedString(@"kWrongNumberError", @"")] completionBlock:nil];
        }
    }
}

-(NSString *)formButtonPicker:(LCFormButtonPickerCell *)picker pathForResourceForButton:(LCFormCellPickerButton *)button atIndex:(NSUInteger)buttonIndex
{
    return nil;
}

-(void)formButtonPicker:(LCFormButtonPickerCell *)picker didPressButton:(LCFormCellPickerButton *)button atIndex:(NSUInteger)buttonIndex
{
    
}

@end