//
//  LCLeadPreviewViewController.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 28/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "MZFormSheetController.h"

@class LCLead;

@interface LCLeadPreviewViewController : MZFormSheetController

@property (nonatomic, strong) LCLead *lead;

@end