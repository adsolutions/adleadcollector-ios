//
//  TFWarehouseViewController.h
//  TeamFactory
//
//  Created by Daniele Angeli on 07/04/15.
//  Copyright (c) 2015 Info-Team. All rights reserved.
//

#import "LCTableViewController.h"
#import "MBDataReceiver.h"

@class LCFolder;

@interface LCLeadsTableViewController : LCTableViewController <MBDataReceiver>

@property (nonatomic, strong) LCFolder *folder;

@end