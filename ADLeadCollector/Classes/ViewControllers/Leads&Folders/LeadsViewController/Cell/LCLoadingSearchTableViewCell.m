//
//  TFLoadingSearchTableViewCell.m
//  TeamFactory
//
//  Created by Daniele Angeli on 09/04/15.
//  Copyright (c) 2015 Info-Team. All rights reserved.
//

#import "LCLoadingSearchTableViewCell.h"

@interface LCLoadingSearchTableViewCell ()

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation LCLoadingSearchTableViewCell

-(void)layoutSubviews
{
    [_activityIndicator setCenter:self.center];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end