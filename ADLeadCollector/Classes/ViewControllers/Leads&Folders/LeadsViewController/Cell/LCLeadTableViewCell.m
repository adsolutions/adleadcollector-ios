//
//  TFWarehouseTableViewCell.m
//  TeamFactory
//
//  Created by Daniele Angeli on 08/04/15.
//  Copyright (c) 2015 Info-Team. All rights reserved.
//

#import "LCLeadTableViewCell.h"
#import "LCLead.h"
#import "NSDate+Extras.h"
#import "LCFolder.h"
#import "LCActionMenu.h"

#import <UIImageView-Letters/UIImageView+Letters.h>
#import <UIColor-HexString/UIColor+HexString.h>
#import <FontAwesome+iOS/UIImage+FontAwesome.h>
#import <NSString+Validation/NSString+Validation.h>

#define kDefaultImagesSize CGSizeMake(15, 15)

@interface LCLeadCellSideView : UIView

@property (nonatomic, strong) NSArray *items;

@end

@implementation LCLeadCellSideView
{
    NSArray *_internalItems;
}

-(id)init
{
    self = [super init];
    
    if (self) {
        [self setBackgroundColor:FlatWhite];
    }
    
    return self;
}

#pragma mark - Setters

-(void)setItems:(NSArray *)items
{
    _items = items;
    
    [_internalItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
        obj = nil;
    }];
    
    NSMutableArray *objects = @[].mutableCopy;
    
    UIView *previousObj = nil;
    
    for (NSNumber *type in items) {
        [objects addObject:({
            UIImageView *image = [[UIImageView alloc] initForAutoLayout];
            [self addSubview:image];
            [image autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self];
            [image autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self];
            
            if (previousObj) {
                [image autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:previousObj withOffset:4];
            }
        
            [image setContentMode:UIViewContentModeScaleAspectFit];
            [image setImage:[LCActionMenuImage imageWithSize:kDefaultImagesSize color:FlatWhiteDark andType:[type unsignedIntegerValue]]];
            
            previousObj = image;
            
            image;
        })];
    }
    
    if ([objects count] > 0) {
        NSUInteger index = [objects count]/2;
        UIView *middleView = [objects objectAtIndex:index];
        [middleView autoCenterInSuperview];
    }
    
    _internalItems = [objects copy];
    objects = nil;
}

@end

@interface LCLeadTableViewCell ()

@property (nonatomic, strong) UILabel *nameSurname;
@property (nonatomic, strong) UILabel *email;
@property (nonatomic, strong) UIImageView *initialsView;
@property (nonatomic, strong) LCLeadCellSideView *sideView;

@end

@implementation LCLeadTableViewCell

- (void)awakeFromNib
{
    _sideView = ({
        LCLeadCellSideView *view = [[LCLeadCellSideView alloc] initForAutoLayout];
        [self.contentView addSubview:view];
        [view autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.contentView];
        [view autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.contentView];
        [view autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.contentView];
        [view autoSetDimension:ALDimensionWidth toSize:20];
        view;
    });
    
    _initialsView = ({
        UIImageView *initialsView = [[UIImageView alloc] initForAutoLayout];
        [self.contentView addSubview:initialsView];
        [initialsView autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.contentView withOffset:15];
        [initialsView autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:_sideView withOffset:8];
        [initialsView autoSetDimension:ALDimensionWidth toSize:40];
        [initialsView autoSetDimension:ALDimensionHeight toSize:40];
        [initialsView setBackgroundColor:[UIColor clearColor]];
        initialsView;
    });
    
    _nameSurname = ({
        UILabel *nameSurname = [[UILabel alloc] initForAutoLayout];
        [self.contentView addSubview:nameSurname];
        [nameSurname autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.contentView withOffset:15];
        [nameSurname autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:_initialsView withOffset:8];
        [nameSurname setFont:[LCUtils boldFontWithSize:18]];
        [nameSurname autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.contentView withOffset:10];
        [nameSurname setTextColor:FlatBlack];
        nameSurname;
    });
    
    _email = ({
        UILabel *email = [[UILabel alloc] initForAutoLayout];
        [self.contentView addSubview:email];
        [email autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_nameSurname withOffset:8];
        [email autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:_initialsView withOffset:8];
        [email autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.contentView withOffset:10];
        [email setFont:[LCUtils fontWithSize:17]];
        [email setTextColor:FlatBlack];
        email;
    });
    
    UIView *bottomLine = [[UIView alloc] initForAutoLayout];
    [self.contentView addSubview:bottomLine];
    [bottomLine autoSetDimension:ALDimensionHeight toSize:1];
    [bottomLine autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero excludingEdge:ALEdgeTop];
    [bottomLine setBackgroundColor:FlatWhite];
    
    [self setTintColor:companyColor()];
    self.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - Setters

-(void)setLeadObject:(LCLead *)leadObject
{
    _leadObject = leadObject;
    
    [_nameSurname setText:![NSString isEmptyString:_leadObject.companyName]? _leadObject.companyName : _leadObject.nameSurname];
    [_email setText:_leadObject.email];
    
    NSString *initials = _nameSurname.text;
    
    if ([NSString isEmptyString:_nameSurname.text]) {
        initials = @"N A";
    }
    
    NSString *hexColor = [[[MBEngine sharedEngine] documentWithID:leadObject.associatedFolder] userProperties][@"hexColor"];
    if (!hexColor) {
        hexColor = [LCUtils hexStringFromColor:companyColor()];
    }
    
    UIColor *circleColor = [UIColor colorWithHexString:hexColor];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // switch to a background thread and perform your expensive operation
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // switch back to the main thread to update your UI
            [_initialsView setImageWithString:initials color:circleColor circular:YES];
        });
    });
    
    NSMutableArray *itemsArray = @[].mutableCopy;
    if (![NSString isEmptyString:leadObject.audioFileURLs.firstObject]) {
        [itemsArray addObject:@(LCActionMenuButtonTypeMic)];
    }
    
    if (![NSString isEmptyString:leadObject.pictureFileURLs.firstObject]) {
        [itemsArray addObject:@(LCActionMenuButtonTypePicture)];
    }
    
    if (leadObject.starred) {
        [itemsArray addObject:@(LCActionMenuButtonTypeStarred)];
        self.sideView.backgroundColor = FlatGold;
    }
    else
    {
        self.sideView.backgroundColor = FlatWhite;
    }
    
    [_sideView setItems:itemsArray];
}

@end