//
//  TFSearchTableViewCell.m
//  TeamFactory
//
//  Created by Daniele Angeli on 09/04/15.
//  Copyright (c) 2015 Info-Team. All rights reserved.
//

#import "LCLeadSearchTableViewCell.h"

@implementation LCLeadSearchTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self awakeFromNib];
    }
    
    return self;
}

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end