//
//  TFWarehouseTableViewCell.h
//  TeamFactory
//
//  Created by Daniele Angeli on 08/04/15.
//  Copyright (c) 2015 Info-Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MGSwipeTableCell/MGSwipeTableCell.h>

@class LCLead;

@interface LCLeadTableViewCell : MGSwipeTableCell

@property (nonatomic, strong) LCLead *leadObject;
@property (nonatomic, strong) NSIndexPath *indexPath;

@end