//
//  TFSearchTableViewCell.h
//  TeamFactory
//
//  Created by Daniele Angeli on 09/04/15.
//  Copyright (c) 2015 Info-Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LCLeadTableViewCell.h"

@interface LCLeadSearchTableViewCell : LCLeadTableViewCell

@end