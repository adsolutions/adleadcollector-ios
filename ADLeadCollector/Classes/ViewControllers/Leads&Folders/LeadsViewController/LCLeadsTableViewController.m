//
//  TFWarehouseViewController.m
//  TeamFactory
//
//  Created by Daniele Angeli on 07/04/15.
//  Copyright (c) 2015 Info-Team. All rights reserved.
//

#import "LCLeadsTableViewController.h"
#import "LCLead.h"
#import "MBDataProvider.h"
#import "LCLeadTableViewCell.h"
#import "LCSearchResultsDataProvider.h"
#import "LCLeadSearchTableViewCell.h"
#import "LCFormViewController.h"
#import "LCSettings.h"
#import "LCAlertView.h"
#import "LCRegistrationFormViewController.h"
#import "LCAlwaysTemplateImage.h"
#import "LCFolder.h"
#import "LCLeftViewController.h"
#import "LCLeadPreviewViewController.h"
#import "LCLoadingSearchTableViewCell.h"
#import "LCAWSManager.h"

#import <UIColor-HexString/UIColor+HexString.h>
#import <MGSwipeTableCell/MGSwipeButton.h>
#import <FRDLivelyButton/FRDLivelyButton.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <libPhoneNumber-iOS/NBPhoneNumberUtil.h>
#import <FontAwesome+iOS/UIImage+FontAwesome.h>
#import <UIAlertView+Blocks/UIAlertView+Blocks.h>
#import <NSString+Validation/NSString+Validation.h>
#import <MobileBridge/MobileBridge.h>

NSUInteger const LeadsPerPage = 50;

#define DefaultIconSize CGSizeMake(30, 30)

@interface LCLeadsTableViewController () <MBDataProviderDelegate, UISearchDisplayDelegate, MGSwipeTableCellDelegate, LCRegistrationFormDelegate>

@property (nonatomic, strong) LCSearchResultsDataProvider *searchResultDataProvider;

@end

@implementation LCLeadsTableViewController
{
    NSIndexPath *_loaderCellIndexPath;
    NSIndexPath *_selectedCellIndexPath;
}

@synthesize dataProvider = _dataProvider;

- (void)viewDidLoad {
    //        LCFolder *folder = [LCFolder createNew];
    //            [folder setName:@"Pitti Uomo"];
    //            [folder save];
    //
    //            [LCUtils addPeople:5 toFolder:folder];
    
    [super viewDidLoad];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:[LCUtils livelyButtonWithFrame:CGRectMake(0, 0, 25, 25) forType:kFRDLivelyButtonStylePlus target:self andAction:@selector(addLeadButtonPressed:)]]];
    
    self.searchDisplayController.displaysSearchBarInNavigationBar = NO;
    [self.searchDisplayController.searchResultsTableView setRowHeight:65];
    [self.searchDisplayController.searchResultsTableView registerClass:[LCLeadSearchTableViewCell class] forCellReuseIdentifier:@"LCLeadSearchTableViewCell"];
    [self.searchDisplayController.searchResultsTableView registerClass:[LCLoadingSearchTableViewCell class] forCellReuseIdentifier:@"LCLoadingSearchTableViewCell"];
    [self.searchDisplayController.searchBar setBarTintColor:companyColor()];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.searchDisplayController.searchBar setPlaceholder:_folder? [NSString stringWithFormat:NSLocalizedString(@"kSearchIn%@", @""), _folder.name] : NSLocalizedString(@"kSearchLeads", @"")];
}

#pragma mark - Setters

-(void)setFolder:(LCFolder *)folder
{
    _folder = folder;
    
    [self reloadData];
}

#pragma mark - Overrides

-(void)reloadData
{
    self.dataProvider = nil;
    self.searchResultDataProvider.dataProvider = nil;
    
    NSPredicate *predicate = nil;
    
    if (_folder) {
        predicate = [NSPredicate predicateWithFormat:@"value._id IN (%@)", [_folder.relatedLeads componentsJoinedByString:@","]];
    }
    
    NSUInteger leadsCount = [LCLead countAllMatchingPredicate:predicate];
    
    if (leadsCount > 0) {
        if (leadsCount == 1) {
            [self setTitle:_folder? [NSString stringWithFormat:@"%@ - 1 Lead", NSLocalizedString(_folder.name, @"")] : @"Leads (1)"];
        }
        else
        {
            [self setTitle:_folder? [NSString stringWithFormat:@"%@ - %lu Leads", NSLocalizedString(_folder.name, @""), (unsigned long)leadsCount] : [NSString stringWithFormat:@"Leads (%lu)", (unsigned long)leadsCount]];
        }
        
        self.dataProvider = nil;
        self.dataProvider = [[MBDataProvider alloc] initForModelClass:[LCLead class] withPageSize:leadsCount >= LeadsPerPage? LeadsPerPage : leadsCount totalObjectsCount:leadsCount withFilter:predicate sortedBy:nil];
        self.dataProvider.shouldLoadAutomatically = leadsCount > 0;
        [self.dataProvider setDelegate:self];
        
        //        [[RACObserve(self.dataProvider, dataObjects) deliverOnMainThread] subscribeNext:^(id x) {
        //            NSArray *visibleRowsIndexPaths = [self.tableView indexPathsForVisibleRows];
        //
        //            if ([visibleRowsIndexPaths count] == 0) {
        //                return;
        //            }
        //
        //            UITableViewCell *cell = [self tableView:self.tableView cellForRowAtIndexPath:visibleRowsIndexPaths.firstObject];
        //            if (cell) {
        //                [self.tableView scrollToRowAtIndexPath:visibleRowsIndexPaths.firstObject atScrollPosition:UITableViewScrollPositionTop animated:YES];
        //            }
        //        }];
    }
    else
    {
        [self setTitle:_folder? [NSString stringWithFormat:NSLocalizedString(@"kNoLeads%@", @""), NSLocalizedString(_folder.name, @"")] : NSLocalizedString(@"kNoLeads", @"")];
        self.dataProvider = nil;
    }
    
    [super reloadData];
    
    if (![NSString isEmptyString:self.searchDisplayController.searchBar.text]) {
        [self.searchDisplayController.searchBar setText:self.searchDisplayController.searchBar.text];
    }
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = NSLocalizedString(@"kNoLeadsAvailable", @"TableView");
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [LCUtils boldFontWithSize:17.0],
                                 NSForegroundColorAttributeName: FlatGray,
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

#pragma mark - Setters

-(NSPredicate *)predicateForFolder:(LCFolder *)folder withSearchString:(NSString *)searchString
{
    return [NSPredicate predicateWithBlock:^BOOL(LCLead *evaluatedObject, NSDictionary *bindings) {
        if (folder) {
            for (NSString *documentID in folder.relatedLeads) {
                if ([evaluatedObject.document.documentID isEqualToString:documentID])
                {
                    if ([[evaluatedObject.document.userProperties[@"nameSurname"] lowercaseString] containsString:[searchString lowercaseString]]) {
                        return YES;
                    }
                }
            }
        }
        else
        {
            if ([[evaluatedObject.document.userProperties[@"nameSurname"] lowercaseString] containsString:[searchString lowercaseString]]) {
                return YES;
            }
        }
        
        return NO;
    }];
}

#pragma mark - TableviewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return self.searchResultDataProvider.dataProvider.dataObjects.count == 0? 0 : 1;
    }
    
    return self.dataProvider.dataObjects.count == 0? 0 : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return MIN(self.searchResultDataProvider.dataProvider.loadedCount+1, self.searchResultDataProvider.dataProvider.dataObjects.count);
    }
    
    return MIN(self.dataProvider.loadedCount+1, self.dataProvider.dataObjects.count);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *DataCellIdentifier = @"LCLeadTableViewCell";
    static NSString *LoaderCellIdentifier = @"LCLoadingSearchTableViewCell";
    static NSString *SearchCellIdentifier = @"LCLeadSearchTableViewCell";
    static NSString *LoadingSearchCellIdentifier = @"LCLoadingSearchTableViewCell";
    
    NSString *cellIdentifier;
    NSUInteger index = indexPath.row;
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        if (index < self.searchResultDataProvider.dataProvider.loadedCount) {
            cellIdentifier = SearchCellIdentifier;
        }
        else
        {
            cellIdentifier = LoadingSearchCellIdentifier;
            self.searchResultDataProvider.loaderCellIndexPath = indexPath;
        }
        
        id cell = [self.searchDisplayController.searchResultsTableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cellIdentifier == SearchCellIdentifier && [cell isKindOfClass:[LCLeadSearchTableViewCell class]]) {
            if (!cell) {
                cell = [[LCLeadSearchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            
            LCLead *lead = self.searchResultDataProvider.dataProvider.dataObjects[indexPath.row];
            [cell setLeadObject:lead];
            ((LCLeadTableViewCell *)cell).delegate = self;
            
            [cell setIndexPath:indexPath];
        }
        else
        {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        
        return cell;
    }
    
    if (index < self.dataProvider.loadedCount) {
        cellIdentifier = DataCellIdentifier;
    }
    else {
        cellIdentifier = LoaderCellIdentifier;
        _loaderCellIndexPath = indexPath;
    }
    
    id cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cellIdentifier == DataCellIdentifier && [cell isKindOfClass:[LCLeadTableViewCell class]]) {
        LCLead *lead = self.dataProvider.dataObjects[indexPath.row];
        [cell setLeadObject:lead];
        ((LCLeadTableViewCell *)cell).delegate = self;
        
        [cell setIndexPath:indexPath];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }
    else
    {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    return cell;
}

#pragma mark - DataProviderDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:self.tableView]) {
        if ([_loaderCellIndexPath isEqual:indexPath]) {
            return;
        }
    }
    else
    {
        if ([self.searchResultDataProvider.loaderCellIndexPath isEqual:indexPath]) {
            return;
        }
    }
    
    _selectedCellIndexPath = indexPath;
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self tableView:tableView accessoryButtonTappedForRowWithIndexPath:indexPath];
    return;
    
    //    LCLead *lead = tableView == self.searchDisplayController.searchResultsTableView? [self.searchResultDataProvider.dataProvider.dataObjects objectAtIndex:indexPath.row] : [self.dataProvider.dataObjects objectAtIndex:indexPath.row];
    //    LCLead *newThreadedLead = [LCLead modelForDocument:[[MBEngine sharedEngine] documentWithID:lead.document.documentID]];
    //
    //    LCLeadPreviewViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LCLeadPreviewViewController"];
    //
    //    [viewController setLead:newThreadedLead];
    //
    //    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    //    [navigationController setNavigationBarHidden:YES];
    //
    //    CGSize size = CGSizeZero;
    //
    //    if (IS_IPAD) {
    //        size = CGSizeMake(500, 600);
    //    }
    //    else
    //    {
    //        size = CGSizeMake([UIScreen mainScreen].bounds.size.width/100*90, [UIScreen mainScreen].bounds.size.height/100*80);
    //    }
    //
    //    MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithSize:size viewController:navigationController];
    //    [formSheet setCornerRadius:5];
    //    [formSheet setShouldCenterVertically:YES];
    //    [formSheet setMovementWhenKeyboardAppears:IS_IPAD? MZFormSheetWhenKeyboardAppearsDoNothing : MZFormSheetWhenKeyboardAppearsMoveToTop];
    //    [formSheet setTransitionStyle:MZFormSheetTransitionStyleSlideFromBottom];
    //    [formSheet setShouldDismissOnBackgroundViewTap:YES];
    //
    //    [navigationController setPreferredContentSize:size];
    //    [viewController setPreferredContentSize:size];
    //    [viewController setPresentedFormSheetSize:size];
    //    [viewController.view setFrame:CGRectMake(0, 0, size.width, size.height)];
    //
    //    [self mz_presentFormSheetController:formSheet animated:YES completionHandler:nil];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        if ([indexPath isEqual:self.searchResultDataProvider.loaderCellIndexPath]) {
            [self.searchResultDataProvider.dataProvider loadDataForIndex:indexPath.row];
        }
    }
    else
    {
        if ([indexPath isEqual:_loaderCellIndexPath]) {
            [self.dataProvider loadDataForIndex:indexPath.row];
        }
    }
}

#pragma mark - Data controller delegate

- (void)dataProvider:(MBDataProvider *)dataProvider willLoadDataAtIndexes:(NSIndexSet *)indexes {
    //
}

- (void)dataProvider:(MBDataProvider *)dataProvider didLoadDataAtIndexes:(NSIndexSet *)indexes {
    if (dataProvider == _dataProvider) {
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[_loaderCellIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        [indexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
            if (idx < dataProvider.dataObjects.count-1) {
                [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx+1 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
            }
        }];
        [self.tableView endUpdates];
    }
    else
    {
        [self.searchDisplayController.searchResultsTableView beginUpdates];
        [self.searchDisplayController.searchResultsTableView reloadRowsAtIndexPaths:@[self.searchResultDataProvider.loaderCellIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        [indexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
            if (idx < self.searchResultDataProvider.dataProvider.dataObjects.count-1) {
                [self.searchDisplayController.searchResultsTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx+1 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
            }
        }];
        [self.searchDisplayController.searchResultsTableView endUpdates];
    }
    
    _loaderCellIndexPath = nil;
    self.searchResultDataProvider.loaderCellIndexPath = nil;
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        if (indexPath.row == self.searchResultDataProvider.dataProvider.loadedCount) {
            [self.searchResultDataProvider.dataProvider loadDataForIndex:indexPath.row];
        }
    }
    else
    {
        if (indexPath.row == self.dataProvider.loadedCount) {
            [self.dataProvider loadDataForIndex:indexPath.row];
        }
    }
    
    LCLead *lead = tableView == self.searchDisplayController.searchResultsTableView? [self.searchResultDataProvider.dataProvider.dataObjects objectAtIndex:indexPath.row] : [self.dataProvider.dataObjects objectAtIndex:indexPath.row];
    LCLead *newThreadedLead = [[LCLead alloc] initFromDocumentID:lead.document.documentID];
    
    LCRegistrationFormViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LCRegistrationFormViewController"];
    [viewController setDelegate:self];
    [viewController setLead:newThreadedLead];
    
    if (_folder) {
        [viewController setFolder:_folder];
    }
    else if (!_folder && ![NSString isEmptyString:lead.associatedFolder]) {
        [viewController setFolder:[[LCFolder alloc] initFromDocumentID:lead.associatedFolder]];
    }
    
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - UISearchDisplayDelegate

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    if ([NSString isEmptyString:searchString]) {
        return NO;
    }
    
    NSUInteger count = [LCLead countAllMatchingPredicate:[self predicateForFolder:_folder withSearchString:searchString]];
    
    self.searchResultDataProvider.dataProvider = nil;
    self.searchResultDataProvider = nil;
    
    self.dataProvider = nil;
    
    if (count == 0) {
        return YES;
    }
    
    self.searchResultDataProvider = [LCSearchResultsDataProvider new];
    [self.searchResultDataProvider setLoaderCellIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    self.searchResultDataProvider.dataProvider = [[MBDataProvider alloc] initForModelClass:[LCLead class] withPageSize:count >= LeadsPerPage? LeadsPerPage : count totalObjectsCount:count withFilter:[self predicateForFolder:_folder withSearchString:searchString] sortedBy:nil];
    [self.searchResultDataProvider.dataProvider setDelegate:self];
    self.searchResultDataProvider.dataProvider.shouldLoadAutomatically = YES;
    [self.searchResultDataProvider.dataProvider loadDataForIndex:0];
    
    return YES;
}

-(void)searchDisplayController:(UISearchDisplayController *)controller didHideSearchResultsTableView:(UITableView *)tableView
{
    [self reloadData];
}

#pragma mark - Actions

-(void)addLeadButtonPressed:(id)sender
{
    if ([LCUtils needsPremium] && (![LCUtils isPremium] || ![LCUtils isUnlimited])) {
        [UIAlertView showWithTitle:NSLocalizedString(@"kGet-Premium", @"") message:NSLocalizedString(@"kGet-PremiumInfo", @"") cancelButtonTitle:NSLocalizedString(@"kGet-Premium", @"") otherButtonTitles:@[NSLocalizedString(@"kCancel", @"")] tapBlock:^(UIAlertView * __nonnull alertView, NSInteger buttonIndex) {
            if (alertView.cancelButtonIndex == buttonIndex) {
                [(LCLeftViewController *)self.sideMenuViewController.leftMenuViewController showGetPremiumView];
            }
        }];
    }
    else
    {
        LCRegistrationFormViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LCRegistrationFormViewController"];
        [viewController setDelegate:self];
        [viewController setFolder:_folder];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

#pragma mark - MGSwipeTableCellDelegate

-(NSArray *)swipeTableCell:(MGSwipeTableCell *)cell swipeButtonsForDirection:(MGSwipeDirection)direction swipeSettings:(MGSwipeSettings *)swipeSettings expansionSettings:(MGSwipeExpansionSettings *)expansionSettings
{
    NSMutableArray *buttonsArray = @[].mutableCopy;
    
    __block typeof(self) me = self;
    
    if (direction == MGSwipeDirectionLeftToRight) {
        // phone call
        if (![NSString isEmptyString:((LCLeadTableViewCell *)cell).leadObject.phoneNumber]) {
            //configure left buttons
            [buttonsArray addObject:[MGSwipeButton buttonWithTitle:@"" icon:[UIImage imageWithIcon:@"icon-phone" backgroundColor:ClearColor iconColor:[UIColor whiteColor] iconScale:1 andSize:DefaultIconSize] backgroundColor:FlatNavyBlue callback:^BOOL(MGSwipeTableCell *sender) {
                NBPhoneNumberUtil *phoneUtil = [NBPhoneNumberUtil new];
                NBPhoneNumber *phoneNumber = [phoneUtil parse:((LCLeadTableViewCell *)cell).leadObject.phoneNumber defaultRegion:[NSLocale currentLocale].description error:nil];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[phoneUtil format:phoneNumber numberFormat:NBEPhoneNumberFormatRFC3966 error:nil]]];
                
                [cell refreshContentView];
                
                return YES;
            }]];
        }
        
        // email
        if (![NSString isEmptyString:((LCLeadTableViewCell *)cell).leadObject.email])
        {
            //configure left buttons
            [buttonsArray addObject:[MGSwipeButton buttonWithTitle:@"" icon:[UIImage imageWithIcon:@"icon-envelope" backgroundColor:ClearColor iconColor:[UIColor whiteColor] iconScale:1 andSize:DefaultIconSize] backgroundColor:FlatSkyBlue callback:^BOOL(MGSwipeTableCell *sender) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[@"mailto:" stringByAppendingString:((LCLeadTableViewCell *)cell).leadObject.email]]];
                
                [cell refreshContentView];
                
                return YES;
            }]];
        }
        
        // star
        [buttonsArray addObject:[MGSwipeButton buttonWithTitle:@"" icon:[UIImage imageWithIcon:((LCLeadTableViewCell *)cell).leadObject.starred? @"icon-star" : @"icon-star-empty" backgroundColor:ClearColor iconColor:[UIColor whiteColor] iconScale:1 andSize:DefaultIconSize] backgroundColor:FlatGold callback:^BOOL(MGSwipeTableCell *sender) {
            LCLead *newThreadedLead = [[LCLead alloc] initFromDocumentID:((LCLeadTableViewCell *)cell).leadObject.document.documentID];
            newThreadedLead.starred = !newThreadedLead.starred;
            [((LCLeadTableViewCell *)cell) performSelectorOnMainThread:@selector(setLeadObject:) withObject:newThreadedLead waitUntilDone:YES];
            [newThreadedLead performSelectorOnMainThread:@selector(saveDocument) withObject:newThreadedLead waitUntilDone:YES];
            
            [cell refreshContentView];
            [cell refreshButtons:YES];
            
            return YES;
        }]];
        
        [((LCLeadTableViewCell *)cell) setLeftSwipeSettings:swipeSettings];
        [((LCLeadTableViewCell *)cell) setLeftExpansion:expansionSettings];
    }
    else
    {
        //configure right buttons - TRASH
        [buttonsArray addObject:[MGSwipeButton buttonWithTitle:@"" icon:[UIImage imageWithIcon:@"icon-trash" backgroundColor:ClearColor iconColor:[UIColor whiteColor] iconScale:1 andSize:DefaultIconSize] backgroundColor:FlatRed callback:^BOOL(MGSwipeTableCell *sender) {
            [UIAlertView showWithTitle:NSLocalizedString(@"kLeadDeletion", @"") message:NSLocalizedString(@"kLeadDeletionMessage", @"") cancelButtonTitle:@"OK" otherButtonTitles:@[NSLocalizedString(@"kCancel", @"")] tapBlock:^(UIAlertView * __nonnull alertView, NSInteger buttonIndex) {
                if (alertView.cancelButtonIndex == buttonIndex) {
                    LCLead *newThreadedLead = [[LCLead alloc] initFromDocumentID:((LCLeadTableViewCell *)cell).leadObject.document.documentID];
                    
                    if (![NSString isEmptyString:newThreadedLead.pictureFileURLs.firstObject]) {
                        [[LCAWSFailedUpload objectForName:newThreadedLead.pictureFileURLs.firstObject] remove];
                        [[LCAWSManager sharedManager] deleteFile:newThreadedLead.pictureFileURLs.firstObject forType:LCAWSManagerFileTypeJPEG inBucketPath:AWSPicturesPath withCompletionHandler:^(BOOL success, id object) {/**/}];
                    }
                    
                    if (![NSString isEmptyString:newThreadedLead.audioFileURLs.firstObject]) {
                        [[LCAWSFailedUpload objectForName:newThreadedLead.audioFileURLs.firstObject] remove];
                        [[LCAWSManager sharedManager] deleteFile:newThreadedLead.audioFileURLs.firstObject forType:LCAWSManagerFileTypeM4A inBucketPath:AWSVoiceNotesPath withCompletionHandler:^(BOOL success, id object) {/**/}];
                    }
                    
                    if (!me.folder) {
                        NSMutableArray *foldersContainingID = @[].mutableCopy;
                        NSArray *folders = [LCFolder findAll];
                        [folders enumerateObjectsUsingBlock:^(LCFolder *obj, NSUInteger idx, BOOL *stop) {
                            if ([obj.relatedLeads containsObject:newThreadedLead.document.documentID]) {
                                [foldersContainingID addObject:obj];
                            }
                        }];
                        
                        if (foldersContainingID.count > 0) {
                            for (LCFolder *folder in foldersContainingID) {
                                NSMutableArray *relatedLeads = folder.relatedLeads.mutableCopy;
                                [relatedLeads removeObject:newThreadedLead.document.documentID];
                                [newThreadedLead deleteDocument];
                            }
                        }
                        else
                        {
                            [newThreadedLead deleteDocument];
                        }
                    }
                    else
                    {
                        NSMutableArray *relatedLeads = me.folder.relatedLeads.mutableCopy;
                        [relatedLeads removeObject:newThreadedLead.document.documentID];
                        [newThreadedLead deleteDocument];
                    }
                    
                    [cell refreshContentView];
                    
                    [me mobileBridgeDidUpdateDataWithNotification:nil];
                }
            }];
            return YES;
        }]];
        
        [((LCLeadTableViewCell *)cell) setRightSwipeSettings:swipeSettings];
        [((LCLeadTableViewCell *)cell) setRightExpansion:expansionSettings];
    }
    
    return buttonsArray;
}

-(BOOL)swipeTableCell:(LCLeadTableViewCell *)cell canSwipe:(MGSwipeDirection)direction fromPoint:(CGPoint)point
{
    MGSwipeExpansionSettings *expansionSettings = [[MGSwipeExpansionSettings alloc] init];
    [expansionSettings setButtonIndex:0];
    [expansionSettings setFillOnTrigger:YES];
    [expansionSettings setThreshold:3];
    
    MGSwipeSettings *swipeSettings = [[MGSwipeSettings alloc] init];
    [swipeSettings setTransition:MGSwipeTransitionBorder];
    [swipeSettings setOnlySwipeButtons:NO];
    
    return [[self swipeTableCell:cell swipeButtonsForDirection:direction swipeSettings:swipeSettings expansionSettings:expansionSettings] count] > 0;
}

#pragma mark - Overrides

-(void)mobileBridgeDidUpdateDataWithNotification:(NSNotification *)notification
{
    if (!notification) {
        [super mobileBridgeDidUpdateDataWithNotification:nil];
    }
}

#pragma mark - LCRegistrationFormDelegate

-(void)registrationFormViewController:(LCRegistrationFormViewController *)controller didAddLead:(LCLead *)lead
{
    // Answers
    [Answers logCustomEventWithName:@"Lead Created" customAttributes:[LCUtils answersUserDetails]];
    
    [super mobileBridgeDidUpdateDataWithNotification:nil];
}

-(void)registrationFormViewController:(LCRegistrationFormViewController *)controller didUpdateLead:(LCLead *)lead
{
    if (_selectedCellIndexPath) {
        LCLeadTableViewCell *cell = (LCLeadTableViewCell *)[self.tableView cellForRowAtIndexPath:_selectedCellIndexPath];
        if ([cell respondsToSelector:@selector(setLeadObject:)]) {
            [cell setLeadObject:lead];
        }
    }
}

@end