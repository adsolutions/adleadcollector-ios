//
//  TFSearchResultsDataProvider.m
//  TeamFactory
//
//  Created by Daniele Angeli on 09/04/15.
//  Copyright (c) 2015 Info-Team. All rights reserved.
//

#import "LCSearchResultsDataProvider.h"
#import "MBDataProvider.h"

@interface LCSearchResultsDataProvider () <MBDataProviderDelegate>

@end

@implementation LCSearchResultsDataProvider

@synthesize dataProvider = _dataProvider;

@end