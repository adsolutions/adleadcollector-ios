//
//  LCFolderCollectionViewCell.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 29/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCFolderCollectionViewCell.h"
#import "LCActionMenu.h"
#import "LCColorPickerCollectionViewController.h"
#import "LCNavigationController.h"
#import "LCLead.h"

#import <DPMeterView/DPMeterView.h>
#import <UIColor-HexString/UIColor+HexString.h>
#import <UIAlertView+Blocks/UIAlertView+Blocks.h>
#import <NSString+Validation/NSString+Validation.h>

@interface LCFolderCollectionViewCell () <LCActionMenuDelegate, LCColorPickerDelegate>

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalLeadsLabel;
@property (nonatomic, weak) IBOutlet UIImageView *iconImage;
@property (nonatomic, weak) IBOutlet DPMeterView *meterView;

@end

@implementation LCFolderCollectionViewCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    [[DPMeterView appearance] setTrackTintColor:FlatWhite];
    [[DPMeterView appearance] setProgressTintColor:companyColor()];
    [_meterView setMeterType:DPMeterTypeLinearVertical];
    [_meterView setShape:[UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, _meterView.frame.size.width, _meterView.frame.size.height)].CGPath];
    [_meterView setCenter:self.center];
    [_meterView setProgress:0];
    
    // TODO: i've disabled it due to high CPU usage > need more investigation
    // [_meterView startGravity];
    
    [_totalLeadsLabel setFont:[LCUtils boldFontWithSize:24]];
    [_nameLabel setFont:[LCUtils fontWithSize:16]];
    [_nameLabel setTextColor:FlatBlack];
}

-(void)setHighlighted:(BOOL)highlighted
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.25 animations:^{
            [self setTransform:highlighted? CGAffineTransformMakeScale(0.80, 0.80) : CGAffineTransformIdentity];
        }];
    });
}

-(void)removeFromSuperview
{
    // TODO: i've disabled it due to high CPU usage > need more investigation
//    [_meterView stopGravity];
    
    [super removeFromSuperview];
}

#pragma mark - Setters

-(void)setFolder:(LCFolder *)folder
{
    _folder = folder;
    
    [_meterView setProgressTintColor:_folder.hexColor? [UIColor colorWithHexString:_folder.hexColor] : companyColor()];
    
    [_nameLabel setText:NSLocalizedString(folder.name, @"")];
    [_totalLeadsLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)folder.leadsCount]];
}

-(void)setPercentage:(CGFloat)percentage
{
    dispatch_async(dispatch_get_main_queue(), ^{
        _percentage = percentage;
        
        if (_percentage > 0.55f) {
            [_totalLeadsLabel setTextColor:ContrastColor(self.meterView.progressTintColor, YES)];
        }
        else
        {
            [_totalLeadsLabel setTextColor:ContrastColor(self.meterView.trackTintColor, YES)];
        }
        
        [_meterView setProgress:percentage animated:YES];
    });
}

#pragma mark - MenuControllerActions

-(void)renameMenuItemPressed:(id)sender
{
    __block typeof(self) me = self;
    
    UIAlertView *alertView = [UIAlertView showWithTitle:NSLocalizedString(@"kRenameFolder", @"") message:[NSString stringWithFormat:NSLocalizedString(@"kRenameFolder%@", @""), NSLocalizedString(_folder.name, @"")] style:UIAlertViewStylePlainTextInput cancelButtonTitle:NSLocalizedString(@"kAlertViewAddFolderSaveButton", @"") otherButtonTitles:@[NSLocalizedString(@"kCancel", @"")] tapBlock:^(UIAlertView * __nonnull alertView, NSInteger buttonIndex) {
        if (alertView.cancelButtonIndex == buttonIndex && ![NSString isEmptyString:[alertView textFieldAtIndex:0].text]) {
            [me.folder setName:[alertView textFieldAtIndex:0].text];
            [me.folder saveDocument];
        }
    }];
    
    [[alertView textFieldAtIndex:0] setText:NSLocalizedString(_folder.name, @"")];
    [[alertView textFieldAtIndex:0] setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
}

#pragma mark - LCActionMenuDelegate

-(void)actionMenu:(LCActionMenu *)menu didPressButtonForType:(LCActionMenuButtonType)type
{
    __block typeof(self) me = self;
    
    [menu dismissViewControllerAnimated:YES completion:^{
        if (type == LCActionMenuButtonTypeDelete) {
            [UIAlertView showWithTitle:NSLocalizedString(@"kDeleteFolder", @"") message:[NSString stringWithFormat:NSLocalizedString(@"kAreYouSureToDelete%@?", @""), NSLocalizedString(_folder.name, @"")] cancelButtonTitle:NSLocalizedString(@"kDelete", @"") otherButtonTitles:@[NSLocalizedString(@"kCancel", @"")] tapBlock:^(UIAlertView * __nonnull alertView, NSInteger buttonIndex) {
                if (alertView.cancelButtonIndex == buttonIndex) {
                    [me.folder.relatedLeads enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        LCLead *newThreadedLead = [[LCLead alloc] initFromDocumentID:obj];
                        
                        if (![NSString isEmptyString:newThreadedLead.pictureFileURLs.firstObject]) {
                            [[LCAWSFailedUpload objectForName:newThreadedLead.pictureFileURLs.firstObject] remove];
                            [[LCAWSManager sharedManager] deleteFile:newThreadedLead.pictureFileURLs.firstObject forType:LCAWSManagerFileTypeJPEG inBucketPath:AWSPicturesPath withCompletionHandler:nil];
                        }
                        
                        if (![NSString isEmptyString:newThreadedLead.audioFileURLs.firstObject]) {
                            [[LCAWSFailedUpload objectForName:newThreadedLead.audioFileURLs.firstObject] remove];
                            [[LCAWSManager sharedManager] deleteFile:newThreadedLead.audioFileURLs.firstObject forType:LCAWSManagerFileTypeM4A inBucketPath:AWSVoiceNotesPath withCompletionHandler:nil];
                        }
                        
                        [newThreadedLead deleteDocument];
                    }];
                    
                    [me.folder deleteDocument];
                    [me.delegate folderCollectionViewCellHasMadeChanges:me];
                }
            }];
        }
        else if (type == LCActionMenuButtonTypeRename)
        {
            [me renameMenuItemPressed:nil];
        }
        else if (type == LCActionMenuButtonTypeChangeColor)
        {
            LCColorPickerCollectionViewController *viewController = [[UIApplication sharedApplication].keyWindow.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"LCColorPickerCollectionViewController"];
            
            LCNavigationController *navigationController = [[LCNavigationController alloc] initWithRootViewController:viewController];
            [viewController setDelegate:me];
            [navigationController setNavigationBarHidden:NO];
            
            CGSize size = CGSizeZero;
            
            if (IS_IPAD) {
                size = CGSizeMake(400, 400);
            }
            else
            {
                size = CGSizeMake(300, [UIScreen mainScreen].bounds.size.height > 500? 300 : 280);
            }
            
            MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithSize:size viewController:navigationController];
            [formSheet setCornerRadius:10];
            [formSheet setShouldCenterVertically:YES];
            [formSheet setMovementWhenKeyboardAppears:IS_IPAD? MZFormSheetWhenKeyboardAppearsDoNothing : MZFormSheetWhenKeyboardAppearsMoveToTop];
            [formSheet setTransitionStyle:MZFormSheetTransitionStyleSlideFromBottom];
            [formSheet setShouldDismissOnBackgroundViewTap:YES];
            
            [navigationController setPreferredContentSize:size];
            [viewController setPreferredContentSize:size];
            [viewController setPresentedFormSheetSize:size];
            [viewController.view setFrame:CGRectMake(0, 0, size.width, size.height)];
            
            [[UIApplication sharedApplication].keyWindow.rootViewController mz_presentFormSheetController:formSheet animated:YES completionHandler:nil];
        }
    }];
}

#pragma mark - LCColorPickerDelegate

-(void)colorPicker:(LCColorPickerCollectionViewController *)picker didPickColor:(UIColor *)color
{
    __block typeof(self) me = self;
    
    [picker mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        [me.folder setHexColor:[LCUtils hexStringFromColor:color]];
        [me.folder saveDocument];
        
        [me.folder.relatedLeads enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            LCLead *lead = [[LCLead alloc] initFromDocumentID:obj];
            if (!lead.associatedFolder) {
                [lead setAssociatedFolder:me.folder.document.documentID];
                [lead saveDocument];
            }
        }];
        
        [me.delegate folderCollectionViewCellHasMadeChanges:me];
    }];
}

@end