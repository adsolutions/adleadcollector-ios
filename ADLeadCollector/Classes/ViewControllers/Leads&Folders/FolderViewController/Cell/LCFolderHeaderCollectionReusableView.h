//
//  LCFolderHeaderCollectionReusableView.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 30/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCFolderHeaderCollectionReusableView : UICollectionReusableView

@property (nonatomic) NSUInteger totalLeads;
@property (nonatomic) NSUInteger totalCollections;
@property (nonatomic) NSUInteger totalStarredLeads;

@end