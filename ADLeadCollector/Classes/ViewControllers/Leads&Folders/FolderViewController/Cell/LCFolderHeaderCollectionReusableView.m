//
//  LCFolderHeaderCollectionReusableView.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 30/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCFolderHeaderCollectionReusableView.h"
#import "LCBadgeView.h"
#import "LCActionMenu.h"

#import <PureLayout/PureLayout.h>

@interface LCFolderHeaderCollectionReusableView ()

@property (nonatomic, strong) LCBadgeView *leadsBadgeView;
@property (nonatomic, strong) LCBadgeView *collectionsBadgeView;
@property (nonatomic, strong) LCBadgeView *starredLeadsBadgeView;

@end

@implementation LCFolderHeaderCollectionReusableView

#pragma mark - Setters

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    _collectionsBadgeView = [[LCBadgeView alloc] initForAutoLayout];
    [self addSubview:_collectionsBadgeView];
    [_collectionsBadgeView autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self withOffset:5];
    [_collectionsBadgeView autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self withOffset:5];
    [_collectionsBadgeView autoSetDimension:ALDimensionHeight toSize:30];
    [_collectionsBadgeView setBorderRadius:30/2];
    [_collectionsBadgeView setImageSize:CGSizeMake(22, 22)];
    [_collectionsBadgeView setImage:[UIImage imageNamed:@"collections"]];
    [_collectionsBadgeView setTextColor:FlatWhiteDark];
    [_collectionsBadgeView setTextInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
    [_collectionsBadgeView setFont:[LCUtils fontWithSize:16]];
    [_collectionsBadgeView setImageInsets:UIEdgeInsetsMake(-2, 0, 0, 0)];
    [_collectionsBadgeView setBackgroundColor:FlatWhite];
    
    _starredLeadsBadgeView = [[LCBadgeView alloc] initForAutoLayout];
    [self addSubview:_starredLeadsBadgeView];
    [_starredLeadsBadgeView autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self withOffset:5];
    [_starredLeadsBadgeView autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self];
    [_starredLeadsBadgeView autoCenterInSuperview];
    [_starredLeadsBadgeView autoSetDimension:ALDimensionHeight toSize:30];
    [_starredLeadsBadgeView setBorderRadius:30/2];
    [_starredLeadsBadgeView setImageSize:CGSizeMake(24, 24)];
    [_starredLeadsBadgeView setImage:[LCActionMenuImage imageWithSize:CGSizeMake(24, 23) color:FlatWhiteDark andType:LCActionMenuButtonTypeStarred]];
    [_starredLeadsBadgeView setTextColor:FlatWhiteDark];
    [_starredLeadsBadgeView setTextInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
    [_starredLeadsBadgeView setFont:[LCUtils fontWithSize:16]];
    [_starredLeadsBadgeView setBackgroundColor:FlatWhite];
    
    _leadsBadgeView = [[LCBadgeView alloc] initForAutoLayout];
    [self addSubview:_leadsBadgeView];
    [_leadsBadgeView autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self withOffset:5];
    [_leadsBadgeView autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self withOffset:-5];
    [_leadsBadgeView autoSetDimension:ALDimensionHeight toSize:30];
    [_leadsBadgeView setBorderRadius:30/2];
    [_leadsBadgeView setImageSize:CGSizeMake(24, 24)];
    [_leadsBadgeView setImage:[UIImage imageNamed:@"leads"]];
    [_leadsBadgeView setTextColor:FlatWhiteDark];
    [_leadsBadgeView setTextInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
    [_leadsBadgeView setFont:[LCUtils fontWithSize:16]];
    [_leadsBadgeView setBackgroundColor:FlatWhite];
}

-(void)setTotalLeads:(NSUInteger)totalLeads
{
    _totalLeads = totalLeads;
    [_leadsBadgeView setText:[@(totalLeads) stringValue]];
}

-(void)setTotalStarredLeads:(NSUInteger)totalStarredLeads
{
    _totalStarredLeads = totalStarredLeads;
    [_starredLeadsBadgeView setText:[@(totalStarredLeads) stringValue]];
}

-(void)setTotalCollections:(NSUInteger)totalCollections
{
    _totalCollections = totalCollections;
    [_collectionsBadgeView setText:[@(totalCollections) stringValue]];
}

@end