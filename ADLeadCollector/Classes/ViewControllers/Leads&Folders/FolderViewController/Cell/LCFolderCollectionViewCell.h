//
//  LCFolderCollectionViewCell.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 29/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LCFolder.h"

@class LCFolderCollectionViewCell;

@protocol LCFolderCollectionViewCellDelegate <NSObject>

-(void)folderCollectionViewCellHasMadeChanges:(LCFolderCollectionViewCell *)cell;

@end

@interface LCFolderCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) LCFolder *folder;
@property (nonatomic, unsafe_unretained) id <LCFolderCollectionViewCellDelegate> delegate;
@property (nonatomic) CGFloat percentage;

@end