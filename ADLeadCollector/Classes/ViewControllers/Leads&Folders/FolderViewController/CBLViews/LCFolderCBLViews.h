//
//  LCFolderCBLViews.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 13/10/15.
//  Copyright © 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LCFolderCBLViews : NSObject

+(CBLQuery *)queryForFoldersView;

@end