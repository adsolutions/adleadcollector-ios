//
//  LCFolderCBLViews.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 13/10/15.
//  Copyright © 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCFolderCBLViews.h"
#import "LCLead.h"

#import <NSString+Validation/NSString+Validation.h>

NSString *const LCViewFolders = @"LCViewFolders";

@implementation LCFolderCBLViews

+(CBLQuery *)queryForFoldersView
{
    CBLView *view = [[MBEngine database] viewNamed:LCViewFolders];
    [view setMapBlock:MAPBLOCK({
        if ([doc[@"type"] isEqualToString:NSStringFromClass([LCLead class])]) {
            NSString *folderID = doc[@"associatedFolder"];
            if (![NSString isEmptyString:folderID]) {
                emit(doc[@"_id"], folderID);
            }
        }
    }) reduceBlock:REDUCEBLOCK({
        NSMutableDictionary *reducedObjects = @{}.mutableCopy;
        for (NSString *folderID in values) {
            NSNumber *number = [reducedObjects objectForKey:folderID];
            NSInteger value = number? ([number integerValue]+1) : 1;
            [reducedObjects setObject:@(value) forKey:folderID];
        }
        
        return reducedObjects;
    }) version:@"1"];
    
    if (view.stale) {
        [view deleteIndex];
    }
    
    return [view createQuery];
}

@end