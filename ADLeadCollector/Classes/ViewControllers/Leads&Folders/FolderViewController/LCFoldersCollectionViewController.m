//
//  LCFoldersCollectionViewController.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 29/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCFoldersCollectionViewController.h"
#import "LCFolder.h"
#import "LCFolderCollectionViewCell.h"
#import "LCRootViewController.h"
#import "LCLeadsTableViewController.h"
#import "LCFolderHeaderCollectionReusableView.h"
#import "LCLead.h"
#import "LCForm.h"
#import "LCActionMenu.h"

#import <ReactiveCocoa/ReactiveCocoa.h>
#import <UIAlertView+Blocks/UIAlertView+Blocks.h>

@interface LCFoldersCollectionViewController () <UIGestureRecognizerDelegate, LCFolderCollectionViewCellDelegate>

@property (nonatomic, strong) NSArray *foldersArray;
@property (nonatomic) NSUInteger totalObjectsCount;
@property (nonatomic) NSUInteger totalStarredLeadsCount;

@property (nonatomic, weak) IBOutlet UILongPressGestureRecognizer *longPressureGestureRecognizer;

@end

@implementation LCFoldersCollectionViewController

static NSString * const reuseIdentifier = @"LCFolderCollectionViewCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:[LCUtils livelyButtonWithFrame:CGRectMake(0, 0, 30, 30) forType:kFRDLivelyButtonStyleHamburger target:self andAction:@selector(showMenuButtonPressed:)]]];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:[LCUtils livelyButtonWithFrame:CGRectMake(0, 0, 25, 25) forType:kFRDLivelyButtonStylePlus target:self andAction:@selector(addFolderButtonPressed:)]]];
    
    [self setTitle:NSLocalizedString(@"kFolderViewControllerTitle", @"FolderViewController")];
}

-(void)reloadData
{
    _foldersArray = [LCFolder findAllMatchingPredicate:nil andSortedBy:[NSSortDescriptor sortDescriptorWithKey:@"value.name" ascending:YES]];
    _totalObjectsCount = [LCLead countAll];
    _totalStarredLeadsCount = [LCLead countAllMatchingPredicate:[NSPredicate predicateWithFormat:@"value.starred == 1"]];
    
    [super reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _foldersArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LCFolderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    [cell setFolder:_foldersArray[indexPath.row]];
    [cell setPercentage:(CGFloat)[_foldersArray[indexPath.row] leadsCount]/_totalObjectsCount];
    [cell setDelegate:self];
    return cell;
}

#pragma mark - UICollectionViewDelegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    LCLeadsTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LCLeadsTableViewController"];
    [viewController setFolder:((LCFolderCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath]).folder];
    [self.navigationController pushViewController:viewController animated:YES];
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == UICollectionElementKindSectionHeader) {
        LCFolderHeaderCollectionReusableView *foldersHeader = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"LCFolderHeaderCollectionReusableView" forIndexPath:indexPath];
        [foldersHeader setFrame:CGRectMake(0, 0, self.view.frame.size.width, 36)];
        [foldersHeader setTotalLeads:_totalObjectsCount];
        [foldersHeader setTotalCollections:_foldersArray.count];
        [foldersHeader setTotalStarredLeads:_totalStarredLeadsCount];
        
        return foldersHeader;
    }
    
    return nil;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    NSUInteger insets = ((int)(self.view.frame.size.width-30) / 150) < 2? 0 : 15;
    return UIEdgeInsetsMake(0, insets, 0, insets);
}

#pragma mark - Login

-(void)loginSuccessed:(BOOL)flag
{
    [self reloadData];
}

#pragma mark - Actions

-(void)showMenuButtonPressed:(id)sender
{
    [(LCRootViewController *)self.sideMenuViewController presentLeftMenuViewController];
}

-(void)addFolderButtonPressed:(id)sender
{
    __block typeof(self) me = self;
    
    [UIAlertView showWithTitle:NSLocalizedString(@"kAlertViewAddFolderTitle", @"") message:NSLocalizedString(@"kAlertViewAddFolderMessage", @"") style:UIAlertViewStylePlainTextInput cancelButtonTitle:NSLocalizedString(@"kAlertViewAddFolderOKButton", @"") otherButtonTitles:@[NSLocalizedString(@"kCancel", @"")] tapBlock:^(UIAlertView * __nonnull alertView, NSInteger buttonIndex) {
        if (alertView.cancelButtonIndex == buttonIndex) {
            NSString *collectionName = [alertView textFieldAtIndex:0].text;
            
            if (collectionName.length > 0) {
                LCFolder *newFolder = [LCFolder createNew];
                [newFolder setName:collectionName];
                [newFolder saveDocument];
                
                [me reloadData];
            }
        }
    }];
}

-(IBAction)handleLongPress:(UILongPressGestureRecognizer *)sender
{
    if (sender.state != UIGestureRecognizerStateRecognized) {
        return;
    }
    
    CGPoint point = [sender locationInView:self.collectionView];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:point];
    if (indexPath){
        // get the cell at indexPath (the one you long pressed)
        LCFolderCollectionViewCell *cell = (LCFolderCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
        [self performSegueWithIdentifier:@"blurOptionsView" sender:cell];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    LCActionMenu *destination = segue.destinationViewController;
    destination.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [destination setButtons:@[@(LCActionMenuButtonTypeRename), @(LCActionMenuButtonTypeChangeColor), @(LCActionMenuButtonTypeDelete)]];
    [destination setDelegate:sender];
}

#pragma mark - LCFolderCollectionViewCellDelegate

-(void)folderCollectionViewCellHasMadeChanges:(LCFolderCollectionViewCell *)cell
{
    [self mobileBridgeDidUpdateDataWithNotification:nil];
}

-(void)mobileBridgeDidUpdateDataWithNotification:(NSNotification *)notification
{
    _foldersArray = nil;
    
    [super mobileBridgeDidUpdateDataWithNotification:notification];
}

@end