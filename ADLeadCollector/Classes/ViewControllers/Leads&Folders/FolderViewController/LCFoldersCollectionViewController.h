//
//  LCFoldersCollectionViewController.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 29/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LCCollectionViewController.h"

@interface LCFoldersCollectionViewController : LCCollectionViewController

@end