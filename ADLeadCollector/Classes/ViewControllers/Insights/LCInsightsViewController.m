//
//  LCAnalyticsViewController.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 06/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCInsightsViewController.h"
#import "LCRootViewController.h"
#import "LCLead.h"
#import "LCInsightsTableViewCell.h"
#import "NSDate+Extras.h"
#import "LCInsightsInviteTeamMateTableViewCell.h"
#import "LCAlwaysTemplateImage.h"
#import "LCInsightsViews.h"
#import "LCBadgeView.h"
#import "LCActionMenu.h"

#import <GraphKit/GraphKit.h>
#import <CouchbaseLite/CouchbaseLite.h>
#import <UIColor-HexString/UIColor+HexString.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import <DZNSegmentedControl/DZNSegmentedControl.h>
#import <PPiFlatSegmentedControl/PPiFlatSegmentedControl.h>
#import <FontAwesome+iOS/UIFont+FontAwesome.h>
#import <JBChartView/JBLineChartView.h>

#define kFooterHeight 25

@interface LCInsightsViewController () <GKBarGraphDataSource, UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource, DZNSegmentedControlDelegate, UIScrollViewDelegate, JBLineChartViewDataSource, JBLineChartViewDelegate>

// UI
@property (nonatomic, strong) GKBarGraph *graphView;
@property (nonatomic, strong) DZNSegmentedControl *segmentedControl;
@property (nonatomic, strong) PPiFlatSegmentedControl *chartSwitcherControl;
@property (nonatomic, strong) JBLineChartView *lineChart;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UILabel *commentLabel;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic, strong) UIView *lineView;

/// main datasource
@property (nonatomic, strong) NSDictionary *dataSource;

// PersonalViewsDatasources
@property (nonatomic, strong) NSDictionary *usersEverInsights;
@property (nonatomic, strong) NSDictionary *users30DaysInsights;
@property (nonatomic, strong) NSDictionary *users7DaysInsights;
@property (nonatomic, strong) NSDictionary *users120DaysInsights;
@property (nonatomic, strong) NSMutableDictionary *orderedValuesForUsers;

// GlobalViewsDatasources
@property (nonatomic, strong) NSDictionary *globalEverInsights;
@property (nonatomic, strong) NSDictionary *global30DaysInsights;
@property (nonatomic, strong) NSDictionary *global7DaysInsights;
@property (nonatomic, strong) NSDictionary *global120DaysInsights;

@property (nonatomic, strong) NSArray *index;
@property (nonatomic, strong) NSDictionary *initials;
@property (nonatomic, strong) NSDictionary *userNames;

@property (nonatomic, strong) NSMutableDictionary *colorsDataSource;
@property (nonatomic, readonly) NSArray *excludedColors;

@property (nonatomic) NSInteger maxValueFor30Days;

@end

@implementation LCInsightsViewController

- (void)viewDidLoad {
    self.title = NSLocalizedString(@"kAnalytics", @"");
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:[LCUtils livelyButtonWithFrame:CGRectMake(0, 0, 30, 30) forType:kFRDLivelyButtonStyleHamburger target:self andAction:@selector(showMenuButtonPressed:)]]];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    _activityIndicatorView = [[UIActivityIndicatorView alloc] initForAutoLayout];
    [_activityIndicatorView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.view addSubview:_activityIndicatorView];
    [_activityIndicatorView setColor:companyColor()];
    [_activityIndicatorView autoCenterInSuperview];
    [_activityIndicatorView setHidesWhenStopped:YES];
    
    [self loadViews];
    [self updateViewsVisibility:NO];
    
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self performSelectorOnMainThread:@selector(segmentedControlPressed:) withObject:self.segmentedControl waitUntilDone:YES];
    [[NSOperationQueue mainQueue] addOperation:[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(mobileBridgeDidUpdateDataWithNotification:) object:(id)@""]];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

-(void)loadViews
{
    _segmentedControl = ({
        DZNSegmentedControl *segmentedControl = [[DZNSegmentedControl alloc] initForAutoLayout];
        [self.view addSubview:segmentedControl];
        [segmentedControl autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero excludingEdge:ALEdgeBottom];
        [segmentedControl setHairlineColor:FlatWhiteDark];
        [segmentedControl setItems:@[NSLocalizedString(@"kEver", @""), NSLocalizedString(@"k7Days", @""), NSLocalizedString(@"k30Days", @""), NSLocalizedString(@"kQuarter", @"")]];
        [segmentedControl addTarget:self action:@selector(segmentedControlPressed:) forControlEvents:UIControlEventValueChanged];
        segmentedControl;
    });
    
    _chartSwitcherControl = ({
        NSArray *items = @[[[PPiFlatSegmentItem alloc] initWithTitle:@"" andIcon:@"icon-bar-chart"],
                           [[PPiFlatSegmentItem alloc] initWithTitle:@"" andIcon:@"icon-line-chart"]];
        PPiFlatSegmentedControl *segmentedControl = [[PPiFlatSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, 90, 34) items:items iconPosition:IconPositionLeft andSelectionBlock:^(NSUInteger segmentIndex) {
            CGRect frame = self.scrollView.frame;
            frame.origin.x = self.scrollView.frame.size.width * segmentIndex;
            frame.origin.y = 0;
            frame.size = self.scrollView.frame.size;
            [self.scrollView scrollRectToVisible:frame animated:YES];
        } iconSeparation:0];
        segmentedControl.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addSubview:segmentedControl];
        [segmentedControl autoSetDimension:ALDimensionWidth toSize:90];
        [segmentedControl autoSetDimension:ALDimensionHeight toSize:34];
        [segmentedControl autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.view withOffset:-5];
        [segmentedControl autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_segmentedControl withOffset:5];
        segmentedControl.color = ClearColor;
        segmentedControl.borderWidth = 0.5;
        segmentedControl.borderColor = companyColor();
        segmentedControl.selectedColor = companyColor();
        segmentedControl.textAttributes = @{NSFontAttributeName:[UIFont fontAwesomeFontOfSize:22], NSForegroundColorAttributeName:companyColor()};
        segmentedControl.selectedTextAttributes = @{NSFontAttributeName:[UIFont fontAwesomeFontOfSize:22],NSForegroundColorAttributeName:[UIColor whiteColor]};
        segmentedControl;
    });
    
//    _commentLabel = ({
//        UILabel *label = [[UILabel alloc] initForAutoLayout];
//        [self.view addSubview:label];
//        [label setTextColor:FlatGrayDark];
//        [label setFont:[LCUtils fontWithSize:16]];
//        [label setTextAlignment:NSTextAlignmentLeft];
//        [label setNumberOfLines:0];
//        [label autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:15];
//        [label autoPinEdge:ALEdgeRight toEdge:ALEdgeLeft ofView:_chartSwitcherControl];
//        [label autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_segmentedControl withOffset:15];
//        //        [label setText:NSLocalizedString(@"kYourTeamPerformance", @"")];
//        [label setBackgroundColor:ClearColor];
//        label;
//    });
    
    _scrollView = ({
        UIScrollView *scrollView = [[UIScrollView alloc] initForAutoLayout];
        [self.view addSubview:scrollView];
        [scrollView autoSetDimension:ALDimensionHeight toSize:180];
        [scrollView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_chartSwitcherControl withOffset:0];
        [scrollView autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view withOffset:0];
        [scrollView autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.view withOffset:0];
        [scrollView setScrollsToTop:NO];
        [scrollView setPagingEnabled:YES];
        [scrollView setShowsHorizontalScrollIndicator:NO];
        [scrollView setDelegate:self];
        
        _contentView = [[UIView alloc] initForAutoLayout];
        [scrollView addSubview:_contentView];
        [_contentView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
        [_contentView autoSetDimension:ALDimensionWidth toSize:self.view.frame.size.width*2];
        [scrollView setContentSize:CGSizeMake(self.view.frame.size.width*2, 180)];
        scrollView;
    });
    
    _graphView = ({
        GKBarGraph *barGraph = [[GKBarGraph alloc] initForAutoLayout];
        [_contentView addSubview:barGraph];
        [barGraph autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:_contentView withOffset:0];
        [barGraph autoSetDimension:ALDimensionWidth toSize:self.view.frame.size.width];
        [barGraph autoSetDimension:ALDimensionHeight toSize:180];
        [barGraph autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:_contentView];
        [barGraph setDataSource:self];
        [barGraph setBackgroundColor:ClearColor];
        barGraph;
    });
    
    _lineChart = ({
        JBLineChartView *lineChart = [[JBLineChartView alloc] initForAutoLayout];
        [_contentView addSubview:lineChart];
        [lineChart autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:_contentView withOffset:0];
        [lineChart autoSetDimension:ALDimensionWidth toSize:self.view.frame.size.width];
        [lineChart autoSetDimension:ALDimensionHeight toSize:180];
        [lineChart autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:_graphView];
        [lineChart autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:_contentView];
        [lineChart setDataSource:self];
        [lineChart setDelegate:self];
        [lineChart setShowsLineSelection:NO];
        [lineChart setShowsVerticalSelection:NO];
        lineChart;
    });
    
    _lineView = ({
        UIView *view = [[UIView alloc] initForAutoLayout];
        [self.view addSubview:view];
        [view autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_scrollView withOffset:20];
        [view autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view withOffset:0];
        [view autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.view withOffset:0];
        [view autoSetDimension:ALDimensionHeight toSize:1];
        [view setBackgroundColor:FlatWhiteDark];
        view;
    });
    
    _pageControl = ({
        UIPageControl *pageControl = [[UIPageControl alloc] initForAutoLayout];
        [self.view addSubview:pageControl];
        [pageControl setPageIndicatorTintColor:FlatWhiteDark];
        [pageControl setCurrentPageIndicatorTintColor:companyColor()];
        [pageControl autoSetDimension:ALDimensionHeight toSize:10];
        [pageControl autoPinEdge:ALEdgeBottom toEdge:ALEdgeTop ofView:_lineView withOffset:-5];
        [pageControl autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view withOffset:0];
        [pageControl autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.view withOffset:0];
        [pageControl setNumberOfPages:2];
        [pageControl addTarget:self action:@selector(pageControlDidScroll:) forControlEvents:UIControlEventValueChanged];
        pageControl;
    });
    
    self.tableView = ({
        UITableView *tableView = [[UITableView alloc] initForAutoLayout];
        [self.view addSubview:tableView];
        [tableView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero excludingEdge:ALEdgeTop];
        [tableView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_lineView withOffset:0];
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.opaque = NO;
        tableView.backgroundColor = FlatWhite;
        tableView.backgroundView = nil;
        tableView.separatorColor = ClearColor;
        tableView.bounces = YES;
        tableView.scrollsToTop = YES;
        [tableView registerNib:[UINib nibWithNibName:@"LCInsightsTableViewCell" bundle:nil] forCellReuseIdentifier:@"LCInsightsTableViewCell"];
        [tableView registerNib:[UINib nibWithNibName:@"LCInsightsInviteTeamMateTableViewCell" bundle:nil] forCellReuseIdentifier:@"LCInsightsInviteTeamMateTableViewCell"];
        [tableView setEmptyDataSetDelegate:self];
        [tableView setEmptyDataSetSource:self];
        tableView;
    });
}

-(void)updateViewsVisibility:(BOOL)visible
{
    [_tableView setHidden:!visible];
    [_pageControl setHidden:!visible];
    [_lineView setHidden:!visible];
    [_lineChart setHidden:!visible];
    [_graphView setHidden:!visible];
    [_commentLabel setHidden:!visible];
//    [_segmentedControl setHidden:!visible];
    
    if (_segmentedControl.selectedSegmentIndex != 0) {
        [_chartSwitcherControl setHidden:!visible];
    }
    else
    {
        [_chartSwitcherControl setHidden:YES];
    }
    
    if (!visible) {
        [_activityIndicatorView startAnimating];
    }
    else
    {
        [_activityIndicatorView stopAnimating];
    }
}

#pragma mark - Async

-(void)prepareViews
{
    NSMutableDictionary *counts = @{}.mutableCopy;
    
    CBLQuery *insightsQuery = [LCInsightsViews queryForGlobalInsightsEverView];
    for (CBLQueryRow *row in [insightsQuery run:nil]) {
        _globalEverInsights = row.value;
    }
    
    // se non ho nessun dato allora nascondo i grafici
    if (_globalEverInsights.count == 0) {
        [_activityIndicatorView stopAnimating];
        [_commentLabel setHidden:YES];
        [_chartSwitcherControl setHidden:YES];
        [_tableView setHidden:NO];
        [_tableView autoCenterInSuperviewMargins];
        [_segmentedControl setHidden:YES];
        return;
    }
    
    [counts setObject:[_globalEverInsights.allValues valueForKeyPath:@"@sum.self"] forKey:@"0"];
    [self performSelectorOnMainThread:@selector(setIndex:) withObject:_globalEverInsights.allKeys waitUntilDone:YES];
    
    _maxValueFor30Days = 0;
    NSInteger count = 0;
    NSInteger sum = 0;
    
    NSMutableDictionary *globalDataSource = @{}.mutableCopy;
    
    // 30Days Insights
    CBLQuery *lastMonthQuery = [LCInsightsViews queryForLastMonthView];
    for (CBLQueryRow *row in [lastMonthQuery run:nil]) {
        for (NSString *author in row.value) {
            sum = 0;
            for (NSNumber *value in [row.value[author] allValues]) {
                if (_maxValueFor30Days < [value integerValue]) {
                    _maxValueFor30Days = [value integerValue];
                }
                
                sum+= [value integerValue];
                count+= [value integerValue];
            }
            
            [globalDataSource setObject:@(sum) forKey:author];
        }
        
        _users30DaysInsights = row.value;
        _global30DaysInsights = [globalDataSource copy];
        [globalDataSource removeAllObjects];
    }
    
    [counts setObject:@(count) forKey:@"2"];
    
    // 7Days Insights
    CBLQuery *lastWeekQuery = [LCInsightsViews queryForLastWeekView];
    count = 0;
    for (CBLQueryRow *row in [lastWeekQuery run:nil]) {
        for (NSString *author in row.value) {
            sum = 0;
            for (NSNumber *value in [row.value[author] allValues]) {
                count+= [value integerValue];
                sum+= [value integerValue];
            }
            
            [globalDataSource setObject:@(sum) forKey:author];
        }
        
        _users7DaysInsights = row.value;
        _global7DaysInsights = [globalDataSource copy];
        [globalDataSource removeAllObjects];
    }
    
    [counts setObject:@(count) forKey:@"1"];
    
    // 120Days Insights
    CBLQuery *lastQuarterQuery = [LCInsightsViews queryForLastQuarterView];
    count = 0;
    for (CBLQueryRow *row in [lastQuarterQuery run:nil]) {
        for (NSString *author in row.value) {
            sum = 0;
            for (NSNumber *value in [row.value[author] allValues]) {
                count+= [value integerValue];
                sum+= [value integerValue];
            }
            
            [globalDataSource setObject:@(sum) forKey:author];
        }
        
        _users120DaysInsights = row.value;
        _global120DaysInsights = [globalDataSource copy];
        [globalDataSource removeAllObjects];
    }
    
    [counts setObject:@(count) forKey:@"3"];
    
    [self performSelectorOnMainThread:@selector(updateCounts:) withObject:counts waitUntilDone:NO];
}

-(void)updateCounts:(NSDictionary *)counts
{
    [self.segmentedControl setCount:counts[@"3"] forSegmentAtIndex:3];
    [self.segmentedControl setCount:counts[@"2"] forSegmentAtIndex:2];
    [self.segmentedControl setCount:counts[@"1"] forSegmentAtIndex:1];
    [self.segmentedControl setCount:counts[@"0"] forSegmentAtIndex:0];
    
    [self mobileBridgeDidUpdateDataWithNotification:nil];
    
    [self segmentedControlPressed:self.segmentedControl];
}

#pragma mark - Setters

-(void)setIndex:(NSArray *)index
{
    for (NSString *emailAddress in index) {
        NSMutableDictionary *cachedAddresses = self.initials.mutableCopy;
        if (!cachedAddresses) {
            cachedAddresses = @{}.mutableCopy;
        }
        
        NSMutableDictionary *cachedUsernames = self.userNames.mutableCopy;
        if (!cachedUsernames) {
            cachedUsernames = @{}.mutableCopy;
        }
        
        if ([cachedAddresses[emailAddress] length] == 0) {
            [[BTEngine sharedEngine] getRestCallbackOnMethod:[NSString stringWithFormat:@"/api/v1/user/%@", emailAddress] withParameters:@{@"parameters" : @"name,surname"} andCompletionHandler:^(NSError *error, NSDictionary *response) {
                NSMutableDictionary *cachedAddressesInternal = self.initials.mutableCopy;
                if (!cachedAddressesInternal) {
                    cachedAddressesInternal = @{}.mutableCopy;
                }
                
                NSMutableDictionary *cachedUsersNameInternal = self.userNames.mutableCopy;
                if (!cachedUsersNameInternal) {
                    cachedUsersNameInternal = @{}.mutableCopy;
                }
                
                if (!error) {
                    NSString *name = response[@"data"][@"name"];
                    NSString *surname = response[@"data"][@"surname"];
                    
                    NSString *initials = nil;
                    
                    if (name.length > 0 && surname.length > 0) {
                        initials = [NSString stringWithFormat:@"%@%@", [[name substringToIndex:1] uppercaseString], [[surname                                                                                                substringToIndex:1] uppercaseString]];
                        
                        [cachedUsersNameInternal setObject:[NSString stringWithFormat:@"%@ %@", name, surname] forKey:emailAddress];
                    }
                    else if (name.length > 0 || surname.length > 0)
                    {
                        if (name.length > 1) {
                            initials = [name substringToIndex:2];
                            [cachedUsernames setObject:name forKey:emailAddress];
                        }
                        else if (surname.length > 1) {
                            initials = [surname substringToIndex:2];
                            [cachedUsersNameInternal setObject:surname forKey:emailAddress];
                        }
                        else
                        {
                            initials = [[name substringToIndex:1] uppercaseString];
                        }
                    }
                    
                    if (!initials) {
                        initials = [[emailAddress substringToIndex:2] lowercaseString];
                    }
                    
                    [cachedAddressesInternal setObject:initials forKey:emailAddress];
                    self.userNames = cachedUsersNameInternal;
                    self.initials = cachedAddressesInternal;
                }
                else
                {
                    self.userNames = cachedUsersNameInternal;
                    self.initials = cachedAddressesInternal;
                }
            }];
        }
        else
        {
            self.userNames = cachedUsernames;
            self.initials = cachedAddresses;
        }
    }
    
    _index = index;
    
    [self mobileBridgeDidUpdateDataWithNotification:nil];
}

-(void)setInitials:(NSDictionary *)initials
{
    [self mobileBridgeDidUpdateDataWithNotification:nil];
    
    [kUserDefaults setObject:initials forKey:kCachedAddressesKey];
}

-(void)setUserNames:(NSDictionary *)userNames
{
    [kUserDefaults setObject:userNames forKey:kCachedUsernamesKey];
}

-(void)setDataSource:(NSDictionary *)dataSource
{
    _dataSource = dataSource;
    
    [self mobileBridgeDidUpdateDataWithNotification:nil];
}

#pragma mark - Getters

-(NSArray *)excludedColors
{
    return @[companyColor(), FlatWhite];
}

-(NSDictionary *)initials
{
    return [kUserDefaults objectForKey:kCachedAddressesKey];
}

-(NSDictionary *)userNames
{
    NSDictionary *sortedDictionary = [kUserDefaults objectForKey:kCachedUsernamesKey];
    NSArray *sortedKeys = [sortedDictionary.allKeys sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES]]];
    // As we using same keys from dictionary, we can put any non-nil value for the notFoundMarker, because it will never used
    NSArray *sortedValues = [sortedDictionary objectsForKeys:sortedKeys notFoundMarker:@""];
    
    return [NSDictionary dictionaryWithObjects:sortedValues forKeys:sortedKeys];
}

#pragma mark - Actions

-(void)pageControlDidScroll:(UIPageControl *)sender
{
    CGRect frame = self.scrollView.frame;
    frame.origin.x = self.scrollView.frame.size.width * self.pageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.scrollView.frame.size;
    [self.scrollView scrollRectToVisible:frame animated:YES];
}

-(void)segmentedControlPressed:(DZNSegmentedControl *)sender
{
    switch (sender.selectedSegmentIndex) {
        case 0:
            self.dataSource = _globalEverInsights;
            [self loadPersonalInsightsFromDataSouce:_usersEverInsights];
            CGRect frame = self.scrollView.frame;
            frame.origin.x = 0;
            frame.origin.y = 0;
            frame.size = self.scrollView.frame.size;
            [self.scrollView scrollRectToVisible:frame animated:YES];
            break;
        case 1:
            self.dataSource = _global7DaysInsights;
            [self loadPersonalInsightsFromDataSouce:_users7DaysInsights];
            break;
        case 2:
            self.dataSource = _global30DaysInsights;
            [self loadPersonalInsightsFromDataSouce:_users30DaysInsights];
            break;
        case 3:
            self.dataSource = _global120DaysInsights;
            [self loadPersonalInsightsFromDataSouce:_users120DaysInsights];
            break;
        default:
            break;
    }
    
    [_chartSwitcherControl setHidden:sender.selectedSegmentIndex == 0];
    [_pageControl setNumberOfPages:sender.selectedSegmentIndex == 0? 1 : 2];
    [_scrollView setScrollEnabled:sender.selectedSegmentIndex != 0];
}

-(void)showMenuButtonPressed:(id)sender
{
    [(LCRootViewController *)self.sideMenuViewController presentLeftMenuViewController];
}

#pragma mark - GKBarGraphDataSource

-(NSInteger)numberOfBars
{
    return self.dataSource.allKeys.count;
}

-(NSNumber *)valueForBarAtIndex:(NSInteger)index
{
    NSInteger totalLead = [[self.dataSource.allValues valueForKeyPath:@"@sum.self"] integerValue];
    return @(([self.dataSource[self.dataSource.allKeys[index]] floatValue]/totalLead)*100);
}

-(UIColor *)colorForBarAtIndex:(NSInteger)index
{
    if (!_colorsDataSource) {
        _colorsDataSource = @{}.mutableCopy;
    }
    
    NSString *key = self.dataSource.allKeys[index];
    
    UIColor *color = _colorsDataSource[key];
    if (color) {
        return color;
    }
    
    if ([key isEqualToString:[BTWallet myWallet].emailAddress]) {
        color = companyColor();
        [_colorsDataSource setObject:color forKeyedSubscript:key];
        return color;
    }
    
    do {
        color = RandomFlatColor;
    } while ([self.excludedColors containsObject:color]);
    
    [_colorsDataSource setObject:color forKeyedSubscript:key];
    
    return color;
}

-(UIColor *)colorForBarBackgroundAtIndex:(NSInteger)index
{
    return FlatWhite;
}

- (NSString *)titleForBarAtIndex:(NSInteger)index
{
    NSString *key = self.dataSource.allKeys[index];
    
    if ([key isEqualToString:[BTWallet myWallet].emailAddress]) {
        return NSLocalizedString(@"kYou", @"");
    }
    
    if ([key isEqualToString:kUnassigned]) {
        return NSLocalizedString(key, @"");
    }
    
    NSDictionary *initials = self.initials;
    if ([initials[key] length] > 0) {
        return initials[key];
    }
    
    return @"--";
}

-(void)mobileBridgeDidUpdateDataWithNotification:(NSNotification *)notification
{
    [kUserDefaults performSelectorOnMainThread:@selector(synchronize) withObject:nil waitUntilDone:NO];
    
    if (notification) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            // switch to a background thread and perform your expensive operation
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // switch back to the main thread to update your UI
                [self prepareViews];
            });
        });
    }
    
    [self.graphView performSelectorOnMainThread:@selector(draw) withObject:nil waitUntilDone:NO];
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    
    [super mobileBridgeDidUpdateDataWithNotification:notification];
}

-(void)loadPersonalInsightsFromDataSouce:(NSDictionary *)dataSource
{
    if (!dataSource || dataSource.count == 0) {
        return;
    }
    
    self.orderedValuesForUsers = nil;
    self.orderedValuesForUsers = @{}.mutableCopy;
    
    for (NSString *author in self.dataSource.allKeys) {
        NSArray *index = [[dataSource[author] allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        [self.orderedValuesForUsers setObject:[dataSource[author] objectsForKeys:index notFoundMarker:@(NAN)] forKey:author];
    }
    
    [self.lineChart setMaximumValue:_maxValueFor30Days];
    
    [[NSOperationQueue mainQueue] addOperation:[[NSInvocationOperation alloc] initWithTarget:self.lineChart selector:@selector(reloadData) object:nil]];
    [[NSOperationQueue mainQueue] addOperation:[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(mobileBridgeDidUpdateDataWithNotification:) object:nil]];
}

#pragma mark - UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1) {
        return 1;
    }
    
    return _dataSource.allKeys.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString *CellIdentifier = @"LCInsightsTableViewCell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        LCInsightsUserDetail *userDetail = [LCInsightsUserDetail new];
        [userDetail setEmail:self.dataSource.allKeys[indexPath.row]];
        [userDetail setCount:[self.dataSource[self.dataSource.allKeys[indexPath.row]] integerValue]];
        [userDetail setColor:_colorsDataSource[self.dataSource.allKeys[indexPath.row]]];
        [userDetail setUserInsights:_users30DaysInsights[self.dataSource.allKeys[indexPath.row]]];
        [userDetail setMaxValue:_maxValueFor30Days];
        [userDetail setUserName:self.userNames[self.dataSource.allKeys[indexPath.row]]];
        [(LCInsightsTableViewCell *)cell setUserDetail:userDetail];
        
        return cell;
    }
    else
    {
        static NSString *CellIdentifier2 = @"LCInsightsInviteTeamMateTableViewCell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        return 44;
    }
    
    return 75;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0 && self.dataSource.allKeys.count != 0) {
        return kFooterHeight;
    }
    else
    {
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0 && self.dataSource.allKeys.count != 0) {
        return kFooterHeight;
    }
    else
    {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0 && self.dataSource.allKeys.count != 0) {
        return ({
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kFooterHeight)];
            [view setBackgroundColor:FlatWhite];
            
            UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 3, (self.view.frame.size.width/100)*75, kFooterHeight)];
            [view addSubview:descriptionLabel];
            [descriptionLabel setFont:[LCUtils fontWithSize:16]];
            [descriptionLabel setTextColor:FlatGrayDark];
            [descriptionLabel setTextAlignment:NSTextAlignmentLeft];
            [descriptionLabel setText:NSLocalizedString(@"kTeammatesActivity", @"")];
            
            LCBadgeView *teamCountBadgeView = [[LCBadgeView alloc] initForAutoLayout];
            [view addSubview:teamCountBadgeView];
            [teamCountBadgeView autoSetDimension:ALDimensionHeight toSize:kFooterHeight-2];
            [teamCountBadgeView autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:view withOffset:4];
            [teamCountBadgeView autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:view withOffset:-2];
            [teamCountBadgeView autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:view withOffset:-2];
            [teamCountBadgeView setBorderRadius:(kFooterHeight-5)/2];
            [teamCountBadgeView setImageSize:CGSizeMake(20, 18)];
            [teamCountBadgeView setImage:[LCActionMenuImage imageWithSize:CGSizeMake(20, 16) color:FlatWhite andType:LCActionMenuButtonTypeTeam]];
            [teamCountBadgeView setTextInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
            [teamCountBadgeView setTextColor:FlatWhite];
            [teamCountBadgeView setText:[@(self.dataSource.allKeys.count) stringValue]];
            [teamCountBadgeView setFont:[LCUtils fontWithSize:16]];
            [teamCountBadgeView setBackgroundColor:FlatWhiteDark];
            
            view;
        });
    }
    else
    {
        return nil;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 0 && self.dataSource.allKeys.count != 0) {
        return ({
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kFooterHeight)];
            [view setBackgroundColor:ClearColor];
            
            LCBadgeView *leadsBadgeView = [[LCBadgeView alloc] initForAutoLayout];
            [view addSubview:leadsBadgeView];
            [leadsBadgeView autoSetDimension:ALDimensionHeight toSize:kFooterHeight-2];
            [leadsBadgeView autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:view withOffset:4];
            [leadsBadgeView autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:view withOffset:-2];
            [leadsBadgeView autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:view withOffset:-2];
            [leadsBadgeView setBorderRadius:(kFooterHeight-5)/2];
            [leadsBadgeView setImageSize:CGSizeMake(24, 24)];
            [leadsBadgeView setImage:[UIImage imageNamed:@"leads"]];
            [leadsBadgeView setTextInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
            [leadsBadgeView setTextColor:FlatWhite];
            [leadsBadgeView setText:[[_dataSource.allValues valueForKeyPath:@"@sum.self"] stringValue]];
            [leadsBadgeView setFont:[LCUtils fontWithSize:16]];
            [leadsBadgeView setBackgroundColor:FlatWhiteDark];
            
            view;
        });
    }
    else
    {
        return nil;
    }
}

#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = NSLocalizedString(@"kNoTeamPerformanceAvailable", @"TableView");
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [LCUtils boldFontWithSize:17.0],
                                 NSForegroundColorAttributeName: FlatGray,
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [LCUtils fontWithSize:15.0],
                                 NSForegroundColorAttributeName: FlatGray,
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state
{
    return nil;
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    [imageView setImage:[UIImage imageNamed:@"LeadsCollectorWhite"]];
    return imageView.image;
}

- (UIColor *)imageTintColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return FlatGray;
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}

#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}

- (void)emptyDataSetWillAppear:(UIScrollView *)scrollView
{
//    [_commentLabel setText:NSLocalizedString(@"kNoTeamPerformanceAvailable", @"")];
//    [_commentLabel setTextAlignment:NSTextAlignmentCenter];
    
    [_pageControl setHidden:YES];
    [_lineView setHidden:YES];
    [_lineChart setHidden:YES];
    [_graphView setHidden:YES];
    [_chartSwitcherControl setHidden:YES];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(void)emptyDataSetWillDisappear:(UIScrollView *)scrollView
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    [self updateViewsVisibility:YES];
}

#pragma mark - UIScrollViewDelegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x -pageWidth /2) /pageWidth) +1;
    self.pageControl.currentPage = page;
    
    [self.chartSwitcherControl setSelected:YES segmentAtIndex:page];
}

#pragma mark - JBLineChartViewDataSource

-(NSUInteger)numberOfLinesInLineChartView:(JBLineChartView *)lineChartView
{
    return self.orderedValuesForUsers.allKeys.count;
}

- (NSUInteger)lineChartView:(JBLineChartView *)lineChartView numberOfVerticalValuesAtLineIndex:(NSUInteger)lineIndex
{
    NSString *key = self.orderedValuesForUsers.allKeys[lineIndex];
    return [self.orderedValuesForUsers[key] count];
}

- (CGFloat)lineChartView:(JBLineChartView *)lineChartView verticalValueForHorizontalIndex:(NSUInteger)horizontalIndex atLineIndex:(NSUInteger)lineIndex
{
    NSString *key = self.orderedValuesForUsers.allKeys[lineIndex];
    return [[self.orderedValuesForUsers[key] objectAtIndex:horizontalIndex] floatValue];
}

- (CGFloat)lineChartView:(JBLineChartView *)lineChartView widthForLineAtLineIndex:(NSUInteger)lineIndex
{
    return 1;
}

- (UIColor *)lineChartView:(JBLineChartView *)lineChartView colorForLineAtLineIndex:(NSUInteger)lineIndex
{
    return _colorsDataSource[self.orderedValuesForUsers.allKeys[lineIndex]];
}

- (BOOL)lineChartView:(JBLineChartView *)lineChartView smoothLineAtLineIndex:(NSUInteger)lineIndex
{
    return YES;
}

- (UIColor *)lineChartView:(JBLineChartView *)lineChartView fillColorForLineAtLineIndex:(NSUInteger)lineIndex
{
    return [_colorsDataSource[self.orderedValuesForUsers.allKeys[lineIndex]] colorWithAlphaComponent:0.35f];
}

@end