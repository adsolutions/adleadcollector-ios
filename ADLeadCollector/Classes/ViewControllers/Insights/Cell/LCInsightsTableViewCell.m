//
//  LCAnalyticsTableViewCell.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 07/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCInsightsTableViewCell.h"
#import "LCGravatarBadgeImageView.h"
#import "NSDate+Extras.h"

#import <JBChartView/JBLineChartView.h>

@implementation LCInsightsUserDetail

@end

@interface LCInsightsTableViewCell () <JBLineChartViewDataSource, JBLineChartViewDelegate>

@property (nonatomic, weak) IBOutlet LCGravatarBadgeImageView *gravatarView;
@property (nonatomic, weak) IBOutlet UILabel *emailLabel;
@property (nonatomic, weak) IBOutlet UILabel *countLabel;
@property (nonatomic, strong) JBLineChartView *lineChart;

@property (nonatomic, strong) NSArray *orderedValues;

@end

@implementation LCInsightsTableViewCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.countLabel setFont:[LCUtils boldFontWithSize:22]];
    [self.countLabel setTextAlignment:NSTextAlignmentRight];
    
    [self.emailLabel setFont:[LCUtils fontWithSize:18]];
    [self.emailLabel setTextColor:FlatBlack];
    self.backgroundColor = ClearColor;
    
    self.lineChart = [[JBLineChartView alloc] initForAutoLayout];
    [self addSubview:self.lineChart];
    [self.lineChart autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.emailLabel];
    [self.lineChart autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:self.gravatarView withOffset:5];
    [self.lineChart autoPinEdge:ALEdgeRight toEdge:ALEdgeLeft ofView:self.countLabel];
    [self.lineChart autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self];
    [self.lineChart setDataSource:self];
    [self.lineChart setDelegate:self];
    [self.lineChart setShowsLineSelection:NO];
    [self.lineChart setShowsVerticalSelection:NO];
    [self bringSubviewToFront:self.lineChart];
    
    [self updateConstraints];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
}

#pragma mark - Setters

-(void)setUserDetail:(LCInsightsUserDetail *)userDetail
{
    _userDetail = userDetail;
    
    [_gravatarView setEmail:userDetail.email];
    if ([userDetail.email isEqualToString:[BTWallet myWallet].emailAddress]) {
        [_emailLabel setText:NSLocalizedString(@"kYou", @"")];
        [_emailLabel setFont:[LCUtils boldFontWithSize:18]];
    }
    else
    {
        [_emailLabel setText:userDetail.userName.length > 0? userDetail.userName : userDetail.email];
    }
    
    [self.countLabel setText:[@(userDetail.count) stringValue]];
    [self.countLabel setTextColor:userDetail.color];
    self.gravatarView.circleColor = userDetail.color;
    
    if (!_userDetail.userInsights) {
        _userDetail.userInsights = @{}.mutableCopy;
    }
    
    if (_userDetail.userInsights.allValues.count == 0) {
        NSMutableDictionary *tempDict = _userDetail.userInsights.mutableCopy;
        NSDate *date = [[NSDate date] dateWithoutTimeComponets];
        for (NSUInteger k = (30-_userDetail.userInsights.allValues.count); k > 0; k--) {
            [tempDict setObject:@(0) forKey:[date string]];
            date = [date dateByAddingTimeInterval:-(60*60*k)];
        }
        
        _userDetail.userInsights = tempDict;
    }
    
    NSArray *index = [userDetail.userInsights.allKeys sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    _orderedValues = [userDetail.userInsights objectsForKeys:index notFoundMarker:@(NAN)];
    
    [self.lineChart setMaximumValue:userDetail.maxValue];
    
    [[NSOperationQueue mainQueue] addOperation:[[NSInvocationOperation alloc] initWithTarget:self.lineChart selector:@selector(reloadData) object:nil]];
}

#pragma mark - JBLineChartViewDataSource

-(NSUInteger)numberOfLinesInLineChartView:(JBLineChartView *)lineChartView
{
    return 1;
}

- (NSUInteger)lineChartView:(JBLineChartView *)lineChartView numberOfVerticalValuesAtLineIndex:(NSUInteger)lineIndex
{
    return _orderedValues.count;
}

- (CGFloat)lineChartView:(JBLineChartView *)lineChartView verticalValueForHorizontalIndex:(NSUInteger)horizontalIndex atLineIndex:(NSUInteger)lineIndex
{
    return [_orderedValues[horizontalIndex] floatValue];
}

- (CGFloat)lineChartView:(JBLineChartView *)lineChartView widthForLineAtLineIndex:(NSUInteger)lineIndex
{
    return 1;
}

- (UIColor *)lineChartView:(JBLineChartView *)lineChartView colorForLineAtLineIndex:(NSUInteger)lineIndex
{
    return _userDetail.color;
}

- (BOOL)lineChartView:(JBLineChartView *)lineChartView smoothLineAtLineIndex:(NSUInteger)lineIndex
{
    return YES;
}

- (UIColor *)lineChartView:(JBLineChartView *)lineChartView fillColorForLineAtLineIndex:(NSUInteger)lineIndex
{
    return [_userDetail.color colorWithAlphaComponent:0.35f];
}

@end