//
//  LCInsightsInviteTeamMateTableViewCell.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 09/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCInsightsInviteTeamMateTableViewCell.h"

#import <FRDLivelyButton/FRDLivelyButton.h>

@interface LCInsightsInviteTeamMateTableViewCell ()

@property (nonatomic, weak) IBOutlet UILabel *inviteTeamMateLabel;
@property (nonatomic, weak) IBOutlet FRDLivelyButton *inviteTeamMateButton;

@end

@implementation LCInsightsInviteTeamMateTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    [_inviteTeamMateLabel setText:NSLocalizedString(@"kInviteTeamMate", @"")];
    [_inviteTeamMateLabel setFont:[LCUtils fontWithSize:18]];
    
    [_inviteTeamMateButton setStyle:kFRDLivelyButtonStyleCirclePlus animated:NO];
    NSMutableDictionary *options = [[FRDLivelyButton defaultOptions] mutableCopy];
    [options setObject:companyColor() forKey:kFRDLivelyButtonColor];
    [options setObject:@(1.0f) forKey:kFRDLivelyButtonLineWidth];
    [_inviteTeamMateButton setOptions:options];
    
    self.backgroundColor = FlatWhite;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end