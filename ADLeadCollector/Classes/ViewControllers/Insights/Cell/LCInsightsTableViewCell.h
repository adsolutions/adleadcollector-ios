//
//  LCAnalyticsTableViewCell.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 07/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCInsightsUserDetail : NSObject

@property (nonatomic, copy) NSString *email;
@property (nonatomic) NSInteger count;
@property (nonatomic, copy) UIColor *color;
@property (nonatomic, copy) NSDictionary *userInsights;
@property (nonatomic) NSInteger maxValue;
@property (nonatomic, copy) NSString *userName;

@end

@interface LCInsightsTableViewCell : UITableViewCell

@property (nonatomic, strong) LCInsightsUserDetail *userDetail;

@end