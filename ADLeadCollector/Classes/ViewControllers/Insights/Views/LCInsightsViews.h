//
//  LCInsightsViews.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 21/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <CouchbaseLite/CouchbaseLite.h>

@interface LCInsightsViews : NSObject

+(CBLQuery *)queryForGlobalInsightsEverView;

+(CBLQuery *)queryForLastQuarterView;
+(CBLQuery *)queryForLastMonthView;
+(CBLQuery *)queryForLastWeekView;

@end