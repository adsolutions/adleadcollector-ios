//
//  LCInsightsViews.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 21/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCInsightsViews.h"
#import "LCLead.h"
#import "NSDate+Extras.h"

NSString *const LCViewLastMonthPersonal = @"LCPersonalInsight_30D";
NSString *const LCViewLastWeekPersonal = @"LCPersonalInsight_7D";
NSString *const LCViewLastQuarterPersonal = @"LCPersonalInsight_120D";
NSString *const LCViewEverPersonal = @"LCPersonalInsight_EVER";

NSString *const LCViewEverGlobal = @"LCGlobalInsight_EVER";
NSString *const LCViewLastMonthGlobal = @"LCGlobalInsight_30D";
NSString *const LCViewLastWeekGlobal = @"LCGlobalInsight_7D";
NSString *const LCViewLastQuarterGlobal = @"LCGlobalInsight_120D";

NSUInteger const LC30Days = 31;
NSUInteger const LC7Days = 7;
NSUInteger const LC120Days = 120;
NSInteger const LCEver = -1;

@implementation LCInsightsViews

#pragma mark - GlobalInsights

+(CBLQuery *)queryForGlobalInsightsEverView
{
    CBLView *view = [[MBEngine database] viewNamed:LCViewEverGlobal];
    [view setMapBlock:MAPBLOCK({
        if ([doc[@"type"] isEqualToString:NSStringFromClass([LCLead class])]) {
            NSString *authorName = doc[@"author"];
            emit(doc[@"_id"], authorName.length > 0? authorName : kUnassigned);
        }
    }) reduceBlock:REDUCEBLOCK({
        NSArray *sortedArray = [values sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        NSMutableDictionary *reducedObjects = @{}.mutableCopy;
        for (NSString *author in sortedArray) {
            NSNumber *number = [reducedObjects objectForKey:author];
            NSInteger value = number? ([number integerValue]+1) : 1;
            [reducedObjects setObject:@(value) forKey:author];
        }
        return reducedObjects;
    }) version:@"1"];
    
    if (view.stale) {
        [view deleteIndex];
    }
    
    CBLQuery *query = [view createQuery];
    [query setMapOnly:NO];
    
    return query;
}

#pragma mark - PersonalInsights

+(CBLQuery *)queryForLastQuarterView
{
    CBLView *usersView = [[MBEngine database] viewNamed:LCViewLastMonthPersonal];
    [usersView setMapBlock:MAPBLOCK({
        if ([self blockCanBeMapped:doc]) {
            NSString *timestamp = [self validTimestampForTimeWindow:LC120Days fromDate:[NSDate dateFromString:doc[@"timestamp"] withFormat:[NSDate timestampFormatString]]];
            if (timestamp) {
                emit(doc[@"author"], @{@"_id" : doc[@"_id"], @"timestamp" : timestamp});
            }
        }
    }) reduceBlock:REDUCEBLOCK({
        return [self reduceObjectForTimeWindow:LC120Days fromKeys:keys andValues:values];
    }) version:@"1"];
    
    if (usersView.stale) {
        [usersView deleteIndex];
    }
    
    CBLQuery *query = [usersView createQuery];
    [query setMapOnly:NO];
    
    return query;
}

+(CBLQuery *)queryForLastMonthView
{
    CBLView *usersView = [[MBEngine database] viewNamed:LCViewLastMonthPersonal];
    [usersView setMapBlock:MAPBLOCK({
        if ([self blockCanBeMapped:doc]) {
            NSString *timestamp = [self validTimestampForTimeWindow:LC30Days fromDate:[NSDate dateFromString:doc[@"timestamp"] withFormat:[NSDate timestampFormatString]]];
            if (timestamp) {
                emit(doc[@"author"], @{@"_id" : doc[@"_id"], @"timestamp" : timestamp});
            }
        }
    }) reduceBlock:REDUCEBLOCK({
        return [self reduceObjectForTimeWindow:LC30Days fromKeys:keys andValues:values];
    }) version:@"1"];
    
    if (usersView.stale) {
        [usersView deleteIndex];
    }
    
    CBLQuery *query = [usersView createQuery];
    [query setMapOnly:NO];
    
    return query;
}

+(CBLQuery *)queryForLastWeekView
{
    CBLView *usersView = [[MBEngine database] viewNamed:LCViewLastWeekPersonal];
    [usersView setMapBlock:MAPBLOCK({
        if ([self blockCanBeMapped:doc]) {
            NSString *timestamp = [self validTimestampForTimeWindow:LC7Days fromDate:[NSDate dateFromString:doc[@"timestamp"] withFormat:[NSDate timestampFormatString]]];
            if (timestamp) {
                emit(doc[@"author"], @{@"_id" : doc[@"_id"], @"timestamp" : timestamp});
            }
        }
    }) reduceBlock:REDUCEBLOCK({
        return [self reduceObjectForTimeWindow:LC7Days fromKeys:keys andValues:values];
    }) version:@"1"];
    
    if (usersView.stale) {
        [usersView deleteIndex];
    }
    
    CBLQuery *query = [usersView createQuery];
    [query setMapOnly:NO];
    
    return query;
}

#pragma mark - Utils

+(BOOL)blockCanBeMapped:(NSDictionary *)map
{
    if ([map[@"type"] isEqualToString:NSStringFromClass([LCLead class])] && [map[@"timestamp"] length] > 0 && [map[@"author"] length] > 0) {
        return YES;
    }
    
    return NO;
}

+(NSString *)validTimestampForTimeWindow:(NSInteger)timeWindow fromDate:(NSDate *)date
{
    if (!date) {
        return nil;
    }
    
    NSDate *dateWithoutTime = [date dateWithoutTimeComponets];
    NSDate *todayWithoutTime = [[NSDate date] dateWithoutTimeComponets];
    
    NSDate *daysInWindow = [todayWithoutTime dateByAddingTimeInterval:-(timeWindow*24*60*60)];
    
    if ([NSDate date:[date dateWithoutTimeComponets] isBetweenDate:daysInWindow andDate:todayWithoutTime]) {
        return [dateWithoutTime description];
    }
    
    if ([dateWithoutTime isEqualToDate:daysInWindow]) {
        return [dateWithoutTime description];
    }
    
    return nil;
}

+(NSDictionary *)reduceObjectForTimeWindow:(NSInteger)timeWindow fromKeys:(NSArray *)keys andValues:(NSArray *)values
{
    NSMutableDictionary *days = @{}.mutableCopy;
    for (int k = 1; k < timeWindow+1; k++) {
        [days setObject:@(0) forKey:[[[[NSDate date] dateWithoutTimeComponets] dateByAddingTimeInterval:-(k*24*60*60)] description]];
    }
    
    NSMutableDictionary *reducedObjects = @{}.mutableCopy;
    for (int k = 0; k < keys.count; k++) {
        NSString *author = keys[k];
        
        NSMutableDictionary *datesForAuthor = reducedObjects[author];
        if (!datesForAuthor) {
            datesForAuthor = [days mutableCopy];
        }
        
        NSNumber *number = [datesForAuthor objectForKey:values[k][@"timestamp"]];
        NSInteger value = number? ([number integerValue]+1) : 1;
        [datesForAuthor setObject:@(value) forKey:values[k][@"timestamp"]];
        [reducedObjects setObject:datesForAuthor forKey:author];
    }
    return reducedObjects;
}

@end