//
//  LCWelcomeViewController.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 15/05/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "PWCirclesViewController.h"

#import <CRToast/CRToast.h>
#import <TPKeyboardAvoiding/TPKeyboardAvoidingScrollView.h>

#define kButtonSize CGSizeMake(IS_IPAD? 320 : (self.view.frame.size.width/100)*80, 55)

@protocol LCLoginViewControllerDelegate <NSObject>

-(void)loginViewController:(id)viewController didAuthenticateUserWithWallet:(BTWallet *)wallet wasSubscribing:(BOOL)subscribing andData:(NSDictionary *)userData;

@end

@interface LCWelcomeViewController : PWCirclesViewController

@property (nonatomic, unsafe_unretained) id <LCLoginViewControllerDelegate> delegate;

@property (nonatomic, strong) TPKeyboardAvoidingScrollView *scrollView;

@property (nonatomic, strong) UIImageView *logoImageView;
@property (nonatomic, strong) UILabel *footerLabel;

-(void)displayError:(NSString *)error;

-(void)loadStandardUIComponents;
-(void)loadPersonalizedUIComponents;

@end