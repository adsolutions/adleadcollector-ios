//
//  LCWelcomeViewController.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 15/05/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCWelcomeViewController.h"
#import "LCLoginViewController.h"
#import "LCNavigationController.h"
#import "LCFormViewController.h"
#import "LCAlwaysTemplateImage.h"

#import <MAFormViewController/MAFormField.h>
#import <NSString+Validation/NSString+Validation.h>

@interface LCWelcomeViewController ()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, weak) UIView *line1;
@property (nonatomic, weak) UIView *line2;
@property (nonatomic, strong) UIButton *loginButton;
@property (nonatomic, strong) UIButton *signupButton;

@end

@implementation LCWelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadStandardUIComponents];
    
    [self loadPersonalizedUIComponents];
    
    [self.view setBackgroundColor:[companyColor() colorWithAlphaComponent:1]];
}

-(void)loadPersonalizedUIComponents
{
    _loginButton = ({
        UIButton *button = [[UIButton alloc] initForAutoLayout];
        [_scrollView addSubview:button];
        [button autoSetDimensionsToSize:kButtonSize];
        [button autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_logoImageView withOffset:20];
        [button autoAlignAxisToSuperviewAxis:ALAxisVertical];
        [button autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
        [button setBackgroundColor:companyColor()];
        [button setShowsTouchWhenHighlighted:YES];
        [button.titleLabel setFont:[LCUtils boldFontWithSize:26]];
        [button setTitle:@"Login" forState:UIControlStateNormal];
        [button.layer setCornerRadius:6];
        [button.layer setBorderWidth:0.5];
        [button.layer setBorderColor:[UIColor whiteColor].CGColor];
        [button addTarget:self action:@selector(loginButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        button;
    });
    
    _titleLabel = ({
        UILabel *label = [[UILabel alloc] initForAutoLayout];
        [self.view addSubview:label];
        [label autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_loginButton withOffset:20];
        [label autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:_scrollView];
        [label autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:_scrollView];
        [label autoSetDimension:ALDimensionHeight toSize:30];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setText:NSLocalizedString(@"kDontHaveAnAccount", @"")];
        [label setFont:[LCUtils fontWithSize:16]];
        [label setNumberOfLines:1];
        [label setMinimumScaleFactor:0.5];
        [label setTextColor:[UIColor whiteColor]];
        label;
    });
    
    _signupButton = ({
        UIButton *button = [[UIButton alloc] initForAutoLayout];
        [_scrollView addSubview:button];
        [button autoSetDimensionsToSize:kButtonSize];
        [button autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_titleLabel withOffset:0];
        [button autoAlignAxisToSuperviewAxis:ALAxisVertical];
        [button setBackgroundColor:FlatBlue];
        [button setShowsTouchWhenHighlighted:YES];
        [button.titleLabel setFont:[LCUtils boldFontWithSize:26]];
        [button.layer setCornerRadius:6];
        [button.layer setBorderWidth:0.5];
        [button.layer setBorderColor:[UIColor whiteColor].CGColor];
        [button setTitle:NSLocalizedString(@"kSignupHere", @"") forState:UIControlStateNormal];
        [button addTarget:self action:@selector(signupButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        button;
    });
}

-(void)loadStandardUIComponents
{
    _scrollView = ({
        TPKeyboardAvoidingScrollView *scrollView = [[TPKeyboardAvoidingScrollView alloc] initForAutoLayout];
        [self.view addSubview:scrollView];
        [scrollView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
        scrollView;
    });
    
    _logoImageView = ({
        UIImageView *image = [[UIImageView alloc] initForAutoLayout];
        [_scrollView addSubview:image];
        [image autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.scrollView];
        [image autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.scrollView];
        [image autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.scrollView withOffset:30];
        [image autoSetDimension:ALDimensionHeight toSize:90];
        [image autoAlignAxisToSuperviewAxis:ALAxisVertical];
        [image setTintColor:[UIColor whiteColor]];
        [image setImage:[LCAlwaysTemplateImage alwaysTemplateImageNamed:@"LeadsCollectorGreen"]];
        [image setContentMode:UIViewContentModeCenter];
        image;
    });
    
    _footerLabel = ({
        UILabel *label = [[UILabel alloc] initForAutoLayout];
        [self.view addSubview:label];
        [label autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero excludingEdge:ALEdgeTop];
        [label autoSetDimension:ALDimensionHeight toSize:30];
        [label setBackgroundColor:companyColor()];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setText:@"© 2015 Leads Collector - All Rights Reserved"];
        [label setFont:[LCUtils fontWithSize:IS_IPAD? 18 : 14]];
        [label setNumberOfLines:1];
        [label setMinimumScaleFactor:0.5];
        [label setTextColor:[UIColor whiteColor]];
        label;
    });
}

#pragma mark - Actions

-(void)loginButtonPressed:(id)sender
{
    LCLoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LCLoginViewController"];
    [viewController setDelegate:self.delegate];
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)signupButtonPressed:(id)sender
{
    MAFormField *nameField = [MAFormField fieldWithKey:@"name" type:MATextFieldTypeName initialValue:@""placeholder:NSLocalizedString(@"kName", @"") required:YES];
    MAFormField *surnameField = [MAFormField fieldWithKey:@"surname" type:MATextFieldTypeName initialValue:@""placeholder:NSLocalizedString(@"kSurname", @"") required:YES];
    MAFormField *emailField = [MAFormField fieldWithKey:@"email" type:MATextFieldTypeEmail initialValue:@"" placeholder:NSLocalizedString(@"kEmail", @"") required:YES];
    MAFormField *passwordField = [MAFormField fieldWithKey:@"password" type:MATextFieldTypePassword initialValue:@"" placeholder:NSLocalizedString(@"kPassword", @"") required:YES];
    MAFormField *companyNameField = [MAFormField fieldWithKey:@"companyName" type:MATextFieldTypeDefault initialValue:@"" placeholder:NSLocalizedString(@"kCompanyName", @"") required:NO];
    
    // separate the cells into sections
    NSArray *firstSection = @[nameField, surnameField, emailField, passwordField];
    NSArray *secondSection = @[companyNameField];
    NSArray *cellConfig = @[firstSection, secondSection];
    NSArray *sectionsHeader = @[NSLocalizedString(@"kAccountDetails", @""), NSLocalizedString(@"kCompanyDetails", @"")];
    
    __block UINavigationController *navigationController = nil;
    
    __block typeof(self) me = self;
    
    // create the form and present it modally with its own navigation controller
    __block LCFormViewController *formVC = [[LCFormViewController alloc] initWithCellConfigurations:cellConfig actionText:NSLocalizedString(@"kSubmit", @"") animatePlaceholders:YES handler:^(NSDictionary *resultDictionary) {
        
        // if we don't have a result dictionary, the user cancelled, rather than submitted the form
        if (!resultDictionary) {
            [formVC dismissViewControllerAnimated:YES completion:nil];
            return;
        }
        
        NSString *email = [resultDictionary[@"email"] lowercaseString];
        
        [[BTEngine sharedEngine] postRestCallbackOnMethod:@"/api/public/v1/user" withParameters:@{@"email" : email, @"notification" : @"false"} andCompletionHandler:^(NSError *error, NSDictionary *response) {
            if (!error && [response[@"success"] boolValue]) {
                __block BTWallet *tempWallet = [[BTWallet alloc] initWithEmailAddress:email password:resultDictionary[@"password"]];
                
                [[BTEngine sharedEngine] postRestCallbackOnMethod:@"/api/public/v1/user/activate" withParameters:@{@"password" : tempWallet.encryptedPassword, @"email" : tempWallet.emailAddress, @"name" : resultDictionary[@"name"], @"surname" : resultDictionary[@"surname"], @"company" : ((NSString *)resultDictionary[@"companyName"]).length == 0? @"" : resultDictionary[@"companyName"], @"key" : response[@"pending"][@"key"], @"token" : response[@"pending"][@"token"]} andCompletionHandler:^(NSError *error, NSDictionary *response) {
                    if (!error) {
                        UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                        [indicatorView setFrame:[(UIButton *)sender frame]];
                        [indicatorView setColor:[UIColor whiteColor]];
                        [me.view addSubview:indicatorView];
                        [indicatorView startAnimating];
                        [sender setHidden:YES];
                        
                        [_loginButton setEnabled:NO];
                        
                        LCLoginViewController *viewController = [me.storyboard instantiateViewControllerWithIdentifier:@"LCLoginViewController"];
                        [viewController setDelegate:me.delegate];
                        [viewController setWallet:tempWallet];
                        [navigationController pushViewController:viewController animated:YES];
                        
                        [_titleLabel setHidden:YES];
                    }
                    else
                    {
                        [formVC reset];
                        NSString *errorString = error.localizedDescription;
                        
                        if ([NSString isEmptyString:errorString]) {
                            errorString = response[@"errorMessage"];
                        }
                        
                        [me displayError:errorString];
                        
                        [Answers logSignUpWithMethod:@"Bitrace" success:@NO customAttributes:@{@"email" : email, @"error" : errorString}];
                    }
                }];
            }
            else
            {
                [formVC reset];
                
                NSString *errorString = error.localizedDescription;
                
                if ([NSString isEmptyString:errorString]) {
                    errorString = response[@"errorMessage"];
                }
                
                [me displayError:errorString];
                
                [Answers logSignUpWithMethod:@"Bitrace" success:@NO customAttributes:@{@"email" : email, @"error" : errorString}];
            }
        }];
    }];
    
    [formVC setSectionsHeader:sectionsHeader];
    [formVC.navigationItem setTitle:NSLocalizedString(@"kSignup", @"")];
    
    navigationController = [[UINavigationController alloc] initWithRootViewController:formVC];
    [self presentViewController:navigationController animated:YES completion:nil];
}

-(void)displayError:(NSString *)error
{
    [CRToastManager showNotificationWithOptions:[LCUtils errorToastWithTitle:NSLocalizedString(@"kError", @"") andMessage:error] completionBlock:nil];
}

@end