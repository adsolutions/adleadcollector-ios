//
//  LCLoginViewController.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 14/05/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCLoginViewController.h"
#import "FRDLivelyButton.h"

#import <UITextField+Shake/UITextField+Shake.h>
#import <NSString+Validation/NSString+Validation.h>

@interface LCLoginViewController ()

@property (nonatomic, strong) UIButton *loginButton;

@property (nonatomic, strong) FRDLivelyButton *backButton;

@property (nonatomic, strong) UITextField *emailTextField;
@property (nonatomic, strong) UITextField *passwordTextField;

@property (nonatomic) BOOL subscribing;

@end

@implementation LCLoginViewController

-(void)loadPersonalizedUIComponents
{
    _emailTextField = ({
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 60, 60)];
        [label setText:@" Email"];
        [label setFont:[LCUtils boldFontWithSize:18]];
        [label setTextColor:companyColor()];
        
        UITextField *textField = [[UITextField alloc] initForAutoLayout];
        [self.scrollView addSubview:textField];
        [textField autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.logoImageView withOffset:40];
        [textField autoAlignAxisToSuperviewAxis:ALAxisVertical];
        if ([UIScreen mainScreen].bounds.size.height > 570) {
            [textField autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
        }
        
        [textField autoSetDimensionsToSize:kButtonSize];
        [textField setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.95]];
        [textField setFont:[LCUtils fontWithSize:20]];
        [textField setTextColor:companyColor()];
        [textField setBorderStyle:UITextBorderStyleNone];
        [textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        [textField setLeftView:label];
        [textField.layer setBorderWidth:0.5];
        [textField.layer setCornerRadius:6];
        [textField.layer setBorderColor:[UIColor whiteColor].CGColor];
        [textField setKeyboardType:UIKeyboardTypeEmailAddress];
        [textField setLeftViewMode:UITextFieldViewModeAlways];
        [textField setClearButtonMode:UITextFieldViewModeUnlessEditing];
        [textField setTintColor:companyColor()];
        [textField setText:[BTWallet myWallet].emailAddress];
        textField;
    });
    
    _passwordTextField = ({
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 95, 60)];
        [label setText:@" Password"];
        [label setFont:[LCUtils boldFontWithSize:18]];
        [label setTextColor:companyColor()];
        
        UITextField *textField = [[UITextField alloc] initForAutoLayout];
        [self.scrollView addSubview:textField];
        [textField autoSetDimensionsToSize:kButtonSize];
        [textField autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_emailTextField withOffset:4];
        [textField autoAlignAxisToSuperviewAxis:ALAxisVertical];
        [textField setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.95]];
        [textField setFont:[LCUtils fontWithSize:20]];
        [textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        [textField setSecureTextEntry:YES];
        [textField setTextColor:companyColor()];
        [textField.layer setBorderWidth:0.5];
        [textField setBorderStyle:UITextBorderStyleNone];
        [textField.layer setBorderColor:[UIColor whiteColor].CGColor];
        [textField setLeftView:label];
        [textField.layer setCornerRadius:6];
        [textField setClearButtonMode:UITextFieldViewModeUnlessEditing];
//        [textField setClearsOnBeginEditing:YES];
        [textField setLeftViewMode:UITextFieldViewModeAlways];
        [textField setTintColor:companyColor()];
        [textField setText:[BTWallet myWallet].password];
        textField;
    });
    
    _loginButton = ({
        UIButton *button = [[UIButton alloc] initForAutoLayout];
        [self.scrollView addSubview:button];
        [button autoSetDimensionsToSize:kButtonSize];
        [button autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_passwordTextField withOffset:20];
        [button autoAlignAxisToSuperviewAxis:ALAxisVertical];
        [button setBackgroundColor:companyColor()];
        [button setShowsTouchWhenHighlighted:YES];
        [button.titleLabel setFont:[LCUtils boldFontWithSize:26]];
        [button.layer setBorderWidth:0.5];
        [button.layer setBorderColor:[UIColor whiteColor].CGColor];
        [button.layer setCornerRadius:6];
        [button setTitle:@"Login" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(loginButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        button;
    });
}

-(void)viewWillAppear:(BOOL)animated
{
    [_emailTextField setTintColor:companyColor()];
    [_passwordTextField setTintColor:companyColor()];
    
    if (_wallet) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        
        [_passwordTextField setText:_wallet.password];
        [_emailTextField setText:_wallet.emailAddress];
        
        [super viewWillAppear:animated];
        
        return;
    }
    else if ([BTWallet myWallet]) {
        [_passwordTextField setText:[BTWallet myWallet].password];
        [_emailTextField setText:[BTWallet myWallet].emailAddress];
    }
    
    _backButton = [[FRDLivelyButton alloc] initWithFrame:CGRectMake(5, 20, 35, 35)];
    [_backButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_backButton setStyle:kFRDLivelyButtonStyleCaretLeft animated:animated];
    NSMutableDictionary *options = [[FRDLivelyButton defaultOptions] mutableCopy];
    [options setObject:[UIColor whiteColor] forKey:kFRDLivelyButtonColor];
    [options setObject:@(2.0f) forKey:kFRDLivelyButtonLineWidth];
    [_backButton setOptions:options];
    [self.view addSubview:_backButton];
    
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    if (_wallet) {
        [self performSelectorOnMainThread:@selector(loginButtonPressed:) withObject:_loginButton waitUntilDone:YES];
    }
}

#pragma mark - Setters

-(void)setWallet:(BTWallet *)wallet
{
    _wallet = wallet;
    _subscribing = YES;
}

#pragma mark - Actions

-(void)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)loginButtonPressed:(id)sender
{
    if ((![NSString isEmptyString:self.passwordTextField.text]) && (![NSString isEmptyString:self.emailTextField.text]))
    {
        __block UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [indicatorView setFrame:[(UIButton *)sender frame]];
        [indicatorView setColor:[UIColor whiteColor]];
        [self.view addSubview:indicatorView];
        [indicatorView startAnimating];
        [sender setHidden:YES];
        
        [_passwordTextField setEnabled:NO];
        [_emailTextField setEnabled:NO];
        
        [_emailTextField resignFirstResponder];
        [_passwordTextField resignFirstResponder];
        
        // lower case email
        self.emailTextField.text = [self.emailTextField.text lowercaseString];
        
        __block typeof(self) me = self;
        
        BTRemoteKeychainServer *server = [[BTRemoteKeychainServer alloc] init];
        BTWallet *wallet = [[BTWallet alloc] initWithEmailAddress:self.emailTextField.text password:self.passwordTextField.text];
        [server verifyUserWallet:wallet withCompletionHandler:^(NSError *error, BOOL verified, BTWallet *updatedWallet) {
            if (!error)
            {
                NSString *companyID = nil;
                for (NSDictionary *company in wallet.companiesArray) {
                    if ([company[@"identifier"] isEqualToString:wallet.data[@"favorite_company"]]) {
                        companyID = company[@"identifier"];
                        break;
                    }
                }
                
                if ([companyID length] == 0)
                {
                    companyID = updatedWallet.emailAddress;
                }
            
                [[BTEngine sharedEngine] getRestCallbackOnMethod:[NSString stringWithFormat:@"api/v1/company/%@", companyID] withParameters:@{@"parameters" : @"*"} andCompletionHandler:^(NSError *error, NSDictionary *response) {
                    if (!error) {
                        [me.delegate loginViewController:me didAuthenticateUserWithWallet:wallet wasSubscribing:_subscribing andData:response];
                    }
                    else
                    {
                        [me displayError:error.localizedDescription];
                        
                        [me.passwordTextField shake];
                        [me.emailTextField shake];
                        
                        [_passwordTextField setEnabled:YES];
                        [_emailTextField setEnabled:YES];
                        
                        [Answers logLoginWithMethod:@"Bitrace" success:@NO customAttributes:@{@"email" : _emailTextField.text, @"error" : error.debugDescription}];
                    }
                    
                    [indicatorView stopAnimating];
                    [indicatorView removeFromSuperview];
                    [sender setHidden:NO];
                }];
            }
            else
            {
                [me displayError:error.localizedDescription];
                
                [me.passwordTextField shake];
                [me.emailTextField shake];
                
                [_passwordTextField setEnabled:YES];
                [_emailTextField setEnabled:YES];
                
                [indicatorView stopAnimating];
                [indicatorView removeFromSuperview];
                [sender setHidden:NO];
                
                [Answers logLoginWithMethod:@"Bitrace" success:@NO customAttributes:@{@"email" : _emailTextField.text, @"error" : error.debugDescription}];
            }
        }];
    }
    else
    {
        [self.passwordTextField shake];
        [self.emailTextField shake];
    }
}

@end