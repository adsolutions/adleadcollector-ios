//
//  LCLoginViewController.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 14/05/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCWelcomeViewController.h"

@interface LCLoginViewController : LCWelcomeViewController

@property (nonatomic, strong) BTWallet *wallet;

@end