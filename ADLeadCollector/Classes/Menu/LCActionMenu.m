//
//  LCFolderActionMenuViewController.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 04/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCActionMenu.h"

#import <FontAwesome+iOS/UIImage+FontAwesome.h>
#import <FontAwesome+iOS/NSString+FontAwesome.h>

@implementation LCActionMenuImage

+(instancetype)imageWithSize:(CGSize)size color:(UIColor *)color andType:(LCActionMenuButtonType)type
{
    return (LCActionMenuImage *)[UIImage imageWithIcon:[self iconNameForType:type] backgroundColor:ClearColor iconColor:color iconScale:1 andSize:size];
}

+(NSString *)iconNameForType:(LCActionMenuButtonType)type
{
    switch (type) {
        case LCActionMenuButtonTypeMail:
            return @"icon-envelope";
            break;
        case LCActionMenuButtonTypePhoneCall:
            return @"icon-phone";
            break;
        case LCActionMenuButtonTypeTextMessage:
            return @"icon-comment";
            break;
        case LCActionMenuButtonTypeMic:
            return @"icon-microphone";
            break;
        case LCActionMenuButtonTypeTeam:
            return @"icon-group";
            break;
        case LCActionMenuButtonTypeStarred:
            return @"icon-star";
            break;
        case LCActionMenuButtonTypeUnStarred:
            return @"icon-star-empty";
            break;
        case LCActionMenuButtonTypeDelete:
            return @"icon-trash";
            break;
        case LCActionMenuButtonTypePicture:
            return @"icon-camera";
            break;
        case LCActionMenuButtonTypeUpload:
            return @"icon-cloud-upload";
            break;
        case LCActionMenuButtonTypeDownload:
            return @"icon-cloud-download";
            break;
        default:
            return nil;
            break;
    }
}

@end

#define kImageSize CGSizeMake(45, 45)

@implementation LCActionMenuButton

-(void)setNeedsLayout
{
    [self.layer setCornerRadius:self.frame.size.height/2];
    [self setShowsTouchWhenHighlighted:YES];
}

-(void)setup
{
    switch (_type) {
        case LCActionMenuButtonTypeChangeColor:
            [self setImage:[UIImage imageNamed:@"changeColor"] forState:UIControlStateNormal];
            break;
        case LCActionMenuButtonTypeChangeType:
            break;
        case LCActionMenuButtonTypeRename:
            [self setImage:[UIImage imageNamed:@"rename"] forState:UIControlStateNormal];
            break;
        default:
            [self setImage:[LCActionMenuImage imageWithSize:kImageSize color:[UIColor whiteColor] andType:_type] forState:UIControlStateNormal];
            break;
    }
}

-(void)setType:(LCActionMenuButtonType)style
{
    _type = style;
    switch (_type) {
        case LCActionMenuButtonTypeChangeColor:
            self.backgroundColor = FlatWhite;
            break;
        case LCActionMenuButtonTypeChangeType:
            self.backgroundColor = FlatSand;
            break;
        case LCActionMenuButtonTypeDelete:
            self.backgroundColor = FlatRed;
            break;
        case LCActionMenuButtonTypeRename:
            self.backgroundColor = FlatTeal;
            break;
        case LCActionMenuButtonTypeMail:
            self.backgroundColor = FlatSkyBlue;
            break;
        case LCActionMenuButtonTypePhoneCall:
            self.backgroundColor = FlatNavyBlue;
            break;
        case LCActionMenuButtonTypeTextMessage:
            self.backgroundColor = FlatGreen;
            break;
        default:
            break;
    }
    
    [self setup];
}

@end

@interface LCActionMenu ()

@property (nonatomic, strong) UILabel *mainTitleLabel;

@end

@implementation LCActionMenu

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Setters

-(void)setButtons:(NSArray *)buttons
{
    [self.view.subviews enumerateObjectsUsingBlock:^(UIView *obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
    
    _mainTitleLabel = [[UILabel alloc] initForAutoLayout];
    [_mainTitleLabel setBackgroundColor:ClearColor];
    [_mainTitleLabel setText:NSLocalizedString(@"kChooseAnOption", @"")];
    [_mainTitleLabel setTextAlignment:NSTextAlignmentCenter];
    [_mainTitleLabel setFont:[LCUtils fontWithSize:22]];
    [_mainTitleLabel setTextColor:FlatWhite];
    [self.view addSubview:_mainTitleLabel];
    [_mainTitleLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.view withOffset:40];
    [_mainTitleLabel autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.view];
    [_mainTitleLabel autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    
    UIView *previousView = self.mainTitleLabel;
    
    for (int k = 0; k < buttons.count; k++) {
        LCActionMenuButton *button = [[LCActionMenuButton alloc] initForAutoLayout];
        [self.view addSubview:button];
        [button setType:(LCActionMenuButtonType)[buttons[k] unsignedIntegerValue]];
        [button autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
        [button autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:previousView withOffset:30];
        [button autoSetDimension:ALDimensionHeight toSize:80];
        [button autoSetDimension:ALDimensionWidth toSize:80];
        [button addTarget:self action:@selector(actionButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        previousView = button;
    }
    
//    if ([self.view.subviews count] > 0) {
//        NSUInteger index = [self.view.subviews count]/2;
//        UIView *middleView = [self.view.subviews objectAtIndex:index];
//        [middleView autoCenterInSuperview];
//    }
}

#pragma mark - Actions

-(IBAction)tapGestureRecognizer:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)actionButtonPressed:(LCActionMenuButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(actionMenu:didPressButtonForType:)]) {
        [self.delegate actionMenu:self didPressButtonForType:sender.type];
    }
}

@end