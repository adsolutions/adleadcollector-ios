//
//  LCFolderActionMenuViewController.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 04/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, LCActionMenuButtonType)
{
    LCActionMenuButtonTypeDelete = 0,
    LCActionMenuButtonTypeRename = 1,
    LCActionMenuButtonTypeChangeType = 2,
    LCActionMenuButtonTypeChangeColor = 3,
    LCActionMenuButtonTypePhoneCall = 4,
    LCActionMenuButtonTypeTextMessage = 5,
    LCActionMenuButtonTypeMail = 6,
    LCActionMenuButtonTypeMic = 7,
    LCActionMenuButtonTypeTeam = 8,
    LCActionMenuButtonTypeStarred = 9,
    LCActionMenuButtonTypeUnStarred = 10,
    LCActionMenuButtonTypePicture = 11,
    LCActionMenuButtonTypeUpload = 12,
    LCActionMenuButtonTypeDownload = 13
};

@class LCActionMenu;

@protocol LCActionMenuDelegate <NSObject>

-(void)actionMenu:(LCActionMenu *)menu didPressButtonForType:(LCActionMenuButtonType)type;

@end

@interface LCActionMenuImage : UIImage

+(instancetype)imageWithSize:(CGSize)size color:(UIColor *)color andType:(LCActionMenuButtonType)type;

@end

@interface LCActionMenuButton : UIButton

@property (nonatomic) LCActionMenuButtonType type;

@end

@interface LCActionMenu : UIViewController

@property (nonatomic, unsafe_unretained) id <LCActionMenuDelegate> delegate;
@property (nonatomic, strong) NSArray *buttons;

@end