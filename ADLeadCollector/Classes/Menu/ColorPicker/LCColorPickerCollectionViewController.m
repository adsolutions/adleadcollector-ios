//
//  LCColorPickerCollectionViewController.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 05/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCColorPickerCollectionViewController.h"

#include <UIColor-HexString/UIColor+HexString.h>

@interface LCColorPickerCollectionViewController ()

@property (nonatomic, strong) NSArray *colorsArray;

@end

@implementation LCColorPickerCollectionViewController

static NSString * const reuseIdentifier = @"LCColorPickerCollectionViewCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"f1f4f4"];
    
    self.title = NSLocalizedString(@"kPickAColor", @"");
    
    // Do any additional setup after loading the view.
//    _colorsArray = @[FlatBlack, FlatBlackDark, FlatBlue, FlatBlueDark, FlatBrown, FlatBrownDark, FlatCoffee, FlatCoffeeDark, FlatForestGreen, FlatForestGreenDark, FlatGray, FlatGrayDark, FlatGreen, FlatGreenDark, FlatLime, FlatLimeDark, FlatMagenta, FlatMagentaDark, FlatMaroon, FlatMaroonDark, FlatMint, FlatMintDark, FlatNavyBlue, FlatNavyBlueDark, FlatOrange, FlatOrangeDark, FlatPink, FlatPinkDark, FlatPlum, FlatPlumDark, FlatPowderBlue, FlatPowderBlueDark, FlatPurple, FlatPurpleDark, FlatRed, FlatRedDark, FlatSand, FlatSandDark, FlatSkyBlue, FlatSkyBlueDark, FlatTeal, FlatTealDark, FlatWatermelon, FlatWatermelonDark, FlatWhiteDark, FlatYellow, FlatYellowDark];
//    _colorsArray = [[_colorsArray reverseObjectEnumerator] allObjects];
    
    _colorsArray = @[FlatBlackDark, FlatGrayDark, FlatGray, FlatWhiteDark, FlatBrownDark, FlatBrown, FlatMaroonDark, FlatMaroon, FlatRedDark, FlatRed, FlatWatermelonDark, FlatWatermelon, FlatOrangeDark, FlatOrange, FlatYellowDark, FlatYellow, FlatCoffeeDark, FlatCoffee, FlatSandDark, FlatSand, FlatForestGreenDark, FlatForestGreen, FlatLimeDark, FlatLime, FlatMintDark, FlatMint, FlatGreenDark, FlatGreen, FlatNavyBlueDark, FlatNavyBlue, FlatBlueDark, FlatBlue, FlatTealDark, FlatTeal, FlatSkyBlueDark, FlatSkyBlue, FlatPlumDark, FlatPlum, FlatMagentaDark, FlatMagenta, FlatPurpleDark, FlatPurple, FlatPowderBlueDark, FlatPowderBlue];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _colorsArray.count;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    [cell setBackgroundColor:_colorsArray[indexPath.row]];
    return cell;
}

#pragma mark - UICollectionViewDelegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(colorPicker:didPickColor:)]) {
        [self.delegate colorPicker:self didPickColor:_colorsArray[indexPath.row]];
    }
}

@end