//
//  LCColorPickerCollectionViewCell.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 05/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCColorPickerCollectionViewCell.h"

@implementation LCColorPickerCollectionViewCell

-(void)awakeFromNib
{
    [self.layer setCornerRadius:self.frame.size.height/2];
    self.layer.borderColor = FlatBlack.CGColor;
    self.layer.borderWidth = 0.15f;
}

@end