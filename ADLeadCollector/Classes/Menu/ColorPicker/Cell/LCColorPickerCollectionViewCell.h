//
//  LCColorPickerCollectionViewCell.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 05/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCColorPickerCollectionViewCell : UICollectionViewCell

@end
