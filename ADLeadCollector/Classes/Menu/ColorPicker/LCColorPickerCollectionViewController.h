//
//  LCColorPickerCollectionViewController.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 05/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MZFormSheetController/MZFormSheetController.h>

@class LCColorPickerCollectionViewController;

@protocol LCColorPickerDelegate <NSObject>

-(void)colorPicker:(LCColorPickerCollectionViewController *)picker didPickColor:(UIColor *)color;

@end

@interface LCColorPickerCollectionViewController : MZFormSheetController

@property (nonatomic, unsafe_unretained) id <LCColorPickerDelegate> delegate;

@end