//
//  LCForm.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 10/05/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCModel.h"

@interface LCForm : LCModel

@property (nonatomic, copy) NSArray *form;

/* 1.2 */
@property (nonatomic, copy) NSString *version;

@end