//
//  Persona.h
//  RubricaCouchBase
//
//  Created by Matteo on 17/02/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCModel.h"
#import <FXForms/FXForms.h>

@interface LCLead : LCModel

/* person */
@property (copy) NSString *nameSurname;
@property (copy) NSString *email;
@property (copy) NSString *phoneNumber;
@property (copy) NSString *jobPosition;

/* company */
@property (copy) NSString *companyName;
@property (copy) NSString *companyEmail;
@property (copy) NSString *companyPhone;
@property (copy) NSString *employeesNumber;

/* generic */
@property (copy) NSString *webSite;
@property (copy) NSString *additionalNotes;

@property (assign) NSInteger gender;
@property (assign) NSUInteger age;
@property (copy) NSString *dateOfBirth;
@property (copy) NSString *country;
@property (copy) NSString *language;
@property (copy) NSArray *interests;
@property (assign) NSInteger areasOfInterest;

@property (copy) NSString *notifications;

@property (assign) bool agreedToTerms;

@property (copy) NSString *author;
@property (copy) NSString *associatedFolder;
@property (copy) NSString *timestamp;

/* 1.2 */
@property (copy) NSArray *audioFileURLs;

@property (copy) NSArray *pictureFileURLs;

@property (assign) bool starred;

@end