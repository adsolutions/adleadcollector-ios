//
//  LCMobel.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 27/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCModel.h"

#import <ALSystemUtilities/ALSystem.h>

@implementation LCModel

-(NSString *)owner
{
    return [BTWallet myWallet].emailAddress;
}

-(NSString *)platform
{
    return [[ALHardware deviceName] stringByAppendingFormat:@" (%@)", [ALHardware deviceModel]];
}

@end