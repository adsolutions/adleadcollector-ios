//
//  LCFolder.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 29/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCFolder.h"
#import "LCFolderCBLViews.h"

#import <Underscore.m/Underscore.h>

@implementation LCFolder

@dynamic name, hexColor;

-(NSInteger)leadsCount
{
    CBLQuery *query = [LCFolderCBLViews queryForFoldersView];
    [query setMapOnly:NO];
    for (CBLQueryRow *row in [query run:nil]) {
        for (NSString *folderID in row.value) {
            if ([folderID isEqualToString:self.document.documentID]) {
                return [row.value[folderID] integerValue];
            }
        }
    }

    return 0;
}

-(NSArray *)relatedLeads
{
    CBLQuery *query = [LCFolderCBLViews queryForFoldersView];
    [query setMapOnly:YES];
    [query setPostFilter:[NSPredicate predicateWithFormat:@"value == %@", self.document.documentID]];
    return Underscore.pluck([query run:nil].allObjects, @"key");
}

@end