//
//  Persona.m
//  RubricaCouchBase
//
//  Created by Matteo on 17/02/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCLead.h"
#import "ISO3166CountryValueTransformer.h"

@implementation LCLead

@dynamic nameSurname, companyPhone, companyEmail, companyName, areasOfInterest, agreedToTerms, dateOfBirth, gender, employeesNumber, email, interests, jobPosition, language, phoneNumber, notifications, webSite, age, country, additionalNotes, timestamp, associatedFolder, author, audioFileURLs, pictureFileURLs, starred;

//because we want to rearrange how this form
//is displayed, we've implemented the fields array
//which lets us dictate exactly which fields appear
//and in what order they appear

+(NSString *)emitKey
{
    return @"nameSurname";
}

@end