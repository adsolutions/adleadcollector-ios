//
//  LCSettings.m
//  ADLeadCollector
//
//  Created by Daniele Angeli on 11/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCSettings.h"

@implementation LCSettings

@dynamic mailChimpAPIKey, mailChimpListName, couchbaseURL, LCPremiumToken, unlimitedPurchased, companyColor, enterprise;

+(NSString *)personalKey
{
    return [BTWallet myWallet].emailAddress;
}

@end