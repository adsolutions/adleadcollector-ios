//
//  LCFolder.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 29/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCModel.h"

@interface LCFolder : LCModel

@property (copy) NSString *name;
@property (copy) NSString *hexColor;

-(NSInteger)leadsCount;
-(NSArray *)relatedLeads;

@end