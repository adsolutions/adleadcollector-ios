//
//  LCEverInsight.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 27/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCModel.h"

@interface LCEverInsight : LCModel

@property (copy) NSArray *monthsInsights;

@end

@interface LCMonthlyInsight : LCModel

@property (copy) NSString *startTimestamp;
@property (copy) NSString *endTimestamp;

@property (nonatomic) NSInteger value;

@end