//
//  LCSettings.h
//  ADLeadCollector
//
//  Created by Daniele Angeli on 11/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCModel.h"

@interface LCSettings : LCModel

@property (copy) NSString *mailChimpAPIKey;
@property (copy) NSString *mailChimpListName;
@property (copy) NSString *couchbaseURL;
@property (copy) NSString *LCPremiumToken;
@property (copy) NSString *companyColor;
@property bool enterprise;
@property bool unlimitedPurchased;

@end