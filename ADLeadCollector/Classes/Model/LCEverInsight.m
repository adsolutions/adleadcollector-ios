//
//  LCEverInsight.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 27/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCEverInsight.h"

@implementation LCEverInsight

@dynamic monthsInsights;

@end

@implementation LCMonthlyInsight

@dynamic value, startTimestamp, endTimestamp;

@end