//
//  LCAudioRecorderViewController.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 19/07/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCAudioRecorderViewController.h"
#import "LCActionMenu.h"

#import <SCSiriWaveformView/SCSiriWaveformView.h>
#import <AVFoundation/AVFoundation.h>
#import <FontAwesome+iOS/UIImage+FontAwesome.h>
#import <UIActionSheet+Blocks/UIActionSheet+Blocks.h>

@implementation NSString (TimeString)

+(NSString*)timeStringForTimeInterval:(NSTimeInterval)timeInterval
{
    NSInteger ti = (NSInteger)timeInterval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    if (hours > 0)
    {
        return [NSString stringWithFormat:@"%02li:%02li:%02li", (long)hours, (long)minutes, (long)seconds];
    }
    else
    {
        return  [NSString stringWithFormat:@"%02li:%02li", (long)minutes, (long)seconds];
    }
}

@end

@interface LCAudioRecorderViewController () <AVAudioRecorderDelegate, AVAudioPlayerDelegate>
{
    //Recording...
    AVAudioRecorder *_audioRecorder;
    
    BOOL _isRecording;
    CADisplayLink *meterUpdateDisplayLink;
    
    //Playing
    AVAudioPlayer *_audioPlayer;
    BOOL _wasPlaying;
    UIView *_viewPlayerDuration;
    UISlider *_playerSlider;
    UILabel *_labelCurrentTime;
    UILabel *_labelRemainingTime;
    CADisplayLink *playProgressDisplayLink;
    
    UIBarButtonItem *_cancelButton;
    UIBarButtonItem *_doneButton;
    
    //Toolbar
    UIBarButtonItem *_flexItem1;
    UIBarButtonItem *_flexItem2;
    UIBarButtonItem *_playButton;
    UIBarButtonItem *_pauseButton;
    UIBarButtonItem *_recordButton;
    UIBarButtonItem *_trashButton;
    
    //Private variables
    NSString *_oldSessionCategory;
    UIColor *_normalTintColor;
    UIColor *_recordingTintColor;
    UIColor *_playingTintColor;
}

@property (nonatomic, assign) BOOL shouldShowRemainingTime;

@property (nonatomic, strong) NSString *navigationBarTitle;
@property (nonatomic, strong) NSString *recordingFilePath;

@property (nonatomic, strong) UIActivityIndicatorView *activitityIndicatorView;

@property (nonatomic, strong) SCSiriWaveformView *musicFlowView;

@property (nonatomic, copy) AudioResourceLoadingBlock block;

@end

@implementation LCAudioRecorderViewController

-(id)initWithResourceRequestBlock:(AudioResourceLoadingBlock)block
{
    self = [super init];
    
    if (self) {
        self.block = [block copy];
        self.recordingFilePath = self.block(self);
    }
    
    return self;
}

-(void)loadView
{
    UIView *view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    view.backgroundColor = [UIColor darkGrayColor];
    
    _musicFlowView = [[SCSiriWaveformView alloc] initWithFrame:view.bounds];
    _musicFlowView.translatesAutoresizingMaskIntoConstraints = NO;
    [view addSubview:_musicFlowView];
    self.view = view;
    
    NSLayoutConstraint *constraintRatio = [NSLayoutConstraint constraintWithItem:_musicFlowView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:_musicFlowView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0];
    
    NSLayoutConstraint *constraintCenterX = [NSLayoutConstraint constraintWithItem:_musicFlowView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0];
    
    NSLayoutConstraint *constraintCenterY = [NSLayoutConstraint constraintWithItem:_musicFlowView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0];
    
    NSLayoutConstraint *constraintWidth = [NSLayoutConstraint constraintWithItem:_musicFlowView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0];
    [_musicFlowView addConstraint:constraintRatio];
    [view addConstraints:@[constraintWidth, constraintCenterX, constraintCenterY]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _navigationBarTitle = NSLocalizedString(@"kVoiceNote", @"");
    [self.navigationItem setTitle:_navigationBarTitle];
    
    _normalTintColor = [UIColor whiteColor];
    _recordingTintColor = FlatRed;
    _playingTintColor = companyColor();
    
    self.view.tintColor = companyColor();
    _musicFlowView.backgroundColor = [self.view backgroundColor];
    //    musicFlowView.idleAmplitude = 0;
    
    [self loadAudioFileURL];
    
    [self createAudioResource];
    
    [_musicFlowView setPrimaryWaveLineWidth:3.0f];
    [_musicFlowView setSecondaryWaveLineWidth:1.0];
    
    //Navigation Bar Settings
    self.navigationItem.title = _navigationBarTitle;
//    _cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAction:)];
//    self.navigationItem.leftBarButtonItem = _cancelButton;
    _doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(doneAction:)];
    
    //Player Duration View
    _viewPlayerDuration = [[UIView alloc] init];
    _viewPlayerDuration.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    _viewPlayerDuration.backgroundColor = [UIColor clearColor];
    
    _labelCurrentTime = [[UILabel alloc] init];
    _labelCurrentTime.text = [NSString timeStringForTimeInterval:0];
    _labelCurrentTime.font = [UIFont boldSystemFontOfSize:14.0];
    _labelCurrentTime.textColor = _normalTintColor;
    _labelCurrentTime.translatesAutoresizingMaskIntoConstraints = NO;
    
    _playerSlider = [[UISlider alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 64)];
    _playerSlider.minimumTrackTintColor = _normalTintColor;
    _playerSlider.value = 0;
    [_playerSlider addTarget:self action:@selector(sliderStart:) forControlEvents:UIControlEventTouchDown];
    [_playerSlider addTarget:self action:@selector(sliderMoved:) forControlEvents:UIControlEventValueChanged];
    [_playerSlider addTarget:self action:@selector(sliderEnd:) forControlEvents:UIControlEventTouchUpInside];
    [_playerSlider addTarget:self action:@selector(sliderEnd:) forControlEvents:UIControlEventTouchUpOutside];
    _playerSlider.translatesAutoresizingMaskIntoConstraints = NO;
    
    _labelRemainingTime = [[UILabel alloc] init];
    _labelCurrentTime.text = [NSString timeStringForTimeInterval:0];
    _labelRemainingTime.userInteractionEnabled = YES;
    [_labelRemainingTime addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecognizer:)]];
    _labelRemainingTime.font = _labelCurrentTime.font;
    _labelRemainingTime.textColor = _labelCurrentTime.textColor;
    _labelRemainingTime.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_viewPlayerDuration addSubview:_labelCurrentTime];
    [_viewPlayerDuration addSubview:_playerSlider];
    [_viewPlayerDuration addSubview:_labelRemainingTime];
    
    NSLayoutConstraint *constraintCurrentTimeLeading = [NSLayoutConstraint constraintWithItem:_labelCurrentTime attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:_viewPlayerDuration attribute:NSLayoutAttributeLeading multiplier:1 constant:10];
    NSLayoutConstraint *constraintCurrentTimeTrailing = [NSLayoutConstraint constraintWithItem:_playerSlider attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:_labelCurrentTime attribute:NSLayoutAttributeTrailing multiplier:1 constant:10];
    NSLayoutConstraint *constraintRemainingTimeLeading = [NSLayoutConstraint constraintWithItem:_labelRemainingTime attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:_playerSlider attribute:NSLayoutAttributeTrailing multiplier:1 constant:10];
    NSLayoutConstraint *constraintRemainingTimeTrailing = [NSLayoutConstraint constraintWithItem:_viewPlayerDuration attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:_labelRemainingTime attribute:NSLayoutAttributeTrailing multiplier:1 constant:10];
    
    NSLayoutConstraint *constraintCurrentTimeCenter = [NSLayoutConstraint constraintWithItem:_labelCurrentTime attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_viewPlayerDuration attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
    NSLayoutConstraint *constraintSliderCenter = [NSLayoutConstraint constraintWithItem:_playerSlider attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_viewPlayerDuration attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
    NSLayoutConstraint *constraintRemainingTimeCenter = [NSLayoutConstraint constraintWithItem:_labelRemainingTime attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_viewPlayerDuration attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
    
    [_viewPlayerDuration addConstraints:@[constraintCurrentTimeLeading, constraintCurrentTimeTrailing, constraintRemainingTimeLeading, constraintRemainingTimeTrailing, constraintCurrentTimeCenter, constraintSliderCenter, constraintRemainingTimeCenter]];
    
    _flexItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    _flexItem2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    _recordButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageWithIcon:@"icon-microphone" backgroundColor:ClearColor iconColor:_normalTintColor iconScale:1 andSize:CGSizeMake(30, 30)] style:UIBarButtonItemStylePlain target:self action:@selector(recordingButtonAction:)];
    _playButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemPlay target:self action:@selector(playAction:)];
    _pauseButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemPause target:self action:@selector(pauseAction:)];
    
    _trashButton = [[UIBarButtonItem alloc] initWithImage:[LCActionMenuImage imageWithSize:CGSizeMake(30, 30) color:[UIColor whiteColor] andType:LCActionMenuButtonTypeDelete] style:UIBarButtonItemStylePlain target:self action:@selector(deleteAction:)];
    
    _playButton.enabled = NO;
    _trashButton.enabled = NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self startUpdatingMeter];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.navigationController setToolbarHidden:NO animated:YES];
    [self updateBarItems];
    
    if (self.block) {
        // nothing to do here
    }
    else if (([self.delegate respondsToSelector:@selector(audioRecorderControllerLoadResource:)]) && ([self.delegate audioRecorderControllerLoadResource:self])) {
        [self playAction:_playButton];
    }
    else
    {
        [self recordingButtonAction:_recordButton];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    _audioPlayer.delegate = nil;
    [_audioPlayer stop];
    _audioPlayer = nil;
    
    _audioRecorder.delegate = nil;
    [_audioRecorder stop];
    _audioRecorder = nil;
    
    [self stopUpdatingMeter];
}

#pragma mark - Setters

-(void)setRecordingFilePath:(NSString *)recordingFilePath
{
    _recordingFilePath = recordingFilePath;
    
    if (self.block) {
        [self.activitityIndicatorView stopAnimating];
        [self.musicFlowView setHidden:NO];
        
        if (_recordingFilePath) {
            [self playAction:_playButton];
        }
        else
        {
            _recordingFilePath = [LCUtils tempFileForType:LCAWSManagerFileTypeM4A];
            [self recordingButtonAction:_recordButton];
        }
    }
}

#pragma mark - Update Meters

- (void)updateMeters
{
    if (_audioRecorder.isRecording)
    {
        [_audioRecorder updateMeters];
        
        CGFloat normalizedValue = pow (10, [_audioRecorder averagePowerForChannel:0] / 20);
        
        [_musicFlowView setWaveColor:_recordingTintColor];
        [_musicFlowView updateWithLevel:normalizedValue];
        
        self.navigationItem.title = [NSString timeStringForTimeInterval:_audioRecorder.currentTime];
    }
    else if (_audioPlayer.isPlaying)
    {
        [_audioPlayer updateMeters];
        
        CGFloat normalizedValue = pow (10, [_audioPlayer averagePowerForChannel:0] / 20);
        
        [_musicFlowView setWaveColor:_playingTintColor];
        [_musicFlowView updateWithLevel:normalizedValue];
        
        [self.navigationController.toolbar setTintColor:_normalTintColor];
    }
    else
    {
        [_musicFlowView setWaveColor:_normalTintColor];
        [_musicFlowView updateWithLevel:0];
    }
}

-(void)startUpdatingMeter
{
    [meterUpdateDisplayLink invalidate];
    meterUpdateDisplayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(updateMeters)];
    [meterUpdateDisplayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}

-(void)stopUpdatingMeter
{
    [meterUpdateDisplayLink invalidate];
    meterUpdateDisplayLink = nil;
}

#pragma mark - Update Play Progress

-(void)updatePlayProgress
{
    _labelCurrentTime.text = [NSString timeStringForTimeInterval:_audioPlayer.currentTime];
    _labelRemainingTime.text = [NSString timeStringForTimeInterval:(_shouldShowRemainingTime)?(_audioPlayer.duration-_audioPlayer.currentTime):_audioPlayer.duration];
    [_playerSlider setValue:_audioPlayer.currentTime animated:YES];
}

-(void)sliderStart:(UISlider*)slider
{
    _wasPlaying = _audioPlayer.isPlaying;
    
    if (_audioPlayer.isPlaying)
    {
        [_audioPlayer pause];
    }
}

-(void)sliderMoved:(UISlider*)slider
{
    _audioPlayer.currentTime = slider.value;
}

-(void)sliderEnd:(UISlider*)slider
{
    if (_wasPlaying)
    {
        [_audioPlayer play];
    }
}

-(void)tapRecognizer:(UITapGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateEnded)
    {
        _shouldShowRemainingTime = !_shouldShowRemainingTime;
    }
}

-(void)cancelAction:(UIBarButtonItem *)item
{
    if ([self.delegate respondsToSelector:@selector(audioRecorderControllerDidCancel:)])
    {
        [self.delegate audioRecorderControllerDidCancel:self];
    }
}

-(void)doneAction:(UIBarButtonItem *)item
{
    if ([self.delegate respondsToSelector:@selector(audioRecorderController:didFinishWithAudioAtPath:)])
    {
        [self.delegate audioRecorderController:self didFinishWithAudioAtPath:_recordingFilePath];
    }
}

- (void)recordingButtonAction:(UIBarButtonItem *)item
{
    if (_isRecording == NO)
    {
        if ([[NSFileManager defaultManager] fileExistsAtPath:_recordingFilePath])
        {
            __block typeof(self) me = self;
            
            [UIActionSheet showInView:self.view withTitle:nil cancelButtonTitle:NSLocalizedString(@"kCancel", @"") destructiveButtonTitle:NSLocalizedString(@"kDeleteRecording", @"") otherButtonTitles:nil tapBlock:^(UIActionSheet * __nonnull actionSheet, NSInteger buttonIndex) {
                if (buttonIndex == actionSheet.destructiveButtonIndex) {
                    if ([me.delegate respondsToSelector:@selector(audioRecorderController:performDeletionOfAudioAtPath:)]) {
                        [me.delegate audioRecorderController:me performDeletionOfAudioAtPath:_recordingFilePath];
                    }
                    
                    [me startRecording];
                }
            }];
        }
        else
        {
            [self startRecording];
        }
    }
    else
    {
        _isRecording = NO;
        
        //UI Update
        [self showNavigationButton:YES];
        [self.navigationController.toolbar setTintColor:_normalTintColor];
        _playButton.enabled = YES;
        _trashButton.enabled = YES;
        
        [_audioRecorder stop];
        [[AVAudioSession sharedInstance] setCategory:_oldSessionCategory error:nil];
    }
    
    [self updateBarItems];
}

- (void)playAction:(UIBarButtonItem *)item
{
    _oldSessionCategory = [[AVAudioSession sharedInstance] category];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:_recordingFilePath] error:nil];
    _audioPlayer.delegate = self;
    _audioPlayer.meteringEnabled = YES;
    [_audioPlayer prepareToPlay];
    [_audioPlayer play];
    
    [self setToolbarItems:@[_pauseButton, _flexItem1, _recordButton, _flexItem2, _trashButton] animated:YES];
    
    //UI Update
    [self showNavigationButton:NO];
    _recordButton.enabled = NO;
    _trashButton.enabled = NO;
    
    //Start regular update
    _playerSlider.value = _audioPlayer.currentTime;
    _playerSlider.maximumValue = _audioPlayer.duration;
    _viewPlayerDuration.frame = self.navigationController.navigationBar.bounds;
    
    _labelCurrentTime.text = [NSString timeStringForTimeInterval:_audioPlayer.currentTime];
    _labelRemainingTime.text = [NSString timeStringForTimeInterval:(_shouldShowRemainingTime)?(_audioPlayer.duration-_audioPlayer.currentTime):_audioPlayer.duration];
    
    [_viewPlayerDuration setNeedsLayout];
    [_viewPlayerDuration layoutIfNeeded];
    self.navigationItem.titleView = _viewPlayerDuration;
    
    [playProgressDisplayLink invalidate];
    playProgressDisplayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(updatePlayProgress)];
    [playProgressDisplayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}

-(void)pauseAction:(UIBarButtonItem *)item
{
    //UI Update
    [self setToolbarItems:@[_playButton, _flexItem1, _recordButton, _flexItem2, _trashButton] animated:YES];
    
    [self showNavigationButton:YES];
    _recordButton.enabled = YES;
    _trashButton.enabled = YES;
    _playButton.enabled = YES;
    
    [playProgressDisplayLink invalidate];
    playProgressDisplayLink = nil;
    self.navigationItem.titleView = nil;
    
    _audioPlayer.delegate = nil;
    [_audioPlayer stop];
    _audioPlayer = nil;
    
    [[AVAudioSession sharedInstance] setCategory:_oldSessionCategory error:nil];
}

-(void)updateBarItems
{
    [self setToolbarItems:@[_playButton, _flexItem1, _recordButton, _flexItem2, _trashButton] animated:YES];
}

-(void)deleteAction:(UIBarButtonItem *)item
{
    __block typeof(self) me = self;
    
    [UIActionSheet showInView:self.view withTitle:nil cancelButtonTitle:NSLocalizedString(@"kCancel", @"") destructiveButtonTitle:NSLocalizedString(@"kDeleteRecording", @"") otherButtonTitles:nil tapBlock:^(UIActionSheet * __nonnull actionSheet, NSInteger buttonIndex) {
        if (buttonIndex == actionSheet.destructiveButtonIndex) {
            if ([me.delegate respondsToSelector:@selector(audioRecorderController:performDeletionOfAudioAtPath:)]) {
                [me.delegate audioRecorderController:me performDeletionOfAudioAtPath:[self.recordingFilePath copy]];
            }
            
            _playButton.enabled = NO;
            _trashButton.enabled = NO;
            [me.navigationItem setRightBarButtonItem:nil animated:YES];
            me.navigationItem.title = _navigationBarTitle;
            
            [me loadAudioFileURL];
            [me createAudioResource];
        }
    }];
}

-(void)showNavigationButton:(BOOL)show
{
    if (show)
    {
        [self.navigationItem setLeftBarButtonItem:_cancelButton animated:YES];
        [self.navigationItem setRightBarButtonItem:_doneButton animated:YES];
    }
    else
    {
        [self.navigationItem setLeftBarButtonItem:nil animated:YES];
        [self.navigationItem setRightBarButtonItem:nil animated:YES];
    }
}

-(void)startRecording
{
    _oldSessionCategory = [[AVAudioSession sharedInstance] category];
    
    __block typeof(self) me = self;
    
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (granted) {
            _isRecording = YES;
            
            //UI Update
            [me showNavigationButton:NO];
            [me.navigationController.toolbar setTintColor:_recordingTintColor];
            _playButton.enabled = NO;
            _trashButton.enabled = NO;
            
            [[NSFileManager defaultManager] removeItemAtPath:_recordingFilePath error:nil];
            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryRecord error:nil];
            [_audioRecorder prepareToRecord];
            [_audioRecorder record];
        }
        else
        {
            NSString *message = [NSString stringWithFormat:@"%@\n%@\n%@\n%@\n%@", NSLocalizedString(@"LCAudioRecorderViewController-Subtitle", @""), NSLocalizedString(@"LCAudioRecorderViewController-Step1", @""), NSLocalizedString(@"LCAudioRecorderViewController-Step2", @""), NSLocalizedString(@"LCAudioRecorderViewController-Step3", @""), NSLocalizedString(@"LCAudioRecorderViewController-Step4", @"")];
            
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"LCAudioRecorderViewController-Title", @"") message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }];
}

#pragma mark - Audio Retrival

-(void)createAudioResource
{
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt:2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    _audioRecorder = [[AVAudioRecorder alloc] initWithURL:[NSURL fileURLWithPath:_recordingFilePath] settings:recordSetting error:nil];
    _audioRecorder.delegate = self;
    _audioRecorder.meteringEnabled = YES;
}

-(void)loadAudioFileURL
{
    NSString *loadedURL = nil;
    if (self.block) {
        // prevent synchronous load
        [_musicFlowView setHidden:YES];
        _activitityIndicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
        [_activitityIndicatorView setCenter:_musicFlowView.center];
        [_activitityIndicatorView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [_activitityIndicatorView setHidesWhenStopped:YES];
        [_activitityIndicatorView startAnimating];
    }
    else if ([self.delegate respondsToSelector:@selector(audioRecorderControllerLoadResource:)])
    {
        loadedURL = [self.delegate audioRecorderControllerLoadResource:self];
        _recordingFilePath = loadedURL? loadedURL : [LCUtils tempFileForType:LCAWSManagerFileTypeM4A];
    }
    else
    {
        _recordingFilePath = [LCUtils tempFileForType:LCAWSManagerFileTypeM4A];
    }
}

#pragma mark - AVAudioPlayerDelegate

/*
 Occurs when the audio player instance completes playback
 */
-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    //To update UI on stop playing
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[_pauseButton.target methodSignatureForSelector:_pauseButton.action]];
    invocation.target = _pauseButton.target;
    invocation.selector = _pauseButton.action;
    [invocation invoke];
}

-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    CLS_LOG(@"%@: %@", NSStringFromSelector(_cmd), error);
}

#pragma mark - AVAudioRecorderDelegate

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{
    
}

- (void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError *)error
{
    CLS_LOG(@"%@: %@", NSStringFromSelector(_cmd), error);
}

@end