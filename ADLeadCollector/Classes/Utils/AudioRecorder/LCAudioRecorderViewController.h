//
//  LCAudioRecorderViewController.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 19/07/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LCAudioRecorderViewController;

typedef NSString* (^AudioResourceLoadingBlock)(LCAudioRecorderViewController *controller);

@protocol LCAudioRecorderDelegate <NSObject>

-(void)audioRecorderController:(LCAudioRecorderViewController *)controller didFinishWithAudioAtPath:(NSString *)audioPath;
-(void)audioRecorderControllerDidCancel:(LCAudioRecorderViewController *)controller;
-(void)audioRecorderController:(LCAudioRecorderViewController *)controller performDeletionOfAudioAtPath:(NSString *)audioPath;

-(NSString *)audioRecorderControllerLoadResource:(LCAudioRecorderViewController *)controller;

@end

@interface LCAudioRecorderViewController : UIViewController

-(id)initWithResourceRequestBlock:(AudioResourceLoadingBlock)block;

@property (nonatomic, unsafe_unretained) id <LCAudioRecorderDelegate> delegate;

@end