//
//  LCLabel.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 17/08/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCLabel : UILabel

@property (nonatomic, assign) UIEdgeInsets edgeInsets;

@end