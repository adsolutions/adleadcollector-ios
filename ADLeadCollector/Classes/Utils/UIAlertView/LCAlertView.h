//
//  LCAlertView.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 25/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCAlertView : UIAlertView

@property (nonatomic, strong) NSIndexPath *indexPath;

@end
