//
//  LCAlwaysTemplateImage.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 29/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCAlwaysTemplateImage : UIImage

+ (UIImage *)alwaysTemplateImageNamed:(NSString *)name;
+ (UIImage *)alwaysTemplateImageWithImage:(UIImage *)image;

-(void)setTintColor:(UIColor *)color;

@end
