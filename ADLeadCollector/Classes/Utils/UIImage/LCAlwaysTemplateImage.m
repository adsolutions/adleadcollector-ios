//
//  ITAlwaysTemplateImage.m
//  TeamSales
//
//  Created by Daniele Angeli on 18/12/13.
//  Copyright (c) 2013 Info-Team. All rights reserved.
//

#import "LCAlwaysTemplateImage.h"

@implementation LCAlwaysTemplateImage
{
    UIColor *_tintColor;
}

+ (UIImage *)alwaysTemplateImageNamed:(NSString *)name
{
    UIImage *image = [super imageNamed:name];
    return [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

+ (UIImage *)alwaysTemplateImageWithImage:(UIImage *)image
{
    return [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

-(void)setTintColor:(UIColor *)color
{
    _tintColor = color;
    [self drawRect:CGRectMake(0, 0, self.size.width, self.size.height)];
}

- (void)drawRect:(CGRect)area
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    
    // Draw picture first
    //
    CGContextDrawImage(context, area, self.CGImage);
    
    // Blend mode could be any of CGBlendMode values. Now draw filled rectangle
    // over top of image.
    //
    CGContextSetBlendMode (context, kCGBlendModeMultiply);
    CGContextSetFillColor(context, CGColorGetComponents(_tintColor.CGColor));
    CGContextFillRect(context, area);
    CGContextRestoreGState(context);
}

@end