//
//  LCUtils.h
//  ADLeadCollector
//
//  Created by Daniele Angeli on 11/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCAWSManager.h"

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <FRDLivelyButton/FRDLivelyButton.h>
#import <CRToast/CRToast.h>

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define kLCUnlimitedPurchaseID @"it.aditsolutions.LeadsCollector.Unlimited"

#define DEGREES_TO_RADIANS(angle) (angle /180.0 *M_PI)
#define M_PI 3.14159265358979323846264338327950288  /* pigreco */

#define kUserDefaults [NSUserDefaults standardUserDefaults]

#define FlatGold [UIColor colorWithHexString:@"FFD700"]

@class LCFolder;

extern NSString *const kDefaultServerDbURL;
extern NSString *const kDefaultDatabaseName;
extern NSString *const kDefaultCompany;
extern NSString *const kCachedAddressesKey;
extern NSString *const kCachedUsernamesKey;
extern NSString *const kUpdateNotificationKey;
extern NSString *const AWSBucketName;
extern NSString *const AWSBucketEndPoint;
extern NSString *const AWSVoiceNotesPath;
extern NSString *const AWSPicturesPath;
extern NSString *const AWSIdentityPoolID;
extern NSString *const LCAWSFilesDirectory;
extern NSString *const kUnassigned;
extern NSString *const kSyncGatewayUsername;
extern NSString *const kSyncGatewayPassword;

UIKIT_EXTERN UIColor *companyColor();

@interface LCUtils : NSObject

+(void)applyStyle;

+(NSUInteger)currentVersion;

+(NSString *)hexStringFromColor:(UIColor *)color;

+(UIFont *)fontWithSize:(CGFloat)size;
+(UIFont *)boldFontWithSize:(CGFloat)size;

+(FRDLivelyButton *)livelyButtonWithFrame:(CGRect)frame forType:(kFRDLivelyButtonStyle)type target:(id)target andAction:(SEL)selector;

+(void)dataMigrationToFolders;
+(void)purgeLeadsFromFolder:(LCFolder *)folder;
+(void)migrateAllLeadsToChannel:(NSString *)channelID;
+(void)purgeAll;
+(BOOL)addCompany:(NSString *)companyID toForm:(NSString *)formID;
+(void)addPeople:(NSInteger)amoutOfPeople toFolder:(LCFolder *)folder;

+(BOOL)validateUrl:(NSString *)candidate;

+(NSString *)privacyPolicyForCompany:(NSString *)companyEmail;

+(BOOL)needsPremium;
+(BOOL)isPremium;
+(BOOL)isUnlimited;
+(BOOL)isEnterprise;

+(NSDictionary *)errorToastWithTitle:(NSString *)title andMessage:(NSString *)message;
+(NSDictionary *)successToastWithTitle:(NSString *)title andMessage:(NSString *)message;
+(NSDictionary *)infoToastWithMessage:(NSString *)message andTimeout:(NSInteger)timeout;

+(UIBezierPath *)lcSplashBezier;

+(NSDictionary *)answersUserDetails;

+(NSString *)tempFileForType:(LCAWSManagerFileType)type;

+(NSString *)defaultCompanyID;
+(NSString *)defaultCompanyName;

@end