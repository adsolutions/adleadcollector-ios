//
//  LCBadgeView.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 13/08/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCBadgeView.h"
#import "LCAlwaysTemplateImage.h"
#import "LCLabel.h"

#import <PureLayout/PureLayout.h>

@interface LCBadgeView ()

@property (nonatomic, strong) LCLabel *label;
@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation LCBadgeView

-(id)init
{
    self = [super init];
    
    if (self) {
        [self draw];
    }
    
    return self;
}

-(void)layoutSubviews
{
    [self draw];
    [super layoutSubviews];
}

-(void)setNeedsDisplay
{
    [self draw];
    [super setNeedsDisplay];
}

-(void)draw
{
    [_label removeFromSuperview];
    _label = nil;
    
    [_imageView removeFromSuperview];
    _imageView = nil;
    
    _textAlignment = LCContentAlignmentRight;
    
    _label = ({
        LCLabel *label = [[LCLabel alloc] initForAutoLayout];
        [self addSubview:label];
        [label autoPinEdge:(_textAlignment == LCContentAlignmentLeft? ALEdgeLeft : ALEdgeRight) toEdge:(_textAlignment == LCContentAlignmentLeft? ALEdgeLeft : ALEdgeRight) ofView:self withOffset:(_textAlignment == LCContentAlignmentLeft? 5 : -5)];
        [label autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self];
        [label autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self];
        [label setEdgeInsets:_textInsets];
        [label setFont:_font];
        [label setText:_text];
        [label setTextColor:_textColor];
        [label setBackgroundColor:ClearColor];
        label;
    });
    
    _imageAligment = LCContentAlignmentLeft;
    
    _imageView = ({
        UIImageView *image = [[UIImageView alloc] initForAutoLayout];
        [self addSubview:image];
        [image autoPinEdge:(_imageAligment == LCContentAlignmentLeft? ALEdgeLeft : ALEdgeRight) toEdge:(_imageAligment == LCContentAlignmentLeft? ALEdgeLeft : ALEdgeRight) ofView:self withOffset:(_imageAligment == LCContentAlignmentLeft? 3 : -3)];
        [image autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self withOffset:_imageInsets.top];
        [image autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self withOffset:_imageInsets.bottom];
        [image autoPinEdge:(_imageAligment == LCContentAlignmentLeft? ALEdgeRight : ALEdgeLeft) toEdge:(_imageAligment == LCContentAlignmentLeft? ALEdgeLeft : ALEdgeRight) ofView:_label withOffset:(_imageAligment == LCContentAlignmentLeft? -5 : 5)];
        [image autoSetDimensionsToSize:_imageSize];
        [image setTintColor:_textColor];
        [image setImage:[LCAlwaysTemplateImage alwaysTemplateImageWithImage:_image]];
        [image setContentMode:UIViewContentModeScaleAspectFit];
        [image setBackgroundColor:ClearColor];
        image;
    });
    
    [self.layer setCornerRadius:_borderRadius];
    [self.layer setMasksToBounds:YES];
}

#pragma mark - Setters

-(void)setText:(NSString *)text
{
    _text = text;
    [_label setText:text];
}

-(void)setTextInsets:(UIEdgeInsets)textInsets
{
    _textInsets = textInsets;
}

-(void)setImageInsets:(UIEdgeInsets)imageInsets
{
    _imageInsets = imageInsets;
}

-(void)setImage:(UIImage *)image
{
    _image = image;
    [_imageView setImage:image];
}

-(void)setFont:(UIFont *)font
{
    _font = font;
}

-(void)setTextColor:(UIColor *)textColor
{
    _textColor = textColor;
}

-(void)setBorderRadius:(CGFloat)borderRadius
{
    _borderRadius = borderRadius;
}

-(void)setImageSize:(CGSize)imageSize
{
    _imageSize = imageSize;
}

@end