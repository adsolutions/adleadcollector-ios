//
//  LCBadgeView.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 13/08/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, LCContentAlignment) {
    LCContentAlignmentLeft = 0,
    LCContentAlignmentRight = 1
};

@interface LCBadgeView : UIView

@property (nonatomic, copy) UIImage *image;
@property (nonatomic) CGSize imageSize;
@property (nonatomic) UIEdgeInsets imageInsets;

@property (nonatomic, copy) NSString *text;
@property (nonatomic) UIEdgeInsets textInsets;
@property (nonatomic, copy) UIColor *textColor;
@property (nonatomic, copy) UIFont *font;

@property (nonatomic) LCContentAlignment imageAligment __unavailable;
@property (nonatomic) LCContentAlignment textAlignment __unavailable;

@property (nonatomic) CGFloat borderRadius;

@end