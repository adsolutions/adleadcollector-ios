//
//  LCTableViewController.m
//  ADLeadCollector
//
//  Created by Daniele Angeli on 11/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCCollectionViewController.h"
#import "LCAlwaysTemplateImage.h"

#import <UIColor-HexString/UIColor+HexString.h>
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface LCCollectionViewController () <DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>

@end

@implementation LCCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
    
    //    self.refreshControl = [[UIRefreshControl alloc] init];
    //    [self.refreshControl addTarget:self action:@selector(reloadData) forControlEvents:UIControlEventValueChanged];
    //    self.refreshControl.backgroundColor = [UIColor colorWithHexString:@"f1f4f4"];
    //    self.refreshControl.tintColor = companyColor();
    
    [self.collectionView setEmptyDataSetDelegate:self];
    [self.collectionView setEmptyDataSetSource:self];
    
    [self.view setTintColor:companyColor()];
        
    self.collectionView.contentOffset = CGPointMake(0, self.searchDisplayController.searchBar.frame.size.height);
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kUpdateNotificationKey object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {
         [self performSelectorOnMainThread:@selector(mobileBridgeDidUpdateDataWithNotification:) withObject:x waitUntilDone:NO];
     }];
}

#pragma mark - Actions

- (void)reloadData
{
    // Reload table data
    [self.collectionView reloadData];
    
    //    // End the refreshing
    //    if (self.refreshControl)
    //    {
    //        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //        [formatter setDateFormat:@"d MMM, h:mm a"];
    //        NSString *title = [NSString stringWithFormat:NSLocalizedString(@"kLastUpdate%@", @"LCTableViewController"), [formatter stringFromDate:[NSDate date]]];
    //        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName : companyColor(), NSFontAttributeName : [UIFont systemFontOfSize:20]}];
    //        self.refreshControl.attributedTitle = attributedTitle;
    //
    //        [self.refreshControl endRefreshing];
    //    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 0;
}

#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = NSLocalizedString(@"kNoCollectionsAvailable", @"TableView");
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [LCUtils boldFontWithSize:17.0],
                                 NSForegroundColorAttributeName: FlatGray,
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [LCUtils fontWithSize:15.0],
                                 NSForegroundColorAttributeName: FlatGray,
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state
{
    return nil;
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    [imageView setImage:[UIImage imageNamed:@"LeadsCollectorWhite"]];
    return imageView.image;
}

- (UIColor *)imageTintColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return FlatGray;
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}

#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return YES;
}

- (void)emptyDataSetWillAppear:(UIScrollView *)scrollView
{
    //
}

-(void)emptyDataSetWillDisappear:(UIScrollView *)scrollView
{
    //
}

#pragma mark - Notification

-(void)mobileBridgeDidUpdateDataWithNotification:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperation:[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(reloadData) object:nil]];
}

@end