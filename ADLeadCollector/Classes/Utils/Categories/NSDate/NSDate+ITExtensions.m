//
//  NSDate+Extras.m
//  iMemos
//
//  Created by Daniele Angeli on 02/04/12.
//  Copyright (c) 2012 Apimac. All rights reserved.
//

#import "NSDate+ITExtensions.h"

@implementation NSDate (ICExtesions)

-(NSString *)dateWithoutTimeWithFormat:(NSString *)format
{
    if (!format || [format isEqualToString:@""])
    {
        format = @"EEE dd MMM yyyy";
    }
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                               fromDate:self];
    NSDate *dateOnly = [calendar dateFromComponents:components];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
	[outputFormatter setDateFormat:format];
    
    return [outputFormatter stringFromDate:dateOnly];
}

-(NSString *)timeComponentsFromDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
	NSDateComponents *timeComponents = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:self];
    
	NSDate *newDate = [calendar dateFromComponents:timeComponents];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm a"];
    
    return [outputFormatter stringFromDate:newDate];
}

-(NSDate *)dateWithoutTimeComponets
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = nil;
    components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:self];
    
    return [calendar dateFromComponents:components];
}

-(NSString *)monthName
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitMonth fromDate:self];
    
    NSDate *monthOnly = [calendar dateFromComponents:components];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
	[outputFormatter setDateFormat:@"MMM"];
    
    return [outputFormatter stringFromDate:monthOnly];
}

-(NSString *)dayNumber
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:self];
    
    NSDate *dayOnly = [calendar dateFromComponents:components];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
	[outputFormatter setDateFormat:@"dd"];
    
    return [outputFormatter stringFromDate:dayOnly];
}

-(NSString *)dayName
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:self];
    
    NSDate *dayOnly = [calendar dateFromComponents:components];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"EEE"];
    
    return [outputFormatter stringFromDate:dayOnly];
}

-(NSString *)yearComponent
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear fromDate:self];
    
    NSDate *yearOnly = [calendar dateFromComponents:components];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"YYYY"];
    
    return [outputFormatter stringFromDate:yearOnly];
}

@end