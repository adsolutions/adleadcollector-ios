//
//  NSDate+Extras.h
//  iMemos
//
//  Created by Daniele Angeli on 02/04/12.
//  Copyright (c) 2012 Apimac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (ICExtesions)

-(NSString *)dateWithoutTimeWithFormat:(NSString *)format;
-(NSString *)timeComponentsFromDate;
-(NSDate *)dateWithoutTimeComponets;

-(NSString *)monthName;
-(NSString *)dayNumber;
-(NSString *)dayName;
-(NSString *)yearComponent;

@end