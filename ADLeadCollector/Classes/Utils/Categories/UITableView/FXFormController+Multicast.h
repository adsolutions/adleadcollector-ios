//
//  UITableView+Multicast.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 05/07/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "SHCMulticastDelegate.h"

#import <FXForms/FXForms.h>

@interface FXFormController (MultiCast)

@property (readonly) SHCMulticastDelegate *multicastDelegate;

@end