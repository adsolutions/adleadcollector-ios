//
//  UITableView+Multicast.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 05/07/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "FXFormController+Multicast.h"

#import <objc/runtime.h>

@implementation FXFormController (MultiCast)

NSString* const FXFormControllerMulticastDelegateKey = @"multicastDelegate";

- (SHCMulticastDelegate *)multicastDelegate
{
    // do we have a SHCMulticastDelegate associated with this class?
    id multicastDelegate = objc_getAssociatedObject(self, (__bridge const void *)(FXFormControllerMulticastDelegateKey));
    if (multicastDelegate == nil) {
        
        // if not, create one
        multicastDelegate = [[SHCMulticastDelegate alloc] init];
        objc_setAssociatedObject(self, (__bridge const void *)(FXFormControllerMulticastDelegateKey), multicastDelegate, OBJC_ASSOCIATION_RETAIN);
        
        // and set it as the delegate
        self.delegate = multicastDelegate;
    }
    
    return multicastDelegate;
}

@end