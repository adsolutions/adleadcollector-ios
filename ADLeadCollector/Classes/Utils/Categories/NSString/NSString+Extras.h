//
//  NSString+Extras.h
//  iMemos
//
//  Created by Daniele Angeli on 27/02/12.
//  Copyright (c) 2012 Angel IT DEV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extras)

-(NSString *)dateStringWithoutSeconds;
-(NSDate *)dateWithoutSeconds;
-(NSComparisonResult)compareDate:(NSString *)date;

+(BOOL)isEmptyString:(NSString *)string;

-(NSString *)escapedStringWithLenght:(NSInteger)lenght;

+(NSString *)longEscapedStringFromString:(NSString *)string;
+(NSString *)shortEscapedStringFromString:(NSString *)string;
+(NSInteger)linesInString:(NSString *)string;

@end