//
//  NSString+Extras.m
//  iMemos
//
//  Created by Daniele Angeli on 27/02/12.
//  Copyright (c) 2012 Angel IT DEV. All rights reserved.
//

#import "NSString+Extras.h"

NSInteger const FixedLenght = 10;

@implementation NSString (Extras)

#pragma mark - compare Subclass

-(NSComparisonResult)compareDate:(NSString *)date
{
    NSDate *firstDateFromString = [date dateWithoutSeconds];
    NSDate *secondDateFromString = [self dateWithoutSeconds];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [calendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSDateComponents *components = nil;
    
    components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                             fromDate:firstDateFromString];
    
    NSDate *firstDate = [calendar dateFromComponents:components];
    
    components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                             fromDate:secondDateFromString];
    
    NSDate *secondDate = [calendar dateFromComponents:components];
    
    firstDateFromString = nil;
    secondDateFromString = nil;
    calendar = nil;
    components = nil;
    
    return [firstDate compare:secondDate];
}

-(NSString *)dateStringWithoutSeconds
{
    //data senza secondi
    NSDateFormatter *dataSenzaSecondi = [[NSDateFormatter alloc] init];
    [dataSenzaSecondi setDateStyle:NSDateFormatterLongStyle];
    [dataSenzaSecondi setTimeStyle:NSDateFormatterShortStyle];
    
    NSDate *date = [dataSenzaSecondi dateFromString:self];
    
    return [dataSenzaSecondi stringFromDate:date];
}

-(NSDate *)dateWithoutSeconds
{
    //data senza secondi
    NSDateFormatter *dataSenzaSecondi = [[NSDateFormatter alloc] init];
    [dataSenzaSecondi setDateStyle:NSDateFormatterLongStyle];
    [dataSenzaSecondi setTimeStyle:NSDateFormatterShortStyle];
    
    return [dataSenzaSecondi dateFromString:self];
}

#pragma mark - EmptyString

+(BOOL)isEmptyString:(NSString *)string
{
    if ((!string) || ([string length] == 0) || ((id)string == [NSNull null]) || ([string isEqualToString:@""]))
    {
        return YES;
    }
    
    return NO;
}

#pragma mark - NSStringUtils

-(NSString *)escapedStringWithLenght:(NSInteger)lenght
{
    return [NSString escapedStringFromString:self withLenght:lenght];
}

+(NSString *)longEscapedStringFromString:(NSString *)string
{
    return [self escapedStringFromString:string withLenght:FixedLenght*4];
}

+(NSString *)shortEscapedStringFromString:(NSString *)string
{
    return [self escapedStringFromString:string withLenght:FixedLenght];
}

+(NSString *)escapedStringFromString:(NSString *)string withLenght:(NSInteger)lenght
{
    if ([string length] > lenght)
    {
        int count = 0;
        int lastCount = 0;
        
        NSString *newString = @"";
        
        while (count < [string length])
        {
            if ((count != 0) && (count%lenght == 0))
            {
                NSRange range = NSMakeRange(lastCount, lenght);
                NSString *initialString = [string substringWithRange:range];
                
                newString = [newString stringByAppendingFormat:@"%@\n", initialString];
                lastCount = count;
            }
            
            count++;
        }
    
        return [newString stringByAppendingString:[string substringFromIndex:(lastCount)]];
    }
    
    return string;
}

+(NSInteger)linesInString:(NSString *)string
{
    NSInteger count = [[string componentsSeparatedByString:@"\n"] count];
    return count == 0? 1 : count;
}

@end