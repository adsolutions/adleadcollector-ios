//
//  TFNavigationController.h
//  TeamFactory
//
//  Created by Daniele Angeli on 08/04/15.
//  Copyright (c) 2015 Info-Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCNavigationController : UINavigationController

@end