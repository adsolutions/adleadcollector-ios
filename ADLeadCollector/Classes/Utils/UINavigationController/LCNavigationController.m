//
//  TFNavigationController.m
//  TeamFactory
//
//  Created by Daniele Angeli on 08/04/15.
//  Copyright (c) 2015 Info-Team. All rights reserved.
//

#import "LCNavigationController.h"

@interface LCNavigationController () <UINavigationControllerDelegate>

@end

@implementation LCNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationBar setBarTintColor:companyColor()];
    [self.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [LCUtils boldFontWithSize:19]}];
    
    [self.toolbar setTintColor:[UIColor whiteColor]];
    [self.toolbar setBarTintColor:companyColor()];
    
    [self setNeedsStatusBarAppearanceUpdate];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end