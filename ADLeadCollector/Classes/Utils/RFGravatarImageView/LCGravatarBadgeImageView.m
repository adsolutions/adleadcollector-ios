//
//  LCGravatarBadgeImageView.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 05/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCGravatarBadgeImageView.h"

#import <NSString+Validation/NSString+Validation.h>

@interface LCGravatarBadgeImageView ()

@property (nonatomic, strong) RFGravatarImageView *gravatarImageView;

@end

@implementation LCGravatarBadgeImageView

-(id)init
{
    self = [super init];
    
    if (self) {
        _gravatarImageView = [[RFGravatarImageView alloc] initForAutoLayout];
        [self addSubview:_gravatarImageView];
        [_gravatarImageView autoCenterInSuperview];

        [_gravatarImageView setDefaultGravatar:RFDefaultGravatarMysteryMan];
        
        [self setClipsToBounds:YES];
    }
    
    return self;
}

-(void)awakeFromNib
{
    _gravatarImageView = [[RFGravatarImageView alloc] initForAutoLayout];
    [self addSubview:_gravatarImageView];
    [_gravatarImageView autoCenterInSuperview];

    [_gravatarImageView setDefaultGravatar:RFDefaultGravatarMysteryMan];
    
    [self setClipsToBounds:YES];
}

-(void)layoutSubviews
{
    CGFloat dimension = (self.frame.size.width+self.frame.size.width/4);
    [_gravatarImageView setSize:dimension*[UIScreen mainScreen].scale];
    [_gravatarImageView autoSetDimensionsToSize:CGSizeMake(dimension, dimension)];
    [self.layer setCornerRadius:self.frame.size.width/2];
    [self.layer setBorderColor:_circleColor? _circleColor.CGColor : [UIColor whiteColor].CGColor];
    [self.layer setBorderWidth:2];
}

-(void)setCircleColor:(UIColor *)circleColor
{
    _circleColor = circleColor;
    [self.layer setBorderColor:_circleColor? _circleColor.CGColor : [UIColor whiteColor].CGColor];
}

-(void)refresh
{
    if (![NSString isEmptyString:_email]) {
        [_gravatarImageView refresh];
    }
}

#pragma mark - Setters

-(void)setEmail:(NSString *)email
{
    if (![NSString isEmptyString:email]) {
        [_gravatarImageView setForceDefault:NO];
        
        [_gravatarImageView setEmail:email];
        [_gravatarImageView load];
    }
    else
    {
        [_gravatarImageView setForceDefault:YES];
    }
}

@end