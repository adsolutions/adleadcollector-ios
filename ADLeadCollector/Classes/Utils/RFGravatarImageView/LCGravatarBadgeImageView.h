//
//  LCGravatarBadgeImageView.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 05/06/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "RFGravatarImageView.h"

@interface LCGravatarBadgeImageView : UIButton

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) UIColor *circleColor;

-(void)refresh;

@end