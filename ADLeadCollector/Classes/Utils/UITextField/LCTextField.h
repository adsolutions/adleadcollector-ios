//
//  LCTextField.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 15/05/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCTextField : UITextField

@property (nonatomic) BOOL validated;

@end
