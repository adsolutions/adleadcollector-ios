//
//  ISO3166CountryValueTransformer.m
//  BasicExample
//
//  Created by Bart Vandendriessche on 17/03/14.
//  Copyright (c) 2014 Charcoal Design. All rights reserved.
//

#import "ISO8601DateValueTransformer.h"
#import "NSDate+Extras.h"

@implementation ISO8601DateValueTransformer

+ (Class)transformedValueClass
{
    return [NSDate class];
}

+ (BOOL)allowsReverseTransformation
{
    return YES;
}

- (id)transformedValue:(id)value
{
    if ([value isKindOfClass:[NSString class]]) {
        value = [NSDate dateFromString:value withFormat:@"YYYY-MM-dd"];
    }
    
    NSString *date = [NSDate stringFromDate:value withFormat:@"YYYY-MM-dd"];
    return date? date : nil;
}

- (id)reverseTransformedValue:(id)value
{
    return value;
}

@end