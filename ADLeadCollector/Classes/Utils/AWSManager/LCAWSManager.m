//
//  LCAWSManager.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 20/07/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCAWSManager.h"

#import <AWSS3/AWSS3.h>
#import <FCFileManager/FCFileManager.h>
#import <NSObject-KJSerializer/NSObject+KJSerializer.h>
#import <NSString+Validation/NSString+Validation.h>

NSString *const kObjectsKey = @"it.aditsolutions.LeadsCollector.objects";

@implementation LCAWSManager (Cache)

#pragma mark - Static Methods

+(BOOL)wipeCache
{
    [[AWSS3TransferManager defaultS3TransferManager] cancelAll];
    [[AWSS3TransferManager defaultS3TransferManager] clearCache];
    [LCAWSFailedUpload setObjects:nil];
    
    return [FCFileManager emptyCachesDirectory];
}

#pragma mark - Instance Methods

-(NSData *)fileForName:(NSString *)name andType:(LCAWSManagerFileType)type
{
    if ([NSString isEmptyString:name]) {
        return nil;
    }
    
    NSString *filePath = [self filePathForName:name andType:type];
    if ([FCFileManager existsItemAtPath:filePath]) {
        return [NSData dataWithContentsOfFile:filePath];
    }
    
    return nil;
}

-(NSString *)storeFileInCache:(NSData *)fileData forName:(NSString *)name andType:(LCAWSManagerFileType)type
{
    if ([NSString isEmptyString:name] || !fileData) {
        return nil;
    }
    
    NSString *filePath = [self filePathForName:name andType:type];
    [FCFileManager writeFileAtPath:filePath content:fileData];
    
    return filePath;
}

-(BOOL)deleteFileFromCache:(NSString *)name withType:(LCAWSManagerFileType)type
{
    if ([NSString isEmptyString:name]) {
        return YES;
    }
    
    NSString *filePath = [self filePathForName:name andType:type];
    if ([FCFileManager existsItemAtPath:filePath]) {
        [FCFileManager removeItemAtPath:filePath];
    }
    
    return ![FCFileManager existsItemAtPath:filePath];
}

-(NSString *)filesPath
{
    NSString *path = [FCFileManager pathForCachesDirectoryWithPath:LCAWSFilesDirectory];
    if (![FCFileManager isDirectoryItemAtPath:path]) {
        [FCFileManager createDirectoriesForPath:[[FCFileManager pathForCachesDirectory] stringByAppendingPathComponent:LCAWSFilesDirectory]];
    }
    
    return path;
}

-(NSString *)filePathForName:(NSString *)name andType:(LCAWSManagerFileType)type
{
    return [[self filesPath] stringByAppendingPathComponent:[name stringByAppendingString:[self fileExtensionFromType:type]]];
}

@end

#pragma mark - LCAWSObject(s)

@interface LCAWSObject ()

@end

@implementation LCAWSObject

-(void)store
{
    if ([NSString isEmptyString:self.fileName]) {
        return;
    }
    
    LCAWSObject *objectFromCache = [self.class objectForName:self.fileName];
    if (objectFromCache) {
        ((LCAWSObject *)objectFromCache).tries++;
    }
    else
    {
        objectFromCache = [LCAWSObject new];
        [objectFromCache setClassKind:NSStringFromClass(self.class)];
        [objectFromCache setFileName:self.fileName];
        [objectFromCache setTries:self.tries];
        [objectFromCache setType:self.type];
    }
    
    NSMutableArray *objects = [self.class objects].mutableCopy;
    [objects addObject:[objectFromCache getDictionary]];
    [LCAWSObject setObjects:objects];
}

-(void)remove
{
    if ([NSString isEmptyString:self.fileName]) {
        return;
    }
    
    NSMutableArray *objects = [LCAWSObject objects].mutableCopy;
    for (NSMutableDictionary *obj in objects) {
        if ([obj[@"classKind"] isEqualToString:NSStringFromClass(self.class)] && [obj[@"fileName"] isEqualToString:self.fileName]) {
            [objects removeObject:obj];
            break;
        }
    }
    
    [LCAWSObject setObjects:objects];
}

#pragma mark - Getters

-(NSString *)filePath
{
    return [[LCAWSManager sharedManager] filePathForName:self.fileName andType:self.type];
}

#pragma mark - Static Methods

+(NSArray *)objects
{
    NSArray *objects = [kUserDefaults objectForKey:kObjectsKey];
    if (!objects) {
        objects = @[];
    }
    
    return objects;
}

+(NSArray *)filteredObjects
{
    NSArray *objects = [kUserDefaults objectForKey:kObjectsKey];
    if (!objects) {
        objects = @[];
    }
    
    objects = [objects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.classKind == %@", NSStringFromClass(self.class)]];
    
    return objects;
}


+(instancetype)objectForName:(NSString *)name
{
    if ([NSString isEmptyString:name]) {
        return nil;
    }
    
    for (NSDictionary *objectsDictionary in [self objects]) {
        if ([objectsDictionary allKeys].count > 0)
        {
            id object = [self new];
            [(LCAWSObject *)object setDictionary:objectsDictionary];
            
            if ([((LCAWSObject *)object).classKind isEqualToString:NSStringFromClass(self.class)] && [((LCAWSObject *)object).fileName isEqualToString:name]) {
                return object;
            }
        }
    }
    
    return nil;
}

+(void)setObjects:(NSArray *)objects
{
    [kUserDefaults setObject:objects forKey:kObjectsKey];
    [kUserDefaults synchronize];
}

@end

@implementation LCAWSFailedUpload

+(void)restoreFailedUploads:(NSArray *)failedUploads
{
    if (failedUploads.count == 0) {
        return;
    }
    
    NSMutableArray *tempFailedUploads = @[].mutableCopy;
    
    @synchronized(failedUploads)
    {
        for (NSDictionary *failedUploadDictionary in failedUploads) {
            if ([failedUploadDictionary allKeys].count > 0)
            {
                LCAWSObject *failedUpload = [LCAWSObject new];
                [failedUpload setDictionary:failedUploadDictionary];
                
                if (failedUpload.tries < INT16_MAX) {
                    failedUpload.tries++;
                    [tempFailedUploads addObject:[tempFailedUploads getDictionary]];
                    
                    [[LCAWSManager sharedManager] putFile:failedUpload.filePath withName:failedUpload.fileName forType:failedUpload.type inBucketPath:AWSBucketName withCompletionHandler:nil];
                }
            }
        }
        
        [LCAWSObject setObjects:tempFailedUploads];
    }
}

@end

@implementation LCAWSInUpload

@end

@implementation LCAWSFailedDeletion

@end

@interface LCAWSManager ()

@property (nonatomic, readwrite) NSArray *failedUploads;

@end

@implementation LCAWSManager

+(instancetype)sharedManager
{
    static id sharedInstance = nil;
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        sharedInstance = [self alloc];
        sharedInstance = [sharedInstance init];
    });
    
    return sharedInstance;
}

-(id)init
{
    self = [super init];
    
    if (self) {
        AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:AWSRegionEUWest1 identityPoolId:AWSIdentityPoolID];
        AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionEUWest1 credentialsProvider:credentialsProvider];
        [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
        
        [[AWSS3TransferManager defaultS3TransferManager] resumeAll:nil];
    }
    
    return self;
}

-(void)putData:(NSData *)data withName:(NSString *)fileName forType:(LCAWSManagerFileType)type inBucketPath:(NSString *)bucketPath withCompletionHandler:(RequestCompletionHandler)handler
{
    if ([data length] == 0 || [NSString isEmptyString:fileName] || [NSString isEmptyString:bucketPath]) {
        return;
    }
    
    NSString *tempPath = [FCFileManager pathForTemporaryDirectoryWithPath:@"tempimage.jpg"];
    [data writeToFile:tempPath atomically:NO];
    
    __block RequestCompletionHandler _handler = [handler copy];
    [self putFile:tempPath withName:fileName forType:type inBucketPath:bucketPath withCompletionHandler:^(BOOL success, id object) {
        _handler(success, object);
    }];
}

-(void)putFile:(NSString *)filePath withName:(NSString *)fileName forType:(LCAWSManagerFileType)type inBucketPath:(NSString *)bucketPath withCompletionHandler:(RequestCompletionHandler)handler
{
    if ([NSString isEmptyString:filePath] || [NSString isEmptyString:fileName] || [NSString isEmptyString:bucketPath]) {
        return;
    }
    
    __block RequestCompletionHandler _handler = [handler copy];
    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = AWSBucketName;
    uploadRequest.key = [self awsObjectKeyForFile:fileName withExtension:type inBucket:bucketPath];
    uploadRequest.body = [[NSURL alloc] initFileURLWithPath:filePath];
    uploadRequest.contentType = [self mimeTypeFromType:type];
    
    __block typeof(self) me = self;
    
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    [[transferManager upload:uploadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor]
                                                       withBlock:^id(AWSTask *task) {
                                                           if (task.error) {
                                                               if ([task.error.domain isEqualToString:AWSS3TransferManagerErrorDomain]) {
                                                                   switch (task.error.code) {
                                                                       case AWSS3TransferManagerErrorCancelled:
                                                                       case AWSS3TransferManagerErrorPaused:
                                                                           break;
                                                                           
                                                                       default:
                                                                           CLS_LOG(@"Error: %@", task.error);
                                                                           if (_handler) {
                                                                               _handler(NO, task.error.localizedDescription);
                                                                           }
                                                                           break;
                                                                   }
                                                               }
                                                               else
                                                               {
                                                                   // Unknown error.
                                                                   CLS_LOG(@"Error: %@", task.error);
                                                                   if (_handler) {
                                                                       _handler(NO, task.error.localizedDescription);
                                                                   }
                                                               }
                                                           }
                                                           else
                                                           {
                                                               // automagically store file in cache
                                                               [me storeFileInCache:[NSData dataWithContentsOfFile:filePath] forName:fileName andType:type];
                                                               
                                                               // AWSS3TransferManagerUploadOutput *uploadOutput = task.result;
                                                               // The file uploaded successfully.
                                                               if (_handler) {
                                                                   _handler(YES, nil);
                                                               }
                                                           }
                                                           
                                                           _handler = nil;
                                                           
                                                           return nil;
                                                       }];
}

-(void)deleteFile:(NSString *)fileName forType:(LCAWSManagerFileType)type inBucketPath:(NSString *)bucketPath withCompletionHandler:(RequestCompletionHandler)handler
{
    if ([NSString isEmptyString:fileName] || [NSString isEmptyString:bucketPath]) {
        if (handler) {
            handler(NO, @"");
        }
        
        return;
    }
    
    __block RequestCompletionHandler _handler = [handler copy];
    __block typeof(self) me = self;
    
    AWSS3DeleteObjectRequest *deleteRequest = [AWSS3DeleteObjectRequest new];
    deleteRequest.bucket = AWSBucketName;
    deleteRequest.key = [self awsObjectKeyForFile:fileName withExtension:type inBucket:bucketPath];
    
    AWSS3 *s3 = [AWSS3 defaultS3];
    [[s3 deleteObject:deleteRequest] continueWithBlock:^id(AWSTask *task) {
        if (task.error != nil) {
            if (task.error.code != AWSS3TransferManagerErrorCancelled && task.error.code != AWSS3TransferManagerErrorPaused) {
                CLS_LOG(@"%s Error: [%@]", __PRETTY_FUNCTION__, task.error);
            }
            
            if (_handler) {
                _handler(NO, task.error.description);
            }
        }
        else
        {
            // Completed logic here
            if (_handler) {
                _handler(YES, nil);
            }
        }
        
        _handler = nil;
        
        // automagically delete file from cache
        [me deleteFileFromCache:fileName withType:type];
        
        return nil;
    }];
}

-(void)getFile:(NSString *)fileName forType:(LCAWSManagerFileType)type inBucketPath:(NSString *)bucketPath withCompletionHandler:(RequestCompletionHandler)handler
{
    __block RequestCompletionHandler _handler = [handler copy];
    
    if ([NSString isEmptyString:fileName] || [NSString isEmptyString:bucketPath]) {
        if (_handler) {
            _handler(NO, nil);
        }
        
        return;
    }
    
    // automagically get file from cache
    __block NSData *file = [self fileForName:fileName andType:type];
    if (file) {
        if (_handler) {
            _handler(YES, file);
        }
        
        return;
    }

    __block typeof(self) me = self;
    
    AWSS3GetObjectRequest *getRequest = [AWSS3GetObjectRequest new];
    getRequest.bucket = AWSBucketName;
    getRequest.key = [self awsObjectKeyForFile:fileName withExtension:type inBucket:bucketPath];
    
    AWSS3 *transferManager = [AWSS3 defaultS3];
    [[transferManager getObject:getRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
        if (task.error || ![task.result body]) {
            if (_handler) {
                if ([task.error.domain isEqualToString:AWSTaskErrorDomain]) {
                    // do something different in the near future
                    _handler(YES, task.error);
                }
                else
                {
                    // possible network error
                    _handler(NO, task.error);
                }
            }
            return nil;
        }
        
        file = [task.result body];
        [me storeFileInCache:file forName:fileName andType:type];
        
        if (_handler) {
            _handler(YES, file);
        }
        
        return nil;
    }];
}

-(void)getFilePathForFile:(NSString *)fileName forType:(LCAWSManagerFileType)type inBucketPath:(NSString *)bucketPath withCompletionHandler:(RequestCompletionHandler)handler
{
    __block RequestCompletionHandler _handler = [handler copy];
    __block typeof(self) me = self;
    
    [self getFile:fileName forType:type inBucketPath:bucketPath withCompletionHandler:^(BOOL success, id object) {
        _handler(success, [me filePathForName:fileName andType:type]);
    }];
}

#pragma mark - Utils

-(NSString *)awsObjectKeyForFile:(NSString *)fileName withExtension:(LCAWSManagerFileType)type inBucket:(NSString *)bucket
{
    fileName = [fileName stringByDeletingPathExtension];
    return [bucket stringByAppendingString:[[fileName lastPathComponent] stringByAppendingString:[self fileExtensionFromType:type]]];
}

-(NSString *)mimeTypeFromType:(LCAWSManagerFileType)type
{
    switch (type) {
        case LCAWSManagerFileTypeJPEG:
            return @"image/jpeg";
            break;
        case LCAWSManagerFileTypeM4A:
            return @"audio/mp4";
        default:
            return @"";
            break;
    }
}

-(NSString *)fileExtensionFromType:(LCAWSManagerFileType)type
{
    switch (type) {
        case LCAWSManagerFileTypeJPEG:
            return @".jpg";
            break;
        case LCAWSManagerFileTypeM4A:
            return @".m4a";
        default:
            return @"";
            break;
    }
}

@end