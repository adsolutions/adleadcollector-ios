//
//  LCAWSManager.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 20/07/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, LCAWSManagerFileType) {
    LCAWSManagerFileTypeM4A = 10,
    LCAWSManagerFileTypeJPEG = 20
};

@interface LCAWSObject : NSObject

@property (nonatomic, readonly) NSString *filePath;
@property (nonatomic, strong) NSString *fileName;
@property (nonatomic) LCAWSManagerFileType type;
@property (nonatomic) NSString *classKind;
@property (nonatomic) NSInteger tries;

+(NSArray *)objects;
+(void)setObjects:(NSArray *)objects;

+(instancetype)objectForName:(NSString *)name;

-(void)store;
-(void)remove;

@end

@interface LCAWSFailedUpload : LCAWSObject

+(void)restoreFailedUploads:(NSArray *)failedUploads __unavailable;

@end

@interface LCAWSInUpload : LCAWSObject

@end

@interface LCAWSFailedDeletion : LCAWSObject

@end

/// @brief CompletionHandler
typedef void (^RequestCompletionHandler)(BOOL success, id object);

@interface LCAWSManager : NSObject

+(instancetype)sharedManager __unused;

-(void)putData:(NSData *)data withName:(NSString *)fileName forType:(LCAWSManagerFileType)type inBucketPath:(NSString *)bucketPath withCompletionHandler:(RequestCompletionHandler)handler;
-(void)putFile:(NSString *)filePath withName:(NSString *)fileName forType:(LCAWSManagerFileType)type inBucketPath:(NSString *)bucketPath withCompletionHandler:(RequestCompletionHandler)handler;
-(void)deleteFile:(NSString *)fileName forType:(LCAWSManagerFileType)type inBucketPath:(NSString *)bucketPath withCompletionHandler:(RequestCompletionHandler)handler;
-(void)getFile:(NSString *)fileName forType:(LCAWSManagerFileType)type inBucketPath:(NSString *)bucketPath withCompletionHandler:(RequestCompletionHandler)handler;
-(void)getFilePathForFile:(NSString *)fileName forType:(LCAWSManagerFileType)type inBucketPath:(NSString *)bucketPath withCompletionHandler:(RequestCompletionHandler)handler;

-(NSString *)fileExtensionFromType:(LCAWSManagerFileType)type;

@end

@interface LCAWSManager (Cache)

/* Cache Management */
+(BOOL)wipeCache;

-(NSString *)storeFileInCache:(NSData *)fileData forName:(NSString *)name andType:(LCAWSManagerFileType)type;
-(BOOL)deleteFileFromCache:(NSString *)name withType:(LCAWSManagerFileType)type;

@end