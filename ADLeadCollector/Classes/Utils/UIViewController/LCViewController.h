//
//  LCViewController.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 03/07/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCViewController : UIViewController

-(void)mobileBridgeDidUpdateDataWithNotification:(NSNotification *)notification;

@end