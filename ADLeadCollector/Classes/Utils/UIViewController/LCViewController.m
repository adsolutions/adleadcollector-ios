//
//  LCViewController.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 03/07/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCViewController.h"
#import "LCRootViewController.h"

#import <ReactiveCocoa/ReactiveCocoa.h>

@interface LCViewController ()

@end

@implementation LCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:kUpdateNotificationKey object:nil]
     subscribeNext:^(id x) {
         [self performSelectorOnMainThread:@selector(mobileBridgeDidUpdateDataWithNotification:) withObject:x waitUntilDone:NO];
     }];
}

-(void)viewDidAppear:(BOOL)animated
{
    if (self.navigationController.navigationBar.items.count < 2) {
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:[LCUtils livelyButtonWithFrame:CGRectMake(0, 0, 30, 30) forType:kFRDLivelyButtonStyleHamburger target:self andAction:@selector(showMenuButtonPressed:)]]];
    }
}

#pragma mark - Actions

-(void)showMenuButtonPressed:(id)sender
{
    [(LCRootViewController *)self.sideMenuViewController presentLeftMenuViewController];
}

#pragma mark - Notification

-(void)mobileBridgeDidUpdateDataWithNotification:(NSNotification *)notification
{
    //
}

@end