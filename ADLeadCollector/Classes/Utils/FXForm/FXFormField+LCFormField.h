//
//  LCFormField.h
//  LeadsCollector
//
//  Created by Daniele Angeli on 04/07/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "FXForms.h"

@interface FXFormField (LCFormField)

@property (nonatomic) NSInteger priority;
@property (nonatomic, readonly) FXFormController *formController;

@end

@class LCFormButtonPickerCell, LCFormCellPickerButton;

@protocol LCFXFormControllerExtensionsDelegate <FXFormControllerDelegate>

@optional
-(NSString *)formButtonPicker:(LCFormButtonPickerCell *)picker pathForResourceForButton:(LCFormCellPickerButton *)button atIndex:(NSUInteger)buttonIndex;

@optional
-(void)formButtonPicker:(LCFormButtonPickerCell *)picker didPressButton:(LCFormCellPickerButton *)button atIndex:(NSUInteger)buttonIndex;

@optional
-(NSUInteger)formButtonPicker:(LCFormButtonPickerCell *)picker stateForButton:(LCFormCellPickerButton *)button atIndex:(NSUInteger)buttonIndex;

@end