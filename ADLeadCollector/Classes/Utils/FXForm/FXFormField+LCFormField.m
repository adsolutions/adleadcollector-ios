//
//  LCFormField.m
//  LeadsCollector
//
//  Created by Daniele Angeli on 04/07/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "FXFormField+LCFormField.h"

#import <objc/runtime.h>

static char priorityKey;

@implementation FXFormField (LCFormField)

-(void)setPriority:(NSInteger)priority
{
     objc_setAssociatedObject(self, &priorityKey, @(priority), OBJC_ASSOCIATION_ASSIGN);
}

-(NSInteger)priority
{
    return [objc_getAssociatedObject(self, &priorityKey) integerValue];
}

@end