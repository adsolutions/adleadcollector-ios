//
//  DataLoadingOperation.h
//  FluentResourcePaging-example
//
//  Created by Alek Astrom on 2014-04-11.
//  Copyright (c) 2014 Alek Åström. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MBDataLoadingOperation : NSBlockOperation

- (instancetype)initForModelClass:(Class)class withObjectsPerPage:(NSInteger)objectsPerPage indexes:(NSIndexSet *)indexes andLastDocumentID:(id)lastDocumentID;
- (instancetype)initForModelClass:(Class)class withObjectsPerPage:(NSInteger)objectsPerPage indexes:(NSIndexSet *)indexes lastDocumentID:(id)lastDocumentID withFilter:(NSPredicate *)filter sortedBy:(NSSortDescriptor *)sortDescriptor;

@property (nonatomic, readonly) NSIndexSet *indexes;
@property (nonatomic, readonly) NSArray *dataPage;
@property (nonatomic, readonly) NSPredicate *filter;
@property (nonatomic, readonly) NSSortDescriptor *sortDescriptor;

@end