//
//  DataLoadingOperation.m
//  FluentResourcePaging-example
//
//  Created by Alek Astrom on 2014-04-11.
//  Copyright (c) 2014 Alek Åström. All rights reserved.
//

#import "MBDataLoadingOperation.h"

@implementation MBDataLoadingOperation

- (instancetype)initForModelClass:(Class)class withObjectsPerPage:(NSInteger)objectsPerPage indexes:(NSIndexSet *)indexes andLastDocumentID:(id)lastDocumentID
{
    self = [super init];
    
    if (self) {
        _indexes = indexes;
        
        MBDataLoadingOperation *weakSelf = self;
        [self addExecutionBlock:^{
            weakSelf->_dataPage = [class findAllFromDocumentID:lastDocumentID withLimit:objectsPerPage andSortedBy:nil];
        }];
    }
    
    return self;
}

- (instancetype)initForModelClass:(Class)class withObjectsPerPage:(NSInteger)objectsPerPage indexes:(NSIndexSet *)indexes lastDocumentID:(id)lastDocumentID withFilter:(NSPredicate *)filter sortedBy:(NSSortDescriptor *)sortDescriptor
{
    self = [super init];
    
    if (self) {
        _indexes = indexes;
        _filter = filter;
        _sortDescriptor = sortDescriptor;
        
        __block NSString *tempLastDocument = [lastDocumentID copy];
        
        MBDataLoadingOperation *weakSelf = self;
        [self addExecutionBlock:^{
            NSMutableArray *tempArray = @[].mutableCopy;
            for (NSUInteger k = 0; k < objectsPerPage; k = tempArray.count) {
                NSArray *arrayFromQuery = [[class findAllFromDocumentID:tempLastDocument withLimit:objectsPerPage matchingPredicate:filter andSortedBy:nil] subarrayWithRange:NSMakeRange(0, objectsPerPage)];
                if (arrayFromQuery.count > 0) {
                    [tempArray addObjectsFromArray:arrayFromQuery];
                }
                else
                {
                    [tempArray addObjectsFromArray:[class findAllMatchingPredicate:filter andSortedBy:nil]];
                    break;
                }
                
                tempLastDocument = [(MBModel *)arrayFromQuery.lastObject valueForKey:[[arrayFromQuery.lastObject class] emitKey]];
            }
            
            if (tempArray.count > objectsPerPage) {
                tempArray = [tempArray subarrayWithRange:NSMakeRange(0, objectsPerPage)].mutableCopy;
            }
            
            weakSelf->_dataPage = tempArray;
        }];
    }
    
    return self;
}

@end