//
//  DataProvider.m
//  FluentResourcePaging-example
//
//  Created by Alek Astrom on 2013-12-28.
//  Copyright (c) 2013 Alek Åström. All rights reserved.
//

#import "MBDataProvider.h"
#import "AWPagedArray.h"
#import "MBDataLoadingOperation.h"

const NSUInteger DataProviderDefaultPageSize = 100;

@interface MBDataProvider () <AWPagedArrayDelegate>

@property (nonatomic) NSUInteger pageSize;
@property (nonatomic) Class modelClass;
@property (nonatomic, strong) NSPredicate *filter;
@property (nonatomic, strong) NSSortDescriptor *sortDescriptor;
@property (nonatomic, strong) id lastDocument;

@end

@implementation MBDataProvider {
    AWPagedArray *_pagedArray;
    NSOperationQueue *_operationQueue;
    NSMutableDictionary *_dataLoadingOperations;
}

#pragma mark - Cleanup

- (void)dealloc {
    [_operationQueue.operations makeObjectsPerformSelector:@selector(cancel)];
}

#pragma mark - Initialization

- (instancetype)init {
    return [self initForModelClass:[MBModel class] withPageSize:DataProviderDefaultPageSize andTotalObjectsCount:DataProviderDefaultPageSize];
}

- (instancetype)initForModelClass:(Class)class withPageSize:(NSUInteger)pageSize totalObjectsCount:(NSInteger)totalCount withFilter:(NSPredicate *)filter sortedBy:(NSSortDescriptor *)sortDescriptor
{
    _filter = filter;
    _sortDescriptor = sortDescriptor;
    self = [self initForModelClass:class withPageSize:pageSize andTotalObjectsCount:totalCount];
    return self;
}

- (instancetype)initForModelClass:(Class)class withPageSize:(NSUInteger)pageSize andTotalObjectsCount:(NSInteger)totalCount
{
    self = [super init];
    if (self) {
        _modelClass = class;
        _pageSize = pageSize;
        _pagedArray = [[AWPagedArray alloc] initWithCount:totalCount objectsPerPage:pageSize];
        _pagedArray.delegate = self;
        _dataLoadingOperations = [NSMutableDictionary dictionary];
        _operationQueue = [NSOperationQueue new];
    }
    return self;
}

#pragma mark - Accessors

- (NSUInteger)loadedCount {
    return _pagedArray.pages.count*_pagedArray.objectsPerPage;
}

- (NSUInteger)pageSize {
    return _pagedArray.objectsPerPage;
}

- (NSArray *)dataObjects {
    return (NSArray *)_pagedArray;
}

#pragma mark - Other public methods

- (BOOL)isLoadingDataAtIndex:(NSUInteger)index {
    return _dataLoadingOperations[@([_pagedArray pageForIndex:index])] != nil;
}

- (void)loadDataForIndex:(NSUInteger)index {
    [self _setShouldLoadDataForPage:[_pagedArray pageForIndex:index]];
}

#pragma mark - Private methods

- (void)_setShouldLoadDataForPage:(NSUInteger)page {
    
    if (!_pagedArray.pages[@(page)] && !_dataLoadingOperations[@(page)]) {
        // Don't load data if there already is a loading operation in progress
        [self _loadDataForPage:page];
    }
}

- (void)_loadDataForPage:(NSUInteger)page {
    
    NSIndexSet *indexes = [_pagedArray indexSetForPage:page];
    
    _pageSize = indexes.count;
    
    NSOperation *loadingOperation = [self _loadingOperationForPage:page indexes:indexes];
    _dataLoadingOperations[@(page)] = loadingOperation;
    
    if ([self.delegate respondsToSelector:@selector(dataProvider:willLoadDataAtIndexes:)]) {
        [self.delegate dataProvider:self willLoadDataAtIndexes:indexes];
    }
    [_operationQueue addOperation:loadingOperation];
}

- (NSOperation *)_loadingOperationForPage:(NSUInteger)page indexes:(NSIndexSet *)indexes {
    MBDataLoadingOperation *operation = nil;
    if (_filter || _sortDescriptor) {
        operation = [[MBDataLoadingOperation alloc] initForModelClass:_modelClass withObjectsPerPage:_pageSize indexes:indexes lastDocumentID:_lastDocument withFilter:_filter sortedBy:_sortDescriptor];
    }
    else
    {
        operation = [[MBDataLoadingOperation alloc] initForModelClass:_modelClass withObjectsPerPage:_pageSize indexes:indexes andLastDocumentID:_lastDocument];
    }
    
    // Remember to not retain self in block since we store the operation
    __weak id weakSelf = self;
    __weak id weakOperation = operation;
    operation.completionBlock = ^{
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [weakSelf _dataOperation:weakOperation finishedLoadingForPage:page];
        }];
    };
    
    return operation;
}

- (void)_preloadNextPageIfNeededForIndex:(NSUInteger)index {
    
    if (!self.shouldLoadAutomatically)  {
        return;
    }
    
    NSUInteger currentPage = [_pagedArray pageForIndex:index];
    NSUInteger preloadPage = [_pagedArray pageForIndex:index+self.automaticPreloadMargin];
    
    if (preloadPage > currentPage && preloadPage <= _pagedArray.numberOfPages) {
        [self _setShouldLoadDataForPage:preloadPage];
    }
}

- (void)_dataOperation:(MBDataLoadingOperation *)operation finishedLoadingForPage:(NSUInteger)page {
    [_dataLoadingOperations removeObjectForKey:@(page)];
    [_pagedArray setObjects:operation.dataPage forPage:page];
    
    _lastDocument = [(MBModel *)operation.dataPage.lastObject valueForKey:[[operation.dataPage.lastObject class] emitKey]];
    
    if ([self.delegate respondsToSelector:@selector(dataProvider:didLoadDataAtIndexes:)]) {
        [self.delegate dataProvider:self didLoadDataAtIndexes:operation.indexes];
    }
}

#pragma mark - Paged array delegate

- (void)pagedArray:(AWPagedArray *)pagedArray willAccessIndex:(NSUInteger)index returnObject:(__autoreleasing id *)returnObject
{
    if ([*returnObject isKindOfClass:[NSNull class]] && self.shouldLoadAutomatically) {
        [self _setShouldLoadDataForPage:[pagedArray pageForIndex:index]];
    } else {
        [self _preloadNextPageIfNeededForIndex:index];
    }
}

@end