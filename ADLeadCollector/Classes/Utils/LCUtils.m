//
//  LCUtils.m
//  ADLeadCollector
//
//  Created by Daniele Angeli on 11/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCUtils.h"
#import "LCFolder.h"
#import "LCLead.h"
#import "LCForm.h"
#import "LCSettings.h"

#import <UIKit/UIKit.h>
//#import <PopulateKit/PopulateKit.h>
#import <UIColor-HexString/UIColor+HexString.h>
#import <DZNSegmentedControl/DZNSegmentedControl.h>
#import "MKStoreManager.h"
#import <FontAwesome+iOS/UIFont+FontAwesome.h>
#import <JDStatusBarNotification/JDStatusBarNotification.h>
#import <couchbase-lite-ios/CouchbaseLite/CouchbaseLite.h>
#import <NSString+Validation/NSString+Validation.h>

NSString *const JDStatusStyleInfo = @"JDStatusStyleInfo";

#if DEBUG
NSString *const kDefaultDatabaseName = @"leadscollector-debug";
NSString *const kDefaultServerDbURL = @"http://mobilebridge.replica.bitrace.io:4984/sync_leadscollector_test";
#else
NSString *const kDefaultDatabaseName = @"leadscollector";
NSString *const kDefaultServerDbURL = @"https://mobilebridge.bitrace.io:4984/sync_leadscollector";
#endif

//NSString *const kDefaultServerDbURL = @"https://mobilebridge.bitrace.io:4984/sync_leadscollector";

NSString *const kDefaultCompany = @"9D9590DB67601FDB4BDF1745AC6827EC";
NSString *const kCachedAddressesKey = @"it.aditsolutions.LeadsCollector.cachedAddresses";
NSString *const kCachedUsernamesKey = @"it.aditsolutions.LeadsCollector.cachedUsernames";
NSString *const kUpdateNotificationKey = @"it.aditsolutions.LeadsCollector.internalRefresh";
NSString *const AWSBucketName = @"leadscollector";
NSString *const AWSBucketEndPoint = @"leadscollector.s3.amazonaws.com";
NSString *const AWSVoiceNotesPath = @"mobile-app/voicenotes/";
NSString *const AWSPicturesPath = @"mobile-app/pictures/";
NSString *const AWSIdentityPoolID = @"eu-west-1:1f9d7cf7-1b5e-40b9-be8f-e2f9a46fa007";
NSString *const LCAWSFilesDirectory = @"AWSData";
NSString *const kUnassigned = @"kUnassigned";
NSString *const kSyncGatewayUsername = @"leadscollector";
NSString *const kSyncGatewayPassword = @"KrJ7tn76df3w2ErW6XN";

static UIColor *CompanyColor = nil;

@implementation LCUtils

inline UIColor *companyColor() {
    if (CompanyColor) {
        return CompanyColor;
    }
    
    NSString *companyColor = @"#2ecc72";
    if (![NSString isEmptyString:[LCUtils storedColor]]) {
        companyColor = [LCUtils storedColor];
    }
    
    CompanyColor = [UIColor colorWithHexString:companyColor];
    return CompanyColor;
}

+(NSString *)storedColor
{
    return [kUserDefaults stringForKey:@"it.aditsolutions.LeadsCollector.companyColor"];
}

#pragma mark - Colors & Styles

+(void)applyStyle
{
    [[UIApplication sharedApplication].keyWindow setTintColor:companyColor()];
    [[UISegmentedControl appearance] setTintColor:companyColor()];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UITableViewCell appearance] setTintColor:companyColor()];
    [[UITextField appearance] setFont:[LCUtils fontWithSize:16]];
    [[UITableViewCell appearance].textLabel setFont:[LCUtils fontWithSize:16]];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSFontAttributeName : [LCUtils fontWithSize:20]} forState:UIControlStateNormal];
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSFontAttributeName : [LCUtils fontWithSize:17]} forState:UIControlStateNormal];
    [[UILabel appearance] setFont:[LCUtils fontWithSize:15]];
    
    [[DZNSegmentedControl appearance] setBackgroundColor:ClearColor];
    [[DZNSegmentedControl appearance] setTintColor:companyColor()];
    [[DZNSegmentedControl appearance] setHairlineColor:FlatWhiteDark];
    
    [[DZNSegmentedControl appearance] setFont:[self fontWithSize:14.0]];
    [[DZNSegmentedControl appearance] setSelectionIndicatorHeight:2.5];
    [[DZNSegmentedControl appearance] setAnimationDuration:0.25];
    
    [JDStatusBarNotification setDefaultStyle:^JDStatusBarStyle *(JDStatusBarStyle *style) {
        style.barColor = companyColor();
        style.textColor = [UIColor whiteColor];
        style.animationType = JDStatusBarAnimationTypeMove;
        style.progressBarColor = style.textColor;
        style.progressBarHeight = 2.0;
        style.font = [LCUtils fontWithSize:14];
        style.progressBarPosition = JDStatusBarProgressBarPositionTop;
        return style;
    }];
    
    CompanyColor = nil;
}

+(NSString *)hexStringFromColor:(UIColor *)color
{
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];
    NSString *hexString=[NSString stringWithFormat:@"%02X%02X%02X", (int)(r * 255), (int)(g * 255), (int)(b * 255)];
    return hexString;
}

#pragma mark - Versioning

+(NSUInteger)currentVersion
{
    NSMutableString *localVersion = [[kUserDefaults objectForKey:@"CFBundleShortVersionString"] mutableCopy];
    NSInteger dotsCount = [localVersion replaceOccurrencesOfString:@"." withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, localVersion.length)];
    
    NSUInteger version = [localVersion integerValue];
    
    if (dotsCount < 2) {
        version = version*10;
    }
    
    return version;
}

#pragma mark - Fonts

+(UIFont *)fontWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"TradeGothicLTStd" size:size];
}

+(UIFont *)boldFontWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"TradeGothicLTStd-Bd2" size:size];
}

#pragma mark - LivelyButton

+(FRDLivelyButton *)livelyButtonWithFrame:(CGRect)frame forType:(kFRDLivelyButtonStyle)type target:(id)target andAction:(SEL)selector
{
    FRDLivelyButton *button = [[FRDLivelyButton alloc] initWithFrame:frame];
    [button setStyle:type animated:NO];
    NSMutableDictionary *options = [[FRDLivelyButton defaultOptions] mutableCopy];
    [options setObject:[UIColor whiteColor] forKey:kFRDLivelyButtonColor];
    [options setObject:@(2.0f) forKey:kFRDLivelyButtonLineWidth];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    [button setOptions:options];
    
    return button;
}

#pragma mark - Toasts

+(NSDictionary *)errorToastWithTitle:(NSString *)title andMessage:(NSString *)message
{
    return @{
             kCRToastTextKey : title,
             kCRToastSubtitleTextKey : message? message : @"",
             kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
             kCRToastBackgroundColorKey : FlatRed,
             kCRToastFontKey : [LCUtils fontWithSize:18],
             kCRToastSubtitleFontKey : [LCUtils fontWithSize:18],
             kCRToastAnimationInTypeKey : @(CRToastAnimationTypeSpring),
             kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
             kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
             kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionBottom),
             kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar)
             };
}

+(NSDictionary *)successToastWithTitle:(NSString *)title andMessage:(NSString *)message
{
    return @{
             kCRToastTextKey : title? title : @"",
             kCRToastSubtitleTextKey : message? message : @"",
             kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
             kCRToastBackgroundColorKey : companyColor(),
             kCRToastFontKey : [LCUtils fontWithSize:18],
             kCRToastSubtitleFontKey : [LCUtils fontWithSize:18],
             kCRToastAnimationInTypeKey : @(CRToastAnimationTypeSpring),
             kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
             kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
             kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionBottom),
             kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar)
             };
}

+(NSDictionary *)infoToastWithMessage:(NSString *)message andTimeout:(NSInteger)timeout
{
    return @{
             kCRToastSubtitleTextKey : message? message : @"",
             kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
             kCRToastBackgroundColorKey : companyColor(),
             kCRToastFontKey : [LCUtils fontWithSize:18],
             kCRToastSubtitleFontKey : [LCUtils fontWithSize:18],
             kCRToastAnimationInTypeKey : @(CRToastAnimationTypeSpring),
             kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
             kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
             kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionBottom),
             kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
             kCRToastTimeIntervalKey : @(timeout)
             };
}

#pragma mark - CBLUtilities

+(void)dataMigrationToFolders
{
    /*
    NSMutableArray *leadsIDs = @[].mutableCopy;
    NSArray *folders = [LCFolder findAll];
    [folders enumerateObjectsUsingBlock:^(LCFolder *obj, NSUInteger idx, BOOL *stop) {
        [leadsIDs addObjectsFromArray:obj.relatedLeads];
    }];
    
    NSArray *leads = [LCLead findAllMatchingPredicate:[NSPredicate predicateWithBlock:^BOOL(LCLead *evaluatedObject, NSDictionary *bindings) {
        return ![leadsIDs containsObject:evaluatedObject.document.documentID];
    }] andSortedBy:nil];
    
    leadsIDs = @[].mutableCopy;
    [leads enumerateObjectsUsingBlock:^(LCLead *obj, NSUInteger idx, BOOL *stop) {
        [leadsIDs addObject:obj.document.documentID];
    }];
    
    LCFolder *newFolder = [LCFolder createNew];
    [newFolder setName:@"Unassigned"];
    [newFolder setRelatedLeads:leadsIDs];
    [newFolder saveDocument];
     */
}

+(void)purgeLeadsFromFolder:(LCFolder *)folder
{
    /*
     NSMutableArray *leadsToDelete = Underscore
     .array(folder.relatedLeads)
     .filter(^BOOL (id obj) {
     return (([[[MBEngine sharedEngine] documentWithID:obj] isDeleted]) || (![[MBEngine database] existingDocumentWithID:obj]));
     });
     */
    
    /*
    NSMutableArray *leadsToDelete = @[].mutableCopy;
    
    [folder.relatedLeads enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        if ([[[MBEngine sharedEngine] documentWithID:obj] isDeleted]) {
            [leadsToDelete addObject:obj];
        }
        else if (![[MBEngine database] existingDocumentWithID:obj])
        {
            [leadsToDelete addObject:obj];
        }
    }];
    
    if (leadsToDelete.count == 0) {
        return;
    }
    
    NSMutableArray *remainingLeads = folder.relatedLeads.mutableCopy;
    [remainingLeads removeObjectsInArray:leadsToDelete];
    folder.relatedLeads = remainingLeads;
    [folder saveDocument];
     */
}

+(void)migrateAllLeadsToChannel:(NSString *)channelID
{
    for (CBLQueryRow *row in [[[MBEngine database] createAllDocumentsQuery] run:nil]) {
        CBLDocument *document = row.document;
        NSMutableDictionary *properties = document.properties.mutableCopy;
        properties[@"channels"] = @[channelID];
        [document putProperties:properties error:nil];
    }
}

+(void)purgeAll
{
    for (CBLQueryRow *row in [[[MBEngine database] createAllDocumentsQuery] run:nil]) {
        CBLDocument *document = row.document;
        [[MBEngine sharedEngine] deleteWithID:document.documentID];
    }
}

+(BOOL)validateUrl:(NSString *)candidate {
    //(http|https)://
    NSString *urlRegEx = @"((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}

+(BOOL)addCompany:(NSString *)companyID toForm:(NSString *)formID
{
    LCForm *form = [[LCForm alloc] initFromDocumentID:formID];
    
    NSMutableArray *channels = form.channels.mutableCopy;
    [channels addObject:companyID];
    [form setChannels:channels];
    
    return [form saveDocument];
}

+(NSString *)privacyPolicyForCompany:(NSString *)companyEmail
{
    return [NSString stringWithFormat:NSLocalizedString(@"kPrivacyPolicy%@", @""), companyEmail];
}

+(void)addPeople:(NSInteger)amoutOfPeople toFolder:(LCFolder *)folder
{
    /*
    NSArray *people = [self generateRandomPeople:amoutOfPeople];
    NSMutableArray *leadIDs = @[].mutableCopy;
    for (ACPerson *person in people) {
        LCLead *lead = [LCLead createNew];
        [lead setNameSurname:[NSString stringWithFormat:@"%@ %@", person.firstName, person.lastName]];
        [lead setEmail:person.email];
        [lead setPhoneNumber:person.phone];
        [lead setAssociatedFolder:folder.document.documentID];
        [lead setAuthor:[BTWallet myWallet].emailAddress];
        [lead setTimestamp:[[NSDate date] description]];
        [lead saveDocument];
        
        [leadIDs addObject:lead.document.documentID];
    }
    
    folder.relatedLeads = [leadIDs arrayByAddingObjectsFromArray:folder.relatedLeads];
    [folder saveDocument];*/
}

+(NSArray *)generateRandomPeople:(NSInteger)numberOfPeople
{
    NSMutableArray *people = @[].mutableCopy;
    /*
    ACPersonSet *set = [ACPersonSet personSetWithRandomNameAndImage];
    
    for (int k = 0; k < numberOfPeople; k++) {
        [people addObject:[set randomPerson]];
    }
    */
    return people;
}

#pragma mark - Premium

+(BOOL)needsPremium
{
    return [LCLead countAll] >= 10;
}

+(BOOL)isPremium
{
    return [LCSettings findFirst].LCPremiumToken.length > 0;
}

+(BOOL)isUnlimited
{
    if ([self isPremium]) {
        return YES;
    }
    
    return [MKStoreManager isFeaturePurchased:kLCUnlimitedPurchaseID];
}

#pragma mark - Answers

+(NSDictionary *)answersUserDetails
{
    if ([NSString isEmptyString:[BTWallet myWallet].emailAddress] || [NSString isEmptyString:[BTWallet myWallet].userName] || [NSString isEmptyString:[BTWallet myWallet].userSurname]) {
        return nil;
    }
    
    return @{@"email" : [BTWallet myWallet].emailAddress, @"name" : [BTWallet myWallet].userName, @"surname" : [BTWallet myWallet].userSurname};
}

#pragma mark - TempFile

+(NSString *)tempFileForType:(LCAWSManagerFileType)type
{
    //Unique recording URL
    return [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@", [[NSUUID UUID] UUIDString], [[LCAWSManager sharedManager] fileExtensionFromType:type]]];
}

#pragma mark - Company

+(BOOL)isEnterprise
{
    return [kUserDefaults boolForKey:@"LCEnterprise"];
}

+(NSString *)defaultCompanyID
{
    NSString *companyID = [kUserDefaults objectForKey:@"defaultCompany"];
    if (![NSString isEmptyString:companyID]) {
        return companyID;
    }
    
    for (NSDictionary *company in [BTWallet myWallet].companiesArray) {
        if ([company[@"identifier"] isEqualToString:[BTWallet myWallet].data[@"favorite_company"]]) {
            companyID = company[@"identifier"];
            [kUserDefaults setObject:companyID forKey:@"defaultCompany"];
            [kUserDefaults setObject:company[@"company"] forKey:@"defaultCompanyName"];
            [kUserDefaults synchronize];
            break;
        }
    }
    
    return companyID;
}

+(NSString *)defaultCompanyName
{
    return [kUserDefaults objectForKey:@"defaultCompanyName"];
}

#pragma mark - SplashScreen

+(UIBezierPath *)lcSplashBezier
{
    //// Color Declarations
    UIColor* fillColor = [UIColor colorWithRed: 1 green: 0.998 blue: 0.995 alpha: 1];
    
    //// Bezier Drawing
    UIBezierPath* bezierPath = UIBezierPath.bezierPath;
    [bezierPath moveToPoint: CGPointMake(70.15, 4.35)];
    [bezierPath addCurveToPoint: CGPointMake(65.75, -0.05) controlPoint1: CGPointMake(70.15, 1.92) controlPoint2: CGPointMake(68.18, -0.05)];
    [bezierPath addCurveToPoint: CGPointMake(61.35, 4.35) controlPoint1: CGPointMake(63.32, -0.05) controlPoint2: CGPointMake(61.35, 1.92)];
    [bezierPath addCurveToPoint: CGPointMake(65.75, 8.75) controlPoint1: CGPointMake(61.35, 6.78) controlPoint2: CGPointMake(63.32, 8.75)];
    [bezierPath addCurveToPoint: CGPointMake(70.15, 4.35) controlPoint1: CGPointMake(68.18, 8.75) controlPoint2: CGPointMake(70.15, 6.78)];
    [bezierPath closePath];
    [bezierPath moveToPoint: CGPointMake(9.3, 32.85)];
    [bezierPath addCurveToPoint: CGPointMake(4.9, 28.45) controlPoint1: CGPointMake(9.3, 30.42) controlPoint2: CGPointMake(7.33, 28.45)];
    [bezierPath addCurveToPoint: CGPointMake(0.5, 32.85) controlPoint1: CGPointMake(2.47, 28.45) controlPoint2: CGPointMake(0.5, 30.42)];
    [bezierPath addCurveToPoint: CGPointMake(4.9, 37.25) controlPoint1: CGPointMake(0.5, 35.28) controlPoint2: CGPointMake(2.47, 37.25)];
    [bezierPath addCurveToPoint: CGPointMake(9.3, 32.85) controlPoint1: CGPointMake(7.33, 37.25) controlPoint2: CGPointMake(9.3, 35.28)];
    [bezierPath closePath];
    [bezierPath moveToPoint: CGPointMake(52.63, 43.41)];
    [bezierPath addCurveToPoint: CGPointMake(54.62, 40.18) controlPoint1: CGPointMake(53.32, 42.36) controlPoint2: CGPointMake(53.98, 41.29)];
    [bezierPath addCurveToPoint: CGPointMake(61.16, 24.68) controlPoint1: CGPointMake(57.62, 34.99) controlPoint2: CGPointMake(59.86, 29.64)];
    [bezierPath addCurveToPoint: CGPointMake(70.74, 32.97) controlPoint1: CGPointMake(67.19, 26.85) controlPoint2: CGPointMake(70.74, 29.91)];
    [bezierPath addCurveToPoint: CGPointMake(52.63, 43.41) controlPoint1: CGPointMake(70.74, 37.44) controlPoint2: CGPointMake(63.17, 41.69)];
    [bezierPath closePath];
    [bezierPath moveToPoint: CGPointMake(50.19, 46.94)];
    [bezierPath addCurveToPoint: CGPointMake(58.83, 45.32) controlPoint1: CGPointMake(53.32, 46.58) controlPoint2: CGPointMake(56.21, 46.03)];
    [bezierPath addCurveToPoint: CGPointMake(57.44, 56.91) controlPoint1: CGPointMake(59.8, 50.73) controlPoint2: CGPointMake(59.28, 54.87)];
    [bezierPath addLineToPoint: CGPointMake(56.42, 57.76)];
    [bezierPath addCurveToPoint: CGPointMake(44.46, 53.61) controlPoint1: CGPointMake(53.68, 59.32) controlPoint2: CGPointMake(49.23, 57.63)];
    [bezierPath addCurveToPoint: CGPointMake(50.19, 46.94) controlPoint1: CGPointMake(46.43, 51.65) controlPoint2: CGPointMake(48.36, 49.41)];
    [bezierPath closePath];
    [bezierPath moveToPoint: CGPointMake(42.13, 51.48)];
    [bezierPath addCurveToPoint: CGPointMake(38.34, 47.29) controlPoint1: CGPointMake(40.86, 50.23) controlPoint2: CGPointMake(39.59, 48.82)];
    [bezierPath addCurveToPoint: CGPointMake(42.13, 47.39) controlPoint1: CGPointMake(39.6, 47.36) controlPoint2: CGPointMake(40.86, 47.39)];
    [bezierPath addCurveToPoint: CGPointMake(43.71, 45.81) controlPoint1: CGPointMake(43, 47.39) controlPoint2: CGPointMake(43.71, 46.68)];
    [bezierPath addCurveToPoint: CGPointMake(42.13, 44.23) controlPoint1: CGPointMake(43.71, 44.94) controlPoint2: CGPointMake(43, 44.23)];
    [bezierPath addCurveToPoint: CGPointMake(35.55, 43.92) controlPoint1: CGPointMake(39.91, 44.23) controlPoint2: CGPointMake(37.69, 44.12)];
    [bezierPath addCurveToPoint: CGPointMake(13.65, 33.97) controlPoint1: CGPointMake(23.83, 42.78) controlPoint2: CGPointMake(14.81, 38.61)];
    [bezierPath addLineToPoint: CGPointMake(13.65, 31.97)];
    [bezierPath addCurveToPoint: CGPointMake(21.4, 25.34) controlPoint1: CGPointMake(14.26, 29.58) controlPoint2: CGPointMake(17, 27.21)];
    [bezierPath addCurveToPoint: CGPointMake(23.1, 24.68) controlPoint1: CGPointMake(21.95, 25.11) controlPoint2: CGPointMake(22.52, 24.89)];
    [bezierPath addCurveToPoint: CGPointMake(29.64, 40.18) controlPoint1: CGPointMake(24.41, 29.65) controlPoint2: CGPointMake(26.65, 34.99)];
    [bezierPath addCurveToPoint: CGPointMake(31.01, 40.97) controlPoint1: CGPointMake(29.94, 40.68) controlPoint2: CGPointMake(30.47, 40.97)];
    [bezierPath addCurveToPoint: CGPointMake(31.8, 40.76) controlPoint1: CGPointMake(31.28, 40.97) controlPoint2: CGPointMake(31.55, 40.9)];
    [bezierPath addCurveToPoint: CGPointMake(32.38, 38.6) controlPoint1: CGPointMake(32.56, 40.32) controlPoint2: CGPointMake(32.81, 39.35)];
    [bezierPath addCurveToPoint: CGPointMake(26.11, 23.71) controlPoint1: CGPointMake(29.48, 33.59) controlPoint2: CGPointMake(27.33, 28.45)];
    [bezierPath addCurveToPoint: CGPointMake(42.13, 21.71) controlPoint1: CGPointMake(30.82, 22.41) controlPoint2: CGPointMake(36.34, 21.71)];
    [bezierPath addCurveToPoint: CGPointMake(48.46, 22) controlPoint1: CGPointMake(44.29, 21.71) controlPoint2: CGPointMake(46.4, 21.8)];
    [bezierPath addCurveToPoint: CGPointMake(51.89, 27.34) controlPoint1: CGPointMake(49.66, 23.68) controlPoint2: CGPointMake(50.8, 25.47)];
    [bezierPath addCurveToPoint: CGPointMake(54.8, 32.97) controlPoint1: CGPointMake(52.95, 29.19) controlPoint2: CGPointMake(53.93, 31.08)];
    [bezierPath addCurveToPoint: CGPointMake(51.89, 38.6) controlPoint1: CGPointMake(53.94, 34.85) controlPoint2: CGPointMake(52.96, 36.73)];
    [bezierPath addCurveToPoint: CGPointMake(48.26, 44.24) controlPoint1: CGPointMake(50.74, 40.58) controlPoint2: CGPointMake(49.52, 42.46)];
    [bezierPath addCurveToPoint: CGPointMake(47.8, 44.87) controlPoint1: CGPointMake(48.05, 44.41) controlPoint2: CGPointMake(47.9, 44.62)];
    [bezierPath addCurveToPoint: CGPointMake(42.13, 51.48) controlPoint1: CGPointMake(45.99, 47.33) controlPoint2: CGPointMake(44.08, 49.56)];
    [bezierPath closePath];
    [bezierPath moveToPoint: CGPointMake(38.37, 54.74)];
    [bezierPath addCurveToPoint: CGPointMake(27.83, 57.75) controlPoint1: CGPointMake(34.02, 58.01) controlPoint2: CGPointMake(30.18, 59.11)];
    [bezierPath addCurveToPoint: CGPointMake(25.43, 45.3) controlPoint1: CGPointMake(25.21, 56.24) controlPoint2: CGPointMake(24.33, 51.54)];
    [bezierPath addCurveToPoint: CGPointMake(34.08, 46.94) controlPoint1: CGPointMake(28.11, 46.03) controlPoint2: CGPointMake(31.02, 46.59)];
    [bezierPath addCurveToPoint: CGPointMake(34.17, 47.08) controlPoint1: CGPointMake(34.11, 46.98) controlPoint2: CGPointMake(34.14, 47.04)];
    [bezierPath addCurveToPoint: CGPointMake(39.79, 53.6) controlPoint1: CGPointMake(36.01, 49.54) controlPoint2: CGPointMake(37.9, 51.72)];
    [bezierPath addCurveToPoint: CGPointMake(38.37, 54.74) controlPoint1: CGPointMake(39.32, 54) controlPoint2: CGPointMake(38.85, 54.38)];
    [bezierPath closePath];
    [bezierPath moveToPoint: CGPointMake(27.83, 8.19)];
    [bezierPath addCurveToPoint: CGPointMake(38.37, 11.2) controlPoint1: CGPointMake(30.18, 6.83) controlPoint2: CGPointMake(34.02, 7.93)];
    [bezierPath addCurveToPoint: CGPointMake(45.93, 18.66) controlPoint1: CGPointMake(40.93, 13.13) controlPoint2: CGPointMake(43.49, 15.67)];
    [bezierPath addCurveToPoint: CGPointMake(42.13, 18.55) controlPoint1: CGPointMake(44.68, 18.59) controlPoint2: CGPointMake(43.41, 18.55)];
    [bezierPath addCurveToPoint: CGPointMake(25.43, 20.64) controlPoint1: CGPointMake(36.14, 18.55) controlPoint2: CGPointMake(30.39, 19.28)];
    [bezierPath addCurveToPoint: CGPointMake(25.16, 18.83) controlPoint1: CGPointMake(25.33, 20.03) controlPoint2: CGPointMake(25.23, 19.42)];
    [bezierPath addCurveToPoint: CGPointMake(27.83, 8.19) controlPoint1: CGPointMake(24.5, 13.43) controlPoint2: CGPointMake(25.47, 9.55)];
    [bezierPath closePath];
    [bezierPath moveToPoint: CGPointMake(61.86, 21.58)];
    [bezierPath addCurveToPoint: CGPointMake(63.78, 10.95) controlPoint1: CGPointMake(64.15, 16.45) controlPoint2: CGPointMake(64.08, 12.84)];
    [bezierPath addCurveToPoint: CGPointMake(63.66, 10.3) controlPoint1: CGPointMake(63.76, 10.73) controlPoint2: CGPointMake(63.71, 10.51)];
    [bezierPath addCurveToPoint: CGPointMake(63.42, 9.47) controlPoint1: CGPointMake(63.54, 9.78) controlPoint2: CGPointMake(63.43, 9.5)];
    [bezierPath addCurveToPoint: CGPointMake(63.4, 9.44) controlPoint1: CGPointMake(63.41, 9.46) controlPoint2: CGPointMake(63.41, 9.45)];
    [bezierPath addCurveToPoint: CGPointMake(61.27, 7.74) controlPoint1: CGPointMake(62.59, 9.07) controlPoint2: CGPointMake(61.86, 8.5)];
    [bezierPath addCurveToPoint: CGPointMake(60.19, 5.24) controlPoint1: CGPointMake(60.69, 6.98) controlPoint2: CGPointMake(60.34, 6.13)];
    [bezierPath addCurveToPoint: CGPointMake(58.1, 4.25) controlPoint1: CGPointMake(59.56, 4.75) controlPoint2: CGPointMake(58.87, 4.4)];
    [bezierPath addCurveToPoint: CGPointMake(45.82, 7.41) controlPoint1: CGPointMake(52.7, 2.68) controlPoint2: CGPointMake(45.82, 7.41)];
    [bezierPath addLineToPoint: CGPointMake(45.82, 7.41)];
    [bezierPath addCurveToPoint: CGPointMake(45.64, 7.52) controlPoint1: CGPointMake(45.76, 7.45) controlPoint2: CGPointMake(45.7, 7.48)];
    [bezierPath addCurveToPoint: CGPointMake(45.18, 9.71) controlPoint1: CGPointMake(44.91, 8) controlPoint2: CGPointMake(44.7, 8.98)];
    [bezierPath addCurveToPoint: CGPointMake(47.37, 10.16) controlPoint1: CGPointMake(45.66, 10.44) controlPoint2: CGPointMake(46.64, 10.64)];
    [bezierPath addCurveToPoint: CGPointMake(56.44, 8.19) controlPoint1: CGPointMake(51.14, 7.69) controlPoint2: CGPointMake(54.36, 6.99)];
    [bezierPath addCurveToPoint: CGPointMake(59.11, 18.83) controlPoint1: CGPointMake(58.79, 9.55) controlPoint2: CGPointMake(59.76, 13.43)];
    [bezierPath addCurveToPoint: CGPointMake(56.43, 29.1) controlPoint1: CGPointMake(58.71, 22.01) controlPoint2: CGPointMake(57.79, 25.5)];
    [bezierPath addCurveToPoint: CGPointMake(54.62, 25.77) controlPoint1: CGPointMake(55.86, 27.98) controlPoint2: CGPointMake(55.25, 26.86)];
    [bezierPath addCurveToPoint: CGPointMake(52.63, 22.53) controlPoint1: CGPointMake(53.98, 24.65) controlPoint2: CGPointMake(53.32, 23.58)];
    [bezierPath addCurveToPoint: CGPointMake(54.26, 22.81) controlPoint1: CGPointMake(53.18, 22.62) controlPoint2: CGPointMake(53.72, 22.71)];
    [bezierPath addCurveToPoint: CGPointMake(56.11, 21.56) controlPoint1: CGPointMake(55.11, 22.98) controlPoint2: CGPointMake(55.94, 22.42)];
    [bezierPath addCurveToPoint: CGPointMake(54.86, 19.72) controlPoint1: CGPointMake(56.27, 20.7) controlPoint2: CGPointMake(55.71, 19.88)];
    [bezierPath addCurveToPoint: CGPointMake(50.19, 19) controlPoint1: CGPointMake(53.34, 19.42) controlPoint2: CGPointMake(51.78, 19.19)];
    [bezierPath addCurveToPoint: CGPointMake(40.27, 8.68) controlPoint1: CGPointMake(47.08, 14.8) controlPoint2: CGPointMake(43.68, 11.25)];
    [bezierPath addCurveToPoint: CGPointMake(26.25, 5.46) controlPoint1: CGPointMake(34.78, 4.55) controlPoint2: CGPointMake(29.8, 3.41)];
    [bezierPath addCurveToPoint: CGPointMake(22.03, 19.21) controlPoint1: CGPointMake(22.69, 7.51) controlPoint2: CGPointMake(21.2, 12.4)];
    [bezierPath addCurveToPoint: CGPointMake(22.4, 21.58) controlPoint1: CGPointMake(22.12, 19.98) controlPoint2: CGPointMake(22.25, 20.77)];
    [bezierPath addCurveToPoint: CGPointMake(22.38, 21.58) controlPoint1: CGPointMake(22.39, 21.58) controlPoint2: CGPointMake(22.39, 21.58)];
    [bezierPath addCurveToPoint: CGPointMake(9.72, 30.33) controlPoint1: CGPointMake(11.61, 22.95) controlPoint2: CGPointMake(9.72, 30.33)];
    [bezierPath addLineToPoint: CGPointMake(9.93, 30.38)];
    [bezierPath addCurveToPoint: CGPointMake(10.44, 32.2) controlPoint1: CGPointMake(10.2, 30.93) controlPoint2: CGPointMake(10.36, 31.55)];
    [bezierPath addCurveToPoint: CGPointMake(10.36, 32.97) controlPoint1: CGPointMake(10.42, 32.46) controlPoint2: CGPointMake(10.36, 32.71)];
    [bezierPath addCurveToPoint: CGPointMake(10.43, 33.66) controlPoint1: CGPointMake(10.36, 33.2) controlPoint2: CGPointMake(10.4, 33.43)];
    [bezierPath addCurveToPoint: CGPointMake(9.86, 35.5) controlPoint1: CGPointMake(10.33, 34.32) controlPoint2: CGPointMake(10.16, 34.94)];
    [bezierPath addCurveToPoint: CGPointMake(9.93, 35.68) controlPoint1: CGPointMake(9.88, 35.56) controlPoint2: CGPointMake(9.92, 35.61)];
    [bezierPath addLineToPoint: CGPointMake(9.72, 35.62)];
    [bezierPath addCurveToPoint: CGPointMake(22.4, 44.36) controlPoint1: CGPointMake(9.72, 35.62) controlPoint2: CGPointMake(11.17, 41.61)];
    [bezierPath addCurveToPoint: CGPointMake(26.25, 60.49) controlPoint1: CGPointMake(20.9, 52.35) controlPoint2: CGPointMake(22.27, 58.19)];
    [bezierPath addCurveToPoint: CGPointMake(30.12, 61.48) controlPoint1: CGPointMake(27.4, 61.15) controlPoint2: CGPointMake(28.71, 61.48)];
    [bezierPath addCurveToPoint: CGPointMake(40.27, 57.26) controlPoint1: CGPointMake(33.09, 61.48) controlPoint2: CGPointMake(36.56, 60.05)];
    [bezierPath addCurveToPoint: CGPointMake(42.13, 55.76) controlPoint1: CGPointMake(40.89, 56.79) controlPoint2: CGPointMake(41.51, 56.29)];
    [bezierPath addCurveToPoint: CGPointMake(42.15, 55.77) controlPoint1: CGPointMake(42.14, 55.76) controlPoint2: CGPointMake(42.14, 55.77)];
    [bezierPath addCurveToPoint: CGPointMake(57.63, 61.79) controlPoint1: CGPointMake(52.28, 65.21) controlPoint2: CGPointMake(57.6, 61.81)];
    [bezierPath addCurveToPoint: CGPointMake(57.64, 61.79) controlPoint1: CGPointMake(57.64, 61.79) controlPoint2: CGPointMake(57.64, 61.79)];
    [bezierPath addCurveToPoint: CGPointMake(59.29, 59.53) controlPoint1: CGPointMake(57.98, 60.94) controlPoint2: CGPointMake(58.53, 60.16)];
    [bezierPath addCurveToPoint: CGPointMake(61.8, 58.33) controlPoint1: CGPointMake(60.04, 58.91) controlPoint2: CGPointMake(60.91, 58.51)];
    [bezierPath addCurveToPoint: CGPointMake(62.31, 57.42) controlPoint1: CGPointMake(61.8, 58.33) controlPoint2: CGPointMake(62.03, 58)];
    [bezierPath addCurveToPoint: CGPointMake(62.48, 57.06) controlPoint1: CGPointMake(62.37, 57.3) controlPoint2: CGPointMake(62.44, 57.18)];
    [bezierPath addCurveToPoint: CGPointMake(61.87, 44.38) controlPoint1: CGPointMake(63.42, 54.95) controlPoint2: CGPointMake(64.73, 50.34)];
    [bezierPath addCurveToPoint: CGPointMake(73.9, 32.97) controlPoint1: CGPointMake(69.36, 41.76) controlPoint2: CGPointMake(73.9, 37.65)];
    [bezierPath addCurveToPoint: CGPointMake(61.86, 21.58) controlPoint1: CGPointMake(73.9, 28.39) controlPoint2: CGPointMake(69.53, 24.27)];
    [bezierPath closePath];
    [bezierPath moveToPoint: CGPointMake(67.45, 63.95)];
    [bezierPath addCurveToPoint: CGPointMake(62.95, 59.45) controlPoint1: CGPointMake(67.45, 61.46) controlPoint2: CGPointMake(65.44, 59.45)];
    [bezierPath addCurveToPoint: CGPointMake(58.45, 63.95) controlPoint1: CGPointMake(60.46, 59.45) controlPoint2: CGPointMake(58.45, 61.46)];
    [bezierPath addCurveToPoint: CGPointMake(62.95, 68.45) controlPoint1: CGPointMake(58.45, 66.44) controlPoint2: CGPointMake(60.46, 68.45)];
    [bezierPath addCurveToPoint: CGPointMake(67.45, 63.95) controlPoint1: CGPointMake(65.44, 68.45) controlPoint2: CGPointMake(67.45, 66.44)];
    [bezierPath closePath];
    [fillColor setFill];
    [bezierPath fill];
    
    return bezierPath;
}

@end