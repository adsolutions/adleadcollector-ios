//
//  LCTableViewController.m
//  ADLeadCollector
//
//  Created by Daniele Angeli on 11/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCTableViewController.h"
#import "LCAlwaysTemplateImage.h"
#import "LCRootViewController.h"

#import <UIColor-HexString/UIColor+HexString.h>
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface LCTableViewController () <DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>

@end

@implementation LCTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;

    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(reloadData) forControlEvents:UIControlEventValueChanged];
    self.refreshControl.tintColor = companyColor();
    
    [self.tableView setEmptyDataSetDelegate:self];
    [self.tableView setEmptyDataSetSource:self];
    
    [self.view setTintColor:companyColor()];
    
    [RACObserve(self, searchDisplayController.searchBar.frame) subscribeNext:^(id x) {
        for (UIView *view in self.searchDisplayController.searchBar.subviews)
        {
            for (id subview in view.subviews)
            {
                if ([subview isKindOfClass:[UIButton class]])
                {
                    [subview setTintColor:[UIColor whiteColor]];
                    return;
                }
            }
        }
    }];
    
    self.tableView.contentOffset = CGPointMake(0, -self.searchDisplayController.searchBar.frame.size.height);
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kUpdateNotificationKey object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {
         [self performSelectorOnMainThread:@selector(mobileBridgeDidUpdateDataWithNotification:) withObject:x waitUntilDone:NO];
     }];
}

-(void)viewDidAppear:(BOOL)animated
{
    if (self.navigationController.navigationBar.items.count < 2) {
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:[LCUtils livelyButtonWithFrame:CGRectMake(0, 0, 30, 30) forType:kFRDLivelyButtonStyleHamburger target:self andAction:@selector(showMenuButtonPressed:)]]];
    }
}

#pragma mark - Actions

- (void)reloadData
{
    // Reload table data
    [self.tableView reloadData];
    
    // End the refreshing
    if (self.refreshControl)
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"d MMM, h:mm a"];
        NSString *title = [NSString stringWithFormat:NSLocalizedString(@"kLastUpdate%@", @"LCTableViewController"), [formatter stringFromDate:[NSDate date]]];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName : companyColor(), NSFontAttributeName : [LCUtils fontWithSize:20]}];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }
}

-(void)showMenuButtonPressed:(id)sender
{
    [(LCRootViewController *)self.sideMenuViewController presentLeftMenuViewController];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 0;
}

#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = NSLocalizedString(@"kNoLeadsAvailable", @"TableView");
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [LCUtils boldFontWithSize:17.0],
                                 NSForegroundColorAttributeName: FlatGray,
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [LCUtils fontWithSize:15.0],
                                 NSForegroundColorAttributeName: FlatGray,
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state
{
    return nil;
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    [imageView setImage:[UIImage imageNamed:@"LeadsCollectorWhite"]];
    return imageView.image;
}

- (UIColor *)imageTintColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return FlatGray;
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}

#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}

- (void)emptyDataSetWillAppear:(UIScrollView *)scrollView
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(void)emptyDataSetWillDisappear:(UIScrollView *)scrollView
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
}

#pragma mark - Notification

-(void)mobileBridgeDidUpdateDataWithNotification:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperation:[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(reloadData) object:nil]];
}

@end