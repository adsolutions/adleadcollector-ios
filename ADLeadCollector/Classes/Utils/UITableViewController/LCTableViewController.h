//
//  LCTableViewController.h
//  ADLeadCollector
//
//  Created by Daniele Angeli on 11/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <ADBIndexedTableView/ADBIndexedTableView.h>

@interface LCTableViewController : UITableViewController

-(void)reloadData;
-(void)mobileBridgeDidUpdateDataWithNotification:(NSNotification *)notification;

@end