//
//  LCFormViewController.h
//  ADLeadCollector
//
//  Created by Daniele Angeli on 11/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <MAFormViewController/MAFormField.h>

@interface LCFormViewController : MAFormViewController

@property (nonatomic, strong) NSArray *sectionsHeader;

-(void)reset;

@end