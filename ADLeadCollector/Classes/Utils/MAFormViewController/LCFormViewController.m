//
//  LCFormViewController.m
//  ADLeadCollector
//
//  Created by Daniele Angeli on 11/04/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import "LCFormViewController.h"

#import <UIColor-HexString/UIColor+HexString.h>

@interface LCFormViewController ()

@property (nonatomic, strong) UIBarButtonItem *backupButton;

@end

@implementation LCFormViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor whiteColor]];
    
    [self.navigationController.navigationBar setBarTintColor:companyColor()];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - UITableViewController 

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (_sectionsHeader.count == _sections.count) {
        return _sectionsHeader[section];
    }

    return @"";
}

-(void)handleAction
{
    if (!_backupButton) {
        _backupButton = self.navigationItem.rightBarButtonItem;
    }
    
    [self.navigationItem.leftBarButtonItem setEnabled:NO];
    
    UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [indicatorView setFrame:CGRectMake(0, 0, 35, 35)];
    [indicatorView setTintColor:[UIColor whiteColor]];
    [indicatorView setColor:[UIColor whiteColor]];
    [indicatorView startAnimating];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:indicatorView]];
    
    [super handleAction];
}

-(void)reset
{
    [self.navigationItem setRightBarButtonItem:_backupButton];
    
    [self.navigationItem.leftBarButtonItem setEnabled:YES];
}

@end