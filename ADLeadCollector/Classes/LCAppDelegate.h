//
//  AppDelegate.h
//  RubricaCouchBase
//
//  Created by Matteo on 13/02/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSMutableDictionary *availableProducts;

@end