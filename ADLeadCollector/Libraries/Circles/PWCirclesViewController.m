//
//  PWCirclesViewController.m
//  ParkWhiz
//
//  Created by Mo Bitar on 10/29/14.
//  Copyright (c) 2014 ParkWhiz. All rights reserved.
//

#import "PWCirclesViewController.h"
#import "PWCirclesView.h"

#import <FXBlurView/FXBlurView.h>
#import <PureLayout/PureLayout.h>

@interface PWCirclesViewController ()

@property (nonatomic) PWCirclesView *circlesView;

@end

@implementation PWCirclesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [self addCircles];
    
    [self.view setBackgroundColor:[FlatWhite colorWithAlphaComponent:0.25]];
}

- (void)addCircles
{
    self.circlesView = [[PWCirclesView alloc] initForAutoLayout];
    [self.view addSubview:self.circlesView];
    [self.circlesView autoPinEdgesToSuperviewEdges];
    
//    self.blurView = [[FXBlurView alloc] initWithFrame:self.view.frame];
//    [self.blurView setTintColor:FlatWhite];
//    [self.blurView setBlurRadius:100];
//    [self.blurView setUnderlyingView:self.circlesView];
//    [self.blurView setDynamic:YES];
//    [self.view addSubview:self.blurView];
}

@end