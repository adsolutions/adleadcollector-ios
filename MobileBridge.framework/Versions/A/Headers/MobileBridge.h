//
//  MobileBridge.h
//  MobileBridge
//
//  Created by Daniele Angeli on 28/04/15.
//  Copyright (c) 2015 AD Solutions. All rights reserved.
//

#ifndef MobileBridge_h
#define MobileBridge_h
#endif

#import <MobileBridge/MBEngine.h>
#import <MobileBridge/MBModel.h>