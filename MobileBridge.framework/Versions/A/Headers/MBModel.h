//
//  ADModel.h
//  MobileBridge®
//
//  Created by Daniele Angeli on 17/02/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <CouchbaseLite/CouchbaseLite.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBModel : CBLModel

/// Creates a new instance of the given class and sets the given channels
+(instancetype)createNew;

/// Creates a new instance of the given class and sets the given personalKey as a PullChannel
+(instancetype)createForPersonalUse;

/// Custom init, creates a new instance of the given class and sets the given personalKey as a PullChannel
-(instancetype)initForPersonalUse;

/// Look for the given document ID in the database and returs an allocated instance for the received class model
-(instancetype)initFromDocumentID:(nonnull NSString *)docID;

/// save the current document
-(BOOL)saveDocument;

/// delete the current document
-(BOOL)deleteDocument;

/// Queries the dataset and returns the first element
+(instancetype)findFirst;

/// Queries the dataset and returns every document that matches this class name
+(nullable NSArray *)findAll;

/// Queries the dataset and returns every document that matches the receiver class name and the given expression, ordered with the given sortDescriptor
+(nullable NSArray *)findAllMatchingPredicate:(nullable NSPredicate *)predicate andSortedBy:(nullable NSSortDescriptor *)sortDescriptor;

/// Queries the dataset and returns every document that matches the receiver class name, ordered with the given sortDescriptor starting from the given documentID with the disidered limit
+(nullable NSArray *)findAllFromDocumentID:(nonnull id)documentID withLimit:(NSInteger)limit andSortedBy:(nullable NSSortDescriptor *)sortDescriptor;

/// Queries the dataset and returns every document that matches the receiver class name and the given expression, ordered with the given sortDescriptor starting from the given documentID with the disidered limit
+(nullable NSArray *)findAllFromDocumentID:(nonnull id)documentID withLimit:(NSInteger)limit matchingPredicate:(nullable NSPredicate *)predicate andSortedBy:(nullable NSSortDescriptor *)sortDescriptor;

/// count all the documents in the dataset matching the receiver class name
+(NSUInteger)countAll;

/// count all the documents in the dataset matching the receiver class name and the given expression
+(NSUInteger)countAllMatchingPredicate:(nullable NSPredicate *)predicate;

/// You should override the emit key if you desire to index the default model's view with a different value. The default value is '_id'
+(nullable NSString *)emitKey;

/// You should override this getter to set a user's channel identifier to provide a isolated container for sensible data and the data shared with other users that have acces to the shared channels. This method is used when the class is initialized with it's "personal" initializers
+(nullable NSString *)personalKey;

/// You should override this getter to set a default channel identifier to provide a shared container to share data with other users that have acces to that channels
+(nullable NSString *)defaultKey;

/// override that property to let this document be a part of a feed
+(BOOL)feedMember;

/// the author of the document
@property (copy, nullable)  NSString *owner;

/// the platform from where the document has been created
@property (copy, nullable)  NSString *platform;

/// set the channels that you want to be pulled from the SyncGateway, default is nil. Note only the first object will be chosed
@property (copy, nullable) NSArray *channels;

@end

NS_ASSUME_NONNULL_END