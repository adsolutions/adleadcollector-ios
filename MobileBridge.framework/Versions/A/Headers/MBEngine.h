//
//  MSCouchbase.h
//  MobileBridge®
//
//  Created by Daniele Angeli on 13/02/15.
//  Copyright (c) 2015 AD Solutions di Angeli Daniele. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class CBLReplication, CBLDatabase, CBLDocument, CBLAuthenticator, MBEngine;

typedef NS_ENUM(NSUInteger, MBEngineState) {
    MBEngineStateNotWorking = 0,
    MBEngineStateWorking = 1
};

@protocol MBEngineDelegate <NSObject>

@optional
-(void)mobileBridgeEngine:(MBEngine *)engine hasEncounteredAnError:(NSError *)error;

@optional
-(void)mobileBridgeEngine:(MBEngine *)engine hasUpdatedprogress:(NSNumber *)overallProgress;

@optional
-(void)mobileBridgeEngine:(MBEngine *)engine didChangeStatus:(MBEngineState)state;

@optional
-(void)mobileBridgeEngine:(MBEngine *)engine hasGetNotifiedAboutChanges:(NSArray *)changesArray;

@optional
-(void)mobileBridgeEngine:(MBEngine *)engine didResetDatabase:(CBLDatabase *)database;

@optional
-(CBLAuthenticator *)mobileBridgeEngine:(MBEngine *)engine credentialsForReplication:(CBLReplication *)replication;

@end

@interface MBEngine : NSObject

/// Observable
@property (nonatomic) MBEngineState currentState;

@property (nonatomic, strong) CBLReplication *pull;
@property (nonatomic, strong) CBLReplication *push;

@property (nonatomic, copy) NSString *remoteServerURL;
@property (nonatomic, copy) NSString *databaseName;

/// Observable
@property (nonatomic, readonly) NSNumber *overallReplicationProgress;

// for the couchbase replications
@property (nonatomic, copy) NSArray *pushChannels;
@property (nonatomic, copy) NSArray *pullChannels;

@property (nonatomic, getter=isContinuousSync) BOOL continuousSync;
@property (nonatomic, getter=isPushChangesEnabled) BOOL pushChangesEnabled;

+(instancetype)sharedEngine;

+(CBLDatabase *)database;

-(void)addToDelegatesPool:(id <MBEngineDelegate>)delegate;
-(BOOL)removeFromDelegatesPool:(id <MBEngineDelegate>)delegate;
-(void)closeAllConnections;
-(void)sync;

-(CBLDocument *)storeDocument:(NSDictionary *)document withID:(NSString *)idString;
-(BOOL)deleteWithID:(NSString *)idString;
-(CBLDocument *)documentWithID:(NSString *)idString;

@end

NS_ASSUME_NONNULL_END